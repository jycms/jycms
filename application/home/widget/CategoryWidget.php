<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id$
// +----------------------------------------------------------------------

namespace app\home\Widget;
use think\Controller;

/**
 * 分类widget
 * 用于动态调用分类信息
 */
class CategoryWidget extends Controller{

	/* 显示指定分类的同级分类或子分类列表 */
	public function lists($cate, $child = false){
		$field = 'catid,catname,upid';
		if($child){
			$category = model('Categories')->getTree($cate, $field);
			$category = $category['_'];
		} else {
			$category = model('Categories')->getSameLevel($cate, $field);
		}
		$this->assign('category', $category);
		$this->assign('current', $cate);
		$this->display('Category/lists');
	}

}
