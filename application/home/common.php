<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id$
// +----------------------------------------------------------------------

/**
 * 前台公共库文件
 * 主要定义前台公共函数库
 */


/**
 * 获取导航URL
 * @param  string $url 导航URL
 * @return string      解析或的url
 * @author IT果农 <htmambo@163.com>
 */
function get_nav_url($url){
    switch ($url) {
        case 'http://' === substr($url, 0, 7):
        case '#' === substr($url, 0, 1):
            break;
        default:
            $url = url(strtolower($url));
            break;
    }
    return $url;
}

/**
 * 处理自定义字段
 *
 * @param mixed $cf
 * @param mixed $value
 * @param mixed $top
 * @param mixed $bot
 */
function ProcessCustomField($cf, $value, &$top, &$bot){
    static $row = array();
    if(!$row) $row = array('bot'=>0, 'top'=>0);
    $html = '<th width="1%" nowrap>'.$cf['name'].':</th><td width="*">';
    switch ($cf['type']) {
        case 'url':
            $html .= '<a href="'.$value.'" target="_blank">'.$value.'</a>';
            break;
        case 'urls':
            if(is_array($value['url'])){
                $urls = $value['url'];
                $notes = $value['note'];
                $curl = count($urls)-1;
                $html .= '<ul>';
                foreach((array)$urls as $k => $url){
                    $note = trim($url);
                    if($note){
                        if($notes[$k]) $note = $notes[$k];
                        $html .= '<li><a href="'.$url.'" target="_blank">'.$note.'</a></li>';
                    }
                }
                $html .='</ul>';
            } else {
                $html .= '<a href="'.$value.'" target="_blank">'.$value.'</a>';
            }
            break;
        default:
            $html .= $value;
            break;
    }
    $html .= '</td>';
    if($cf['position']){
        $row['bot'] = 1-$row['bot'];
        $html = '<tr  class="cbg'.$row['bot'].'">'.$html.'</tr>';
        $bot .= $html;
    } else {
        $row['top'] = 1-$row['top'];
        $html = '<tr class="cbg'.$row['top'].'">'.$html.'</tr>';
        $top .= $html;
    }
}