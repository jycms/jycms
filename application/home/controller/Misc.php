<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id$
// +----------------------------------------------------------------------
namespace app\home\controller;
use \CMS\Core;
use \CMS\Tree;

class Misc extends HomeController {

	function Glyphicon() {
		global $_G;
		$icons = array(
			'asterisk',
			'plus',
			'euro',
			'minus',
			'cloud',
			'envelope',
			'pencil',
			'glass',
			'music',
			'search',
			'heart',
			'star',
			'star-empty',
			'user',
			'film',
			'th-large',
			'th',
			'th-list',
			'ok',
			'remove',
			'zoom-in',
			'zoom-out',
			'off',
			'signal',
			'cog',
			'trash',
			'home',
			'file',
			'time',
			'road',
			'download-alt',
			'download',
			'upload',
			'inbox',
			'play-circle',
			'repeat',
			'refresh',
			'list-alt',
			'lock',
			'flag',
			'headphones',
			'volume-off',
			'volume-down',
			'volume-up',
			'qrcode',
			'barcode',
			'tag',
			'tags',
			'book',
			'bookmark',
			'print',
			'camera',
			'font',
			'bold',
			'italic',
			'text-height',
			'text-width',
			'align-left',
			'align-center',
			'align-right',
			'align-justify',
			'list',
			'indent-left',
			'indent-right',
			'facetime-video',
			'picture',
			'map-marker',
			'adjust',
			'tint',
			'edit',
			'share',
			'check',
			'move',
			'step-backward',
			'fast-backward',
			'backward',
			'play',
			'pause',
			'stop',
			'forward',
			'fast-forward',
			'step-forward',
			'eject',
			'chevron-left',
			'chevron-right',
			'plus-sign',
			'minus-sign',
			'remove-sign',
			'ok-sign',
			'question-sign',
			'info-sign',
			'screenshot',
			'remove-circle',
			'ok-circle',
			'ban-circle',
			'arrow-left',
			'arrow-right',
			'arrow-up',
			'arrow-down',
			'share-alt',
			'resize-full',
			'resize-small',
			'exclamation-sign',
			'gift',
			'leaf',
			'fire',
			'eye-open',
			'eye-close',
			'warning-sign',
			'plane',
			'calendar',
			'random',
			'comment',
			'magnet',
			'chevron-up',
			'chevron-down',
			'retweet',
			'shopping-cart',
			'folder-close',
			'folder-open',
			'resize-vertical',
			'resize-horizontal',
			'hdd',
			'bullhorn',
			'bell',
			'certificate',
			'thumbs-up',
			'thumbs-down',
			'hand-right',
			'hand-left',
			'hand-up',
			'hand-down',
			'circle-arrow-right',
			'circle-arrow-left',
			'circle-arrow-up',
			'circle-arrow-down',
			'globe',
			'wrench',
			'tasks',
			'filter',
			'briefcase',
			'fullscreen',
			'dashboard',
			'paperclip',
			'heart-empty',
			'link',
			'phone',
			'pushpin',
			'usd',
			'gbp',
			'sort',
			'sort-by-alphabet',
			'sort-by-alphabet-alt',
			'sort-by-order',
			'sort-by-order-alt',
			'sort-by-attributes',
			'sort-by-attributes-alt',
			'unchecked',
			'expand',
			'collapse-down',
			'collapse-up',
			'log-in',
			'flash',
			'log-out',
			'new-window',
			'record',
			'save',
			'open',
			'saved',
			'import',
			'export',
			'send',
			'floppy-disk',
			'floppy-saved',
			'floppy-remove',
			'floppy-save',
			'floppy-open',
			'credit-card',
			'transfer',
			'cutlery',
			'header',
			'compressed',
			'earphone',
			'phone-alt',
			'tower',
			'stats',
			'sd-video',
			'hd-video',
			'subtitles',
			'sound-stereo',
			'sound-dolby',
			'sound-5-1',
			'sound-6-1',
			'sound-7-1',
			'copyright-mark',
			'registration-mark',
			'cloud-download',
			'cloud-upload',
			'tree-conifer',
			'tree-deciduous'
		);
		include $this->view->getTpl('misc/glyphicon');
	}

	function Tag() {
		$tag = I('tag/s','','trim');
		$tagid = I('tagid/d', 0);
		if (!$tag && !$tagid) {
			$this->error('对不起，您没有指定要查询的标签！');
		}
		$where = '';
		if ($tagid) {
			$where = 'a.tagid=' . $tagid;
		} else {
			$where = "tagname='$tag'";
		}
		$aids = '';
		$model = D('Tags');
		$rows = $model->alias('a')->join('tagslist AS b', 'a.tagid=b.tagid', 'LEFT')->where($where)->select();
		$total = 0;
		if ($rows) {
			$aids = array();
			foreach ($rows as $v)
				$aids[$v['itemid']] = $v['itemid'];
			$total = count($aids);
			$aids = implode(',', $aids);
			if (!$tag)
				$tag = $v['tagname'];
		}
		if (!$aids) {
			$this->error('对不起，没有找到符合条件的记录！');
		}
		$pageTitle = '标签《' . $tag . '》的搜索结果，共' . $total . '篇';
        $this->assign('pageTitle',$pageTitle);
        $this->assign('aids', $aids);
        $page = new \CMS\Page($total, 10);
        $this->assign('multi', $page->show());
		return $this->fetch('article/search');
	}

	function GetTags() {
		$_SERVER['exception'] = 'getTags';
		$content = trim($_POST['content']);
		$content = strip_tags($content);
		$result = '<span>选择推荐标签来源</span>';
		$action = I('r.action');
		$action = (in_array($action, array(
					'local',
					'discuz',
					'split'
				))) ? $action : 'split';
		if ($action == 'split') {
			$cnsp = new \CMS\SplitWord();
			$cnsp->SetSource($content);
			$cnsp->StartAnalysis();
			$keywords = $cnsp->GetFinallyIndex(10);
			if ($keywords) {
				$result = '';
				foreach ($keywords as $v) {
					$result .= '<span>' . $v['word'] . '</span>';
				}
			}
		} elseif ($action == 'discuz') {
			// Get data
			$subjectenc = rawurlencode(strip_tags(iconv(CHARSET, 'GBK', $_POST['title'])));
			$messageenc = rawurlencode(substr(strip_tags(iconv(CHARSET, 'GBK', $_POST['content'])), 0, 1000));
			$url = 'http://keyword.discuz.com/related_kw.html?ics=gbk&ocs=' . strtolower(CHARSET) . '&title=' . $subjectenc . '&content=' . $messageenc;
			$data = dfsockopen($url);
			if ($data) {
				$result = '';
				$parser = xml_parser_create();
				xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
				xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
				xml_parse_into_struct($parser, $data, $values, $index);
				xml_parser_free($parser);
				foreach ($values as $valuearray) {
					if (strlen($valuearray['value']) > 3)
						if ($valuearray['tag'] == 'kw' || $valuearray['tag'] == 'ekw') {
							$result .= '<span>' . trim($valuearray['value']) . '</span>' . "\n";
						}
				}
			}
		} else {

		}
		exit($result);
	}

}
