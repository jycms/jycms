<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace app\admin\controller;

/**
 * 后台标签控制器
 */
class Tags extends AdminController {
    /**
     * 标签列表
     */
    public function index(){
        $title   =   I('title');
        $map['status']  =   array('egt',0);
        if(is_numeric($title)){
            $map['id']       =   array(intval($title),array('like','%'.$title.'%'));
        }else{
            $map['title']    =   array('like', '%'.(string)$title.'%');
        }

        $list   = $this->lists('Tags', $map, $order='sort asc,id desc');
        int_to_string($list);
        $this->assign('_list', $list);
        // 记录当前列表页的cookie
        Cookie('__forward__',$_SERVER['REQUEST_URI']);
        $this->pageTitle = '标签列表';
        return $this->fetch();
    }

    /**
     * 标签状态修改
     */
    public function changeStatus($method=null){
        $id = array_unique((array)I('id',0));
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }
        $map['id'] =   array('in',$id);
        switch ( strtolower($method) ){
            case 'forbidtags':
                $this->forbid('Tags', $map );
                break;
            case 'resumetags':
                $this->resume('Tags', $map );
                break;
            case 'deletetags':
                $this->delete('Tags', $map );
                break;
            default:
                $this->error('参数非法');
        }
    }

    /**
     * 新增标签
     */
    public function add(){
        $this->pageTitle = '新增标签';
        return $this->fetch();
    }

    /**
     * 编辑标签
     */
    public function edit(){
        $id = I('get.id');
        empty($id) && $this->error('参数不能为空！');
        $data = db('Tags')->field(true)->find($id);

        $this->assign('data',$data);
        $this->pageTitle = '编辑标签';
        return $this->fetch();
    }

    /**
     * 更新标签
     */
    public function save(){
        $res = model('Tags')->update();
        if(!$res){
            $this->error(model('Tags')->getError());
        }else{
            $this->success($res['id']?'更新成功！':'新增成功！', Cookie('__forward__'));
        }
    }

    /**
     * 排序
     */
    public function sort(){
        if(IS_GET){
            $ids = I('get.ids');
            $pid = I('get.pid');

            //获取排序的数据
            $map = array('status'=>array('gt',-1));
            if(!empty($ids)){
                $map['id'] = array('in',$ids);
            }else{
                if($pid !== ''){
                    $map['pid'] = $pid;
                }
            }
            $list = db('Tags')->where($map)->field('id,title')->order('sort asc,id desc')->select();

            $this->assign('list', $list);
            $this->pageTitle = '标签排序';
            return $this->fetch();
        }elseif(IS_POST){
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key=>$value){
                $res = db('Tags')->where(array('id'=>$value))->setField('sort', $key+1);
            }
            if($res !== false){
                $this->success('排序成功！');
            }else{
                $this->error('排序失败！');
            }
        }else{
            $this->error('非法请求！');
        }
    }

    /**
     * 搜索相关标签
     */
    public function tags(){
        $tags =  model('Tags')->searchTags($_GET['q']);
        $data = array();
        foreach($tags as $value){
            $data[] = array('id' => $value['title'], 'title'=> $value['title']);
        }
        echo json_encode($data);
    }
}
