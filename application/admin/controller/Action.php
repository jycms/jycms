<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------
namespace app\admin\controller;

/**
 * 行为控制器
 */
class Action extends AdminController {

    /**
     * 行为日志列表
     */
    public function actionLog() {
        // 获取列表数据
        $map['status'] = array('gt',-1 );
        $list = $this->lists('ActionLog', $map);
        int_to_string($list);
        
        $this->assign('_list', $list);
        $this->pageTitle = '行为日志';
        return $this->fetch();
    }

    /**
     * 查看行为日志
     *
     * @param integer $id            
     * @return none
     */
    public function edit($id = 0) {
        empty($id)&&$this->error('参数错误！');
        
        $info = db('ActionLog')->field(true)->find($id);
        
        $this->assign('info', $info);
        $this->pageTitle = '查看行为日志';
        return $this->fetch();
    }

    /**
     * 删除日志
     *
     * @param mixed $ids            
     */
    public function remove($ids = 0) {
        empty($ids)&&$this->error('参数错误！');
        if (is_array($ids)) {
            $map['id'] = array('in',$ids );
        } elseif (is_numeric($ids)) {
            $map['id'] = $ids;
        }
        $res = db('ActionLog')->where($map)->delete();
        if ($res!==false) {
            $this->success('删除成功！');
        } else {
            $this->error('删除失败！');
        }
    }

    /**
     * 清空日志
     */
    public function clear() {
        $res = db('ActionLog')->where('1=1')->delete();
        if ($res!==false) {
            $this->success('日志清空成功！');
        } else {
            $this->error('日志清空失败！');
        }
    }
}
