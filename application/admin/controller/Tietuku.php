<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace app\admin\controller;
use CMS\Upload\Driver\Tietuku\TietukuStorage;

/**
 * 贴图库扩展类测试控制器
 */
class Tietuku extends AdminController {

    public function _initialize(){
        $config = array(
            'accessKey'=>'__ODsglZwwjRJNZHAu7vtcEf-zgIxdQAY-QqVrZD',
            'secrectKey'=>'Z9-RahGtXhKeTUYy9WCnLbQ98ZuZ_paiaoBjByKv',
            'bucket'=>'blackwhite',
            'domain'=>'blackwhite.u.Tietukudn.com'
        );
        $this->Tietuku = new TietukuStorage($config);
        parent:: _initialize();
    }

    //获取文件列表
    public function index(){
        $this->pageTitle = '贴图库测试';
        $map = array();
        $prefix = trim(I('post.prefix'));
        if($prefix)
            $map['prefix'] = $prefix;
        $list = $this->Tietuku->getList($map);
        if(!$list)
            trace($this->Tietuku->error);
        $this->assign('Tietuku', $this->Tietuku);
        $this->assign('_list', $list['items']);
        return $this->fetch();
    }

    public function del(){
        $file = trim(I('file'));
        if($file){
            $result = $this->Tietuku->del($file);
            if(false === $result){
                $this->error($this->Tietuku->errorStr);
            }else{
                $this->success('删除成功');
            }
        }else{
            $this->error('错误的文件名');
        }
    }

    public function dealImage($key){
        $url = $this->Tietuku->dealWithType($key, 'img') ;
        redirect($url);
    }

    public function dealDoc($key){
        $url = $this->Tietuku->dealWithType($key, 'doc');
        redirect($url);
    }

    public function rename(){
        $key = I('get.file');
        $new = I('new_name');
        $result = $this->Tietuku->rename($key, $new);
        if(false === $result){
            trace($this->Tietuku->error);
            $this->error($this->Tietuku->errorStr);
        }else{
            $this->success('改名成功');
        }
    }

    public function batchDel(){
        $files = $_GET['key'];
        if(is_array($files) && $files !== array()){
            $files = array_column($files,'value');
            $result = $this->Tietuku->delBatch($files);
            if(false === $result){
                $this->error($this->Tietuku->errorStr);
            }else{
                $this->success('删除成功');
            }
        }else{
            $this->error('请至少选择一个文件');
        }
    }

    public function detail($key){
        $result = $this->Tietuku->info($key);
        if($result){
            if(in_array($result['mimeType'], array('image/jpeg','image/png'))){
                $img = "<img src='{$this->Tietuku->downlink($key)}?imageView/2/w/203/h/203'>";
            }else{
                $img = '<img class="file-prev" src="https://dn-portal-static.qbox.me/v104/static/theme/default/image/resource/no-prev.png">';
            }
            $time = date('Y-m-d H:i:s', bcmul(substr(strval($result['putTime']), 0, 11),"1000000000"));
            $filesize = format_bytes($result['fsize']);
            $tpl = <<<tpl
            <div class="right-head">
                {$key}
            </div>
            <div class="right-body">
                <div class="right-body-block">
                    <div class="prev-block">
                        {$img}
                    </div>
                    <p class="file-info-item">
                        外链地址：<input class="file-share-link" type="text" readonly="readonly" value="{$this->Tietuku->downlink($key)}">
                    </p>
                    <p class="file-info-item">
                        最后更新时间：<span>{$time}</span>
                    </p>
                    <p class="file-info-item">
                        文件大小：<span class="file-size">{$filesize}</span>
                    </p>
                </div>
            </div>
tpl;
            $this->success('as', '', array('tpl'=>$tpl));
        }else{
            $this->error('获取文件信息失败');
        }

    }

    //上传单个文件 用uploadify
    public function uploadOne(){
        $file = $_FILES['Tietuku_file'];
        $file = array(
            'name'=>'file',
            'fileName'=>$file['name'],
            'fileBody'=>file_get_contents($file['tmp_name'])
        );
        $config = array();
        $result = $this->Tietuku->upload($config, $file);
        if($result){
            $this->success('上传成功','', $result);
        }else{
            $this->error('上传失败','', array(
                'error'=>$this->Tietuku->error,
                'errorStr'=>$this->Tietuku->errorStr
            ));
        }
        exit;
    }

}
