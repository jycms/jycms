<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: PublicController.class.php 9 2016-09-17 11:22:17Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace app\admin\controller;

/**
 * 后台首页控制器
 */
class Public extends \CMS\Controller {

    /**
     * 后台用户登录
     */
    public function Login($username = null, $password = null, $verify = null){
        if(IS_POST){
            /* 检测验证码 TODO: */
            $result = check_verify($verify);
            if($result<1){
                $this->error('验证码输入错误！'.$result);
            }
            $Member = model('Member');
            if($Member->login($username, $password)){ //登录用户
                //TODO:跳转到登录前页面
                $this->success('登录成功！', url('Index/index'));
            } else {
                $this->error($Member->getError());
            }
        } else {
            if(is_login()){
                $this->redirect('Index/index');
            }else{
                /* 读取数据库中的配置 */
                $config	=	cache('DB_CONFIG_DATA');
                if(!$config){
                    $config	=	model('Config')->lists();
                    cache('DB_CONFIG_DATA',$config);
                }
                config($config); //添加配置

                return $this->fetch('login');
            }
        }
    }

    /* 退出登录 */
    public function Logout(){
        if(is_login()){
            model('Member')->logout();
            session('[destroy]');
            $this->success('退出成功！', url('login'));
        } else {
            $this->redirect('login');
        }
    }

    public function Verify(){
        $verify = new \CMS\Verify(array('fontSize'=>25, 'length'=>5, 'imageH'=>32, 'imageW' => 150));
        $verify->entry(1);
    }

}
