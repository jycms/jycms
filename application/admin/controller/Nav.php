<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: NavController.class.php 14 2016-09-24 06:11:25Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace app\admin\controller;

/**
 * 后台频道控制器
 */

class Nav extends AdminController {

    /**
     * 频道列表
     */
    public function index(){
        $pid = I('get.pid', 0);
        /* 获取频道列表 */
        $map  = array('status' => array('gt', -1), 'pid'=>$pid);
        $list = db('Nav')->where($map)->order('sort asc,id asc')->select();

        $this->assign('list', $list);
        $this->assign('pid', $pid);
        $this->pageTitle = '导航管理';
        return $this->fetch();
    }

    /**
     * 添加频道
     */
    public function add(){
        if(IS_POST){
            $Nav = model('Nav');
            $data = $Nav->create();
            if($data){
                $id = $Nav->add();
                if($id){
                    $this->success('新增成功', url('index'));
                    //记录行为
                    action_log('update_nav', 'nav', $id, UID);
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Nav->getError());
            }
        } else {
            $pid = I('get.pid', 0);
            //获取父导航
            if(!empty($pid)){
                $parent = db('Nav')->where(array('id'=>$pid))->field('title')->find();
                $this->assign('parent', $parent);
            }

            $this->assign('pid', $pid);
            $this->assign('info',null);
            $this->pageTitle = '新增导航';
            return $this->fetch('edit');
        }
    }

    /**
     * 编辑频道
     */
    public function edit($id = 0){
        if(IS_POST){
            $Nav = model('Nav');
            $data = $Nav->create();
            if($data){
                if($Nav->save()){
                    //记录行为
                    action_log('update_nav', 'nav', $data['id'], UID);
                    $this->success('编辑成功', url('index'));
                } else {
                    $this->error('编辑失败');
                }

            } else {
                $this->error($Nav->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = db('Nav')->find($id);

            if(false === $info){
                $this->error('获取配置信息错误');
            }

            $pid = I('get.pid', 0);
            //获取父导航
            if(!empty($pid)){
            	$parent = db('Nav')->where(array('id'=>$pid))->field('title')->find();
            	$this->assign('parent', $parent);
            }

            $this->assign('pid', $pid);
            $this->assign('info', $info);
            $this->pageTitle = '编辑导航';
            return $this->fetch();
        }
    }

    /**
     * 删除频道
     */
    public function del(){
        $id = array_unique((array)I('id',0));

        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }

        $map = array('id' => array('in', $id) );
        if(db('Nav')->where($map)->delete()){
            //记录行为
            action_log('update_nav', 'nav', $id, UID);
            $this->success('删除成功');
        } else {
            $this->error('删除失败！');
        }
    }

    /**
     * 导航排序
     */
    public function sort(){
        if(IS_GET){
            $ids = I('get.ids');
            $pid = I('get.pid');

            //获取排序的数据
            $map = array('status'=>array('gt',-1));
            if(!empty($ids)){
                $map['id'] = array('in',$ids);
            }else{
                if($pid !== ''){
                    $map['pid'] = $pid;
                }
            }
            $list = db('Nav')->where($map)->field('id,title')->order('sort asc,id asc')->select();

            $this->assign('list', $list);
            $this->pageTitle = '导航排序';
            return $this->fetch();
        }elseif (IS_POST){
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key=>$value){
                $res = db('Nav')->where(array('id'=>$value))->setField('sort', $key+1);
            }
            if($res !== false){
                $this->success('排序成功！');
            }else{
                $this->error('排序失败！');
            }
        }else{
            $this->error('非法请求！');
        }
    }
}