<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id$
// +----------------------------------------------------------------------
namespace app\admin\controller;

/**
 * @package        简易CMS
 * @subpackage    附件管理模块
 */
class Attach extends AdminController {


    function index() {
        global $_G;
        $model = model('Attachment');
        $searchsubmit = $_G['gp_searchsubmit'];
        if (!submitcheck('deletesubmit')) {

            $anchor = isset($_G['gp_anchor']) ? $_G['gp_anchor'] : '';
            $anchor = in_array($anchor, array('search', 'admin')) ? $anchor : 'search';
            shownav('内容', '附件管理' . ($task ? '_' . $task : ''));
            showsubmenusteps('附件管理' . ($task ? '_' . $task : ''), array(
                array('搜索', !$searchsubmit),
                array('管理', $searchsubmit),
            ));
            showtips('<li>文件名蓝色标示的附件为远程附件，鼠标指向文件名可以查看附件描述；</li><li>附件<em class="error">丢失</em>是指：附件属性比如文件名、路径、尺寸以及下载次数等相关数据在数据库中记录完好，但是附件文件已经被删除。</li>');
            showtagheader('div', 'search', !$searchsubmit);
            showformheader('attach' . ($task ? '&task=' . $task : ''));
            showtableheader();
            showsetting('只搜索记录存在但文件缺失的附件', 'nomatched', 0, 'radio');
            showsetting('每页显示数', '', $_G['gp_perpage'], "<select name='perpage'><option value='20'>每页20条</option><option value='50'>每页50条</option><option value='100'>每页100条</option></select>");
            showsetting('附件尺寸介于', array('sizeless', 'sizemore'), array('', ''), 'range');
            showsetting('被下载次数介于', array('dlcountless', 'dlcountmore'), array('', ''), 'range');
            showsetting('发表于多少天以前', 'daysold', '', 'text');
            showsetting('文件名', 'filename', '', 'text');
            showsetting('描述关键字', 'keywords', '', 'text');
            showsetting('作者', 'author', '', 'text');
            showsubmit('searchsubmit', '搜索');
            showtablefooter();
            showformfooter();
            showtagfooter('div');

            if (submitcheck('searchsubmit')) {

                $task == 'group' && $_G['gp_inforum'] = 'isgroup';
                $sql = "a.pid=p.pid";
                $inforum = $_G['gp_inforum'] != 'all' && $_G['gp_inforum'] != 'isgroup' ? intval($_G['gp_inforum']) : $_G['gp_inforum'];

                $sql .= is_numeric($inforum) ? " AND p.fid='$inforum'" : '';
                $sql .= $inforum == 'isgroup' ? ' AND t.isgroup=\'1\'' : ' AND t.isgroup=\'0\'';
                $sql .= $_G['gp_author'] ? " AND p.author='$_G[gp_author]'" : '';
                $sql .= $_G['gp_filename'] ? " AND a.filename LIKE '%$_G[gp_filename]%'" : '';

                if ($_G['gp_keywords']) {
                    $sqlkeywords = $or = '';
                    foreach (explode(',', str_replace(' ', '', $_G['gp_keywords'])) as $_G['gp_keywords']) {
                        $sqlkeywords .= " $or af.description LIKE '%$_G[gp_keywords]%'";
                        $or = 'OR';
                    }
                    $sql .= " AND ($sqlkeywords)";
                }

                $sql .= $_G['gp_sizeless'] ? " AND a.filesize<'$_G[gp_sizeless]'" : '';
                $sql .= $_G['gp_sizemore'] ? " AND a.filesize>'$_G[gp_sizemore]' " : '';
                $sql .= $_G['gp_dlcountless'] ? " AND a.downloads<'$_G[gp_dlcountless]'" : '';
                $sql .= $_G['gp_dlcountmore'] ? " AND a.downloads>'$_G[gp_dlcountmore]'" : '';

                $attachments = '';
                $_G['gp_perpage'] = intval($_G['gp_perpage']) < 1 ? 20 : intval($_G['gp_perpage']);
                $perpage = $_G['gp_pp'] ? $_G['gp_pp'] : $_G['gp_perpage'];
                $page = max(1, intval($_G['gp_page']));
                $start = ($page - 1) * $perpage;
                $attachmentarray = $model->table('Attachment AS a')->join('CommonMember AS b','b.uid=a.uid')->field('a.*, b.username AS author')->order('a.attachid DESC')->limit($start, $perpage)->select();
                foreach ((array) $attachmentarray as $attachment) {
                    $p = 'portal';
                    if ($attachment['aidtype'] == '')
                        $p = 'portal';
                    $matched = file_exists(A_DIR . '/' . $p . '/' . $attachment['attachment']) ? '' : '附件已丢失';
                    $attachment['url'] = A_URL . $p . '/';
                    $attachsize = sizecount($attachment['filesize']);
                    if (!$_G['gp_nomatched'] || ($_G['gp_nomatched'] && $matched)) {
                        $attachment['url'] = trim($attachment['url'], '/');
                        $attachments .= showtablerow('', array('class="td25"', 'title="' . $attachment['description'] . '" class="td21"'), array(
                            "<input class=\"checkbox\" type=\"checkbox\" name=\"delete[]\" value=\"$attachment[aid]\" />",
                            $attachment['filename'],
                            "<a href=\"$attachment[url]/$attachment[attachment]\" class=\"smalltxt\" target=\"_blank\">" . Core::msubstr($attachment['attachment'], 0, 30) . "</a>",
                            $attachment['author'],
                            $attachsize,
                            "<em class=\"error\">$matched<em>"
                                ), TRUE);
                    }
                }
                $attachmentcount = $model->count();
                $multipage = multi($attachmentcount, $perpage, $page, "index.php?group=admin&option=attachments");
                $multipage = preg_replace("/href=\"index.php\?option=attachments&amp;page=(\d+)\"/", "href=\"javascript:page(\\1)\"", $multipage);
                $multipage = str_replace("window.location='index.php?group=admin&option=attachments&amp;page='+this.value", "page(this.value)", $multipage);

                echo <<<EOT
<script type="text/JavaScript">
    function page(number) {
        $$$('attachmentforum').page.value=number;
        $$$('attachmentforum').searchsubmit.click();
    }
</script>
EOT;
                showtagheader('div', 'admin', $searchsubmit);
                showformheader('attach' . ($task ? '&task=' . $task : ''), '', 'attachmentforum');
                showhiddenfields(array(
                    'page' => $page,
                    'nomatched' => $_G['gp_nomatched'],
                    'inforum' => $_G['gp_inforum'],
                    'sizeless' => $_G['gp_sizeless'],
                    'sizemore' => $_G['gp_sizemore'],
                    'dlcountless' => $_G['gp_dlcountless'],
                    'dlcountmore' => $_G['gp_dlcountmore'],
                    'daysold' => $_G['gp_daysold'],
                    'filename' => $_G['gp_filename'],
                    'keywords' => $_G['gp_keywords'],
                    'author' => $_G['gp_author'],
                    'pp' => $_G['gp_pp'] ? $_G['gp_pp'] : $_G['gp_perpage']
                ));
                echo '<input type="submit" name="searchsubmit" value="提交" class="button" style="display: none" />';
                showformfooter();

                showformheader('attach&frame=no' . ($task ? '&task=' . $task : ''), 'target="attachmentframe"');
                showtableheader();
                showsubtitle(array('', 'filename', 'attach_path', 'author', 'size', ''));
                echo $attachments;
                showsubmit('deletesubmit', 'submit', 'del', '<a href="###" onclick="$(\'#admin\').hide();$(\'#search\').show();$$$(\'attachmentforum\').pp.value=\'\';'
                        . '$$$(\'attachmentforum\').page.value=\'\';" class="act lightlink normal">重新搜索</a>', $multipage);
                showtablefooter();
                showformfooter();
                echo '<iframe name="attachmentframe" style="display:none"></iframe>';
                showtagfooter('div');
            }
        } else {

            if ($ids = dimplode($_G['gp_delete'])) {
                $tids = $pids = 0;
                $rows = $model->where("aid IN ($ids)")->select();
                foreach ((array) $rows as $attach) {
                    dunlink($attach);
                    $tids .= ',' . $attach['tid'];
                    $pids .= ',' . $attach['pid'];
                }
                $model->where("aid IN($ids)")->delete();
                //@todo 删除附件涉及的文章

                $cpmsg = '附件编辑完成';
            } else {

                $cpmsg = '附件编辑失败';
            }
            echo "<script type=\"text/JavaScript\">alert('$cpmsg');parent.\$\$\$('attachmentforum').searchsubmit.click();</script>";
        }
    }

}

?>