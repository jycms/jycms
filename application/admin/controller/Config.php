<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace app\admin\controller;

/**
 * 后台配置控制器
 */
class Config extends AdminController {
    /**
     * 配置管理
     */
    public function index() {
        /* 查询条件初始化 */
        $map = array('status' => 1);
        if (isset($_GET['group'])) {
            $map['group'] = I('group', 0);
        }
        if (isset($_GET['name'])) {
            $map['name'] = array('like', '%' . (string) I('name') . '%');
        }

        $list = $this->lists('Config', $map, 'sort,id');
        // 记录当前列表页的cookie
        Cookie('__forward__', $_SERVER['REQUEST_URI']);

        $this->assign('group', config('CONFIG_GROUP_LIST'));
        $this->assign('group_id', input('group', 0, 'intval'));
        $this->assign('list', $list);
        $this->pageTitle = '配置管理';
        return $this->fetch();
    }

    public function getDatas(){
        $map = array('status' => 1);
        if (isset($_GET['group'])) {
            $map['group'] = I('group', 0);
        }
        if (isset($_GET['name'])) {
            $map['name'] = array('like', '%' . (string) I('name') . '%');
        }

        $list = $this->lists('Config', $map, 'sort,id');
        return $this->ajaxReturn($list);
    }
    /**
     * 新增配置
     */
    public function add() {
        if (IS_POST) {
            $Config = model('Config');
            $data = $Config->create();
            if ($data) {
                if ($Config->add()) {
                    cache('DB_CONFIG_DATA', null);
                    $this->success('新增成功', url('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Config->getError());
            }
        } else {
            $this->pageTitle = '新增配置';
            $this->assign('info', null);
            return $this->fetch('edit');
        }
    }

    /**
     * 编辑配置
     */
    public function edit($id = 0) {
        if (IS_POST) {
            $Config = model('Config');
            $data = $Config->create();
            if ($data) {
                if ($Config->save()) {
                    cache('DB_CONFIG_DATA', null);
                    //记录行为
                    action_log('update_config', 'config', $data['id'], UID);
                    $this->success('更新成功', Cookie('__forward__'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error($Config->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = db('Config')->field(true)->find($id);

            if (false === $info) {
                $this->error('获取配置信息错误');
            }
            $this->assign('info', $info);
            $this->pageTitle = '编辑配置';
            return $this->fetch();
        }
    }

    /**
     * 批量保存配置
     */
    public function save($config) {
        if ($config && is_array($config)) {
            $Config = db('Config');
            foreach ($config as $name => $value) {
                $map = array('name' => $name);
                $Config->where($map)->setField('value', $value);
            }
        }
        cache('DB_CONFIG_DATA', null);
        $this->success('保存成功！');
    }

    /**
     * 删除配置
     */
    public function del() {
        $id = array_unique((array) I('id', 0));

        if (empty($id)) {
            $this->error('请选择要操作的数据!');
        }

        $map = array('id' => array('in', $id));
        if (db('Config')->where($map)->delete()) {
            cache('DB_CONFIG_DATA', null);
            //记录行为
            action_log('update_config', 'config', $id, UID);
            $this->success('删除成功');
        } else {
            $this->error('删除失败！');
        }
    }

    // 获取某个标签的配置参数
    public function group() {
        $id = input('id', 1, 'intval');
        $type = config('CONFIG_GROUP_LIST');
        $list = db("Config")->where(array('status' => 1, 'group' => $id))->field('id,name,title,extra,value,remark,type')->order('sort')->select();
        if ($list) {
            $this->assign('list', $list);
        }
        $this->assign('id', $id);
        $this->pageTitle = $type[$id] . '设置';
        if (I('load') == 'ajax') {
            return $this->fetch('form');
        } else {
            return $this->fetch();
        }
    }

    /**
     * 配置排序
     */
    public function sort() {
        if (IS_GET) {
            $ids = input('ids');

            //获取排序的数据
            $map = array('status' => array('gt', -1));
            if (!empty($ids)) {
                $map['id'] = array('in', $ids);
            } elseif (I('group')) {
                $map['group'] = I('group');
            }
            $list = db('Config')->where($map)->field('id,title')->order('sort asc,id asc')->select();

            $this->assign('list', $list);
            $this->pageTitle = '配置排序';
            return $this->fetch();
        } elseif (IS_POST) {
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key => $value) {
                $res = db('Config')->where(array('id' => $value))->setField('sort', $key + 1);
            }
            if ($res !== false) {
                $this->success('排序成功！', Cookie('__forward__'));
            } else {
                $this->error('排序失败！');
            }
        } else {
            $this->error('非法请求！');
        }
    }

    public function _initialize() {
        parent::_initialize();
        $this->quickLink = array(
            array(
                'title' => '活动管理',
                'link'  => url('index')
            ),
            array(
                'title'  => '添加活动',
                'link'   => url('add'),
                'hidden' => config('ACTION_NAME') != 'index'
            ),
            array(
                'title' => '作品管理',
                'link'  => url('works')
            ),
            array(
                'title'  => '添加作品',
                'link'   => url('addWork'),
                'hidden' => config('ACTION_NAME') != 'works'
            ),
            array(
                'title' => '参赛人员管理',
                'link'  => url('players')
            ),
            array(
                'title'  => '添加参赛人',
                'link'   => url('addplayer'),
                'hidden' => config('ACTION_NAME') != 'players'
            ),
            array(
                'title' => '投票情况',
                'link'  => url('votes')
            )
        );
    }

}
