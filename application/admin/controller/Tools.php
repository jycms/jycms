<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace app\admin\controller;

/**
 * @package		简易CMS
 * @subpackage	后台工具箱
 */
class tools extends AdminController {
    public function sql() {
        if (IS_AJAX) {
            $sql = input("post.sql");
            $sql = base64_decode($sql);
            if ($sql) {
                if (preg_match('/INSERT INTO|DELETE|UPDATE\s/i', $sql, $info)) {
                    $result = \think\Db::execute($sql);
                } else {
                    $result = \think\Db::query($sql);
                }
                return dump($result); //json_encode($result)
            } else {
                return "错误的sql语句";
            }
        }
        //读取所有的表
        $tables = \think\Db::query('SHOW TABLES');
        $this->assign('tables', $tables);
        $database = \think\Config::get("database");
        $this->assign('tkey', "tables_in_" . $database["database"]);
        return $this->fetch();
    }

    function CleanCache() {

        /**
         * 删除指定目录下的所有文件
         * @param string $dir
         * @param bolean $rescure 是否递归删除
         */
        function deldir($dir, $rescure = true) {
            if (!is_dir($dir)) {
                return FALSE;
            }
            $dh = opendir($dir);
            while ($file = readdir($dh)) {
                if ($file != "." && $file != "..") {
                    $fullpath = $dir . "/" . $file;
                    echo $fullpath . '<br />';
                    if (!is_dir($fullpath)) {
                        Storage::unlink($fullpath);
                    } else if ($rescure) {
                        deldir($fullpath);
                    }
                }
            }
        }

        //要清除的文件夹列表
        $lists = array(
            'cache', 'temp', 'logs', 'data'
        );

        foreach ($lists as $dir) {
            deldir(RUNTIME_PATH . $dir);
        }

        //要清除的文件列表
        $files = array(
            'common~runtime.php'
        );
        foreach ($files as $k) {
            Storage::unlink(RUNTIME_PATH . '/' . $k);
        }
    }

}
