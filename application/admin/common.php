<?php

// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------
/**
 * 解析插件数据列表定义规则
 * @param type $data
 * @param type $grid
 * @param type $addon
 * @return type
 */
function get_addonlist_field($data, $grid, $addon) {
    // 获取当前字段数据
    foreach ((array) $grid['field'] as $field) {
        $array = explode('|', $field);
        $temp = $data[$array[0]];
        // 函数支持
        if (isset($array[1])) {
            $temp = call_user_func($array[1], $temp);
        }
        $data2[$array[0]] = $temp;
    }
    if (!empty($grid['format'])) {
        $value = preg_replace_callback('/\[([a-z_]+)\]/', function($match) use($data2) {
            return $data2[$match[1]];
        }, $grid['format']);
    } else {
        $value = implode(' ', $data2);
    }

    // 链接支持
    if (!empty($grid['href'])) {
        $links = explode(',', $grid['href']);
        foreach ($links as $link) {
            $array = explode('|', $link);
            $href = $array[0];
            if (preg_match('/^\[([a-z_]+)\]$/', $href, $matches)) {
                $val[] = $data2[$matches[1]];
            } else {
                $show = isset($array[1]) ? $array[1] : $value;
                // 替换系统特殊字符串
                $href = str_replace(
                        array('[DELETE]', '[EDIT]', '[ADDON]'), array('del?ids=[id]&name=[ADDON]', 'edit?id=[id]&name=[ADDON]', $addon), $href);

                // 替换数据变量
                $href = preg_replace_callback('/\[([a-z_]+)\]/', function($match) use($data) {
                    return $data[$match[1]];
                }, $href);

                $val[] = '<a href="' . url($href) . '">' . $show . '</a>';
            }
        }
        $value = implode(' ', $val);
    }
    return $value;
}

/**
 * 获取对应状态的文字信息
 * @param int $status
 * @return string 状态文字 ，false 未获取到
 * @author huajie <banhuajie@163.com>
 */
function get_status_title($status = null) {
    if (!isset($status)) {
        return false;
    }
    switch ($status) {
        case -1 : return '已删除';
            break;
        case 0 : return '禁用';
            break;
        case 1 : return '正常';
            break;
        case 2 : return '待审核';
            break;
        default : return false;
            break;
    }
}

// 获取数据的状态操作
function show_status_op($status) {
    switch ($status) {
        case 0 : return '启用';
            break;
        case 1 : return '禁用';
            break;
        case 2 : return '审核';
            break;
        default : return false;
            break;
    }
}

/**
 * 获取配置的类型
 * @param string $type 配置类型
 * @return string
 */
function get_config_type($type = 0) {
    $list = config('CONFIG_TYPE_LIST');
    return $list[$type];
}

/**
 * 获取配置的分组
 * @param string $group 配置分组
 * @return string
 */
function get_config_group($group = 0) {
    $list = config('CONFIG_GROUP_LIST');
    return $group ? $list[$group] : '';
}

/**
 * select返回的数组进行整数映射转换
 *
 * @param array $map  映射关系二维数组  array(
 *                                          '字段名1'=>array(映射关系数组),
 *                                          '字段名2'=>array(映射关系数组),
 *                                           ......
 *                                       )
 * @return array
 *
 *  array(
 *      array('id'=>1,'title'=>'标题','status'=>'1','status_text'=>'正常')
 *      ....
 *  )
 *
 */
function int_to_string(&$data, $map = array('status' => array(1 => '正常', -1 => '已删除', 0 => '禁用', 2 => '未审核', 3 => '草稿'))) {
    if ($data === false || $data === null) {
        return $data;
    }
    $data = (array) $data;
    foreach ($data as $key => &$row) {
        if (is_object($row)) {
            $row = $row->toArray();
        }
        foreach ($map as $col => $pair) {
            if (isset($row[$col]) && isset($pair[$row[$col]])) {
                $data[$key][$col . '_text'] = $pair[$row[$col]];
            }
        }
    }
    return $data;
}

// 分析枚举类型配置值 格式 a:名称1,b:名称2
function parse_config_attr($string) {
    $array = preg_split('/[,;\r\n]+/', trim($string, ",;\r\n"));
    if (strpos($string, ':')) {
        $value = array();
        foreach ($array as $val) {
            list($k, $v) = explode(':', $val);
            $value[$k] = $v;
        }
    } else {
        $value = $array;
    }
    return $value;
}

// 分析枚举类型字段值 格式 a:名称1,b:名称2
// 暂时和 parse_config_attr功能相同
// 但请不要互相使用，后期会调整
function parse_field_attr($string) {
    if (0 === strpos($string, ':')) {
        // 采用函数定义
        return eval('return ' . substr($string, 1) . ';');
    } elseif (0 === strpos($string, '[')) {
        // 支持读取配置参数（必须是数组类型）
        return config(substr($string, 1, -1));
    }

    $array = preg_split('/[,;\r\n]+/', trim($string, ",;\r\n"));
    if (strpos($string, ':')) {
        $value = array();
        foreach ($array as $val) {
            list($k, $v) = explode(':', $val);
            $value[$k] = $v;
        }
    } else {
        $value = $array;
    }
    return $value;
}

/**
 * 获取行为数据
 * @param string $id 行为id
 * @param string $field 需要获取的字段
 */
function get_action($id = null, $field = null) {
    if (empty($id) && !is_numeric($id)) {
        return false;
    }
    $list = cache('action_list');
    if (empty($list[$id])) {
        $map = array('status' => array('gt', -1), 'id' => $id);
        $list[$id] = db('Action')->where($map)->field(true)->find();
    }
    return empty($field) ? $list[$id] : $list[$id][$field];
}

/**
 * 获取行为类型
 * @param intger $type 类型
 * @param bool $all 是否返回全部类型
 */
function get_action_type($type, $all = false) {
    $list = array(
        1 => '系统',
        2 => '用户',
    );
    if ($all) {
        return $list;
    }
    return $list[$type];
}

function getTpl($type = 'view') {
    $path = APP_PATH . DS . 'home' . DS . 'view' . DS . 'article';
    $lists = \CMS\Files::listFiles($path, $type . '.*\.html');
    if ($lists) {
        foreach ($lists as &$v) {
            $v = array(
                'tpl'   => $v,
                'title' => getTplName($v)
            );
        }
    }
    return $lists;
}

function getTplName($tpl = '') {
    if (!$tpl) {
        return '未知';
    }
    $name = $tpl;
    $path = APP_PATH . DS . 'home' . DS . 'view' . DS . 'article';
    $filename = $path . DS . $tpl;
    if (!is_file($filename) || !file_exists($filename)) {
        return $name;
    }
    $content = file_get_contents($filename);
    if ($content) {
        preg_match("/\<\!\-\-\[name\](.+?)\[\/name\]\-\-\>/i", trim($content), $mathes);
        if (!empty($mathes[1])) {
            $name = $mathes[1];
        }
    }
    return $name;
}

function get_sex_title($value = null) {
    $sex_lists = array(
        '0' => '保密', '1' => '男', '2' => '女'
    );
    return $sex_lists[$value];
}
