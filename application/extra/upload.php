<?php

/**
 * 上传驱动以及相应的驱动设置
 */
return [
    'driver'       => 'qiniu', //上传驱动器名称
    //上传的默认配置
    'default'      => [
        'mimes'    => '', // 允许上传的文件MiMe类型
        'maxSize'  => 2 * 1024 * 1024, // 上传的文件大小限制 (0-不做限制)
        'exts'     => 'jpg,gif,png,jpeg', // 允许上传的文件后缀
        'autoSub'  => true, // 自动子目录保存文件
        'subName'  => array('date', 'Y-m-d'), // 上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        'saveExt'  => '', // 文件保存后缀，空则使用原后缀
        'replace'  => false, // 存在同名是否覆盖
        'hash'     => true, // 是否生成hash编码
        'callback' => false, // 检测文件是否存在回调函数，如果存在返回文件信息数组
    ],
    //七牛的配置
    'qiniu_config' => [
        'secretKey' => 'xPXdTyhPB1h5RCeDJA9oN26bUiKthHVqma7qH3X7', //七牛SK
        'accessKey' => 'srWUaAGHIHW88KcjGGMiY6LDbf63lrJ5RFGmT2Oh', //七牛AK
        'domain'    => 'http://7xinin.com1.z0.glb.clouddn.com', //七牛域名
        'bucket'    => 'haolie', //空间名称
        'timeout'   => 300, //超时时间
        'urlpre'    => 'http://7xinin.com1.z0.glb.clouddn.com', //这里是一个独立的设置，用于实现系统中对附件URL的拼接
    ],
    //本地上传相关配置
    'local_config' => [
        'root_path' => './Uploads',
        'urlpre'    => '/Uploads'
    ],
];
