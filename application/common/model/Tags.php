<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id: Tags.php 17 2016-05-08 00:21:48Z IT果农 <htmambo@163.com> $
// +----------------------------------------------------------------------

namespace app\common\model;

use CMS\Model;

/**
 * 标签模型
 */
class Tags extends Model {

    /**
     * 新增或更新一则标签
     * @return boolean fasle 失败 ， int  成功 返回完整的数据
     */
    public function updatex(){
        /* 获取数据对象 */
        $data = input('post.');
        if(empty($data)){
            return false;
        }

        /* 添加或新增标签 */
        if(empty($data['id'])){ //新增数据
            $id = $this->save(); //添加
            if(!$id){
                $this->error = '新增标签出错！';
                return false;
            }
        } else { //更新数据
            $status = $this->isUpdate(true)->save(); //更新基础内容
            if(false === $status){
                $this->error = '更新标签出错！';
                return false;
            }
        }

        //内容添加或更新完成
        return $data;
    }

    /**
     * 搜索相关标签
     *@ param string 搜索关键字
     * @return array 相关标签
     */
    public function searchTags($keyword){
        $map["title"] = array("like", "%".$keyword."%");
        $tags = $this->field('id,title')->where($map)->select();
        return $tags;
    }

    /**
     * 根据ID将某文档拥有的原标签计数减一
     * @param  string  $orignal_tags   原标签
     */
    public function setDecByTags($orignal_tags = array()){
        if($orignal_tags){
            $this->where(array('title'=>array('in', $orignal_tags)))->setDec('count');
            return true;
        }else{
            return false;
        }
    }

    /**
     * 更新文档标签后更新标签表
     * @param  array  $tags   标签数据
     * @return array 标签数据
     */
    public function updateTags($tags = array()){
        foreach ($tags as $value) {
            $map['title'] = $value;
            if($this->where($map)->find()){
                $this->where($map)->setInc('count');
            }else{
               $this->add(array('title'=>$value, 'description'=>$value, 'create_time'=>NOW_TIME,'count'=>1));
            }
        }
        return $tags;
    }
}
