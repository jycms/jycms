<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id: Nav.php 17 2016-05-08 00:21:48Z IT果农 <htmambo@163.com> $
// +----------------------------------------------------------------------
namespace app\common\model;

use CMS\Model;

/**
 * 分类模型
 */
class Nav extends Model{

	/**
	 * 获取导航列表，支持多级导航
	 * @param  boolean $field 要列出的字段
	 * @return array          导航树
	 * @author IT果农 <htmambo@163.com>
	 */
	public function lists($field = true){
		$map = array('status' => 1);
		$list = $this->field($field)->where($map)->order('sort')->select();

		return list_to_tree($list, 'id', 'pid', '_');
	}

}
