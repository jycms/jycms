<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: CustomfieldsModel.class.php 9 2016-09-17 11:22:17Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------
namespace app\common\model;

use CMS\Model;

/**
 * 系统配置表模型
 */
class Customfields extends Model {

    protected function _after_find(&$result){
        $result['customfieldtext'] = unserialize($result['customfieldtext']);
    }
}
