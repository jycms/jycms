<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id$
// +----------------------------------------------------------------------

namespace app\common\model;
use CMS\Model;
/**
 * 配置模型
 * @author IT果农 <htmambo@163.com>
 */

class Config extends Model {
    public function getDbConfig() {
        $lists= $this->column('name,value,type', 'name');
        if($lists) {
            foreach($lists as $key=>&$val) {
                $val = $this->parse($val['type'], $val['value']);
            }
        }
        return $lists;
    }

    /**
     * 获取配置列表
     * @return array 配置数组
     */
    public function lists($map = array()){
        $map    = array('status' => 1);
        $data   = $this->where($map)->field('type,name,value')->select();

        $config = array();
        if($data && is_array($data)){
            foreach ($data as $value) {
                $config[$value['name']] = $this->parse($value['type'], $value['value']);
            }
        }
        return $config;
    }

    /**
     * 根据配置类型解析配置
     * @param  integer $type  配置类型
     * @param  string  $value 配置值
     */
    private function parse($type, $value){
        switch ($type) {
            case 3: //解析数组
                $array = preg_split('/[,;\r\n]+/', trim($value, ",;\r\n"));
                if(strpos($value,':')){
                    $value  = array();
                    foreach ($array as $val) {
                        list($k, $v) = explode(':', $val);
                        $value[$k]   = $v;
                    }
                }else{
                    $value =    $array;
                }
                break;
        }
        return $value;
    }

}
