<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: CategoriesModel.class.php 9 2016-09-17 11:22:17Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace app\common\model;

use CMS\Model;

/**
 * 分类模型
 */
class Categories extends Model {

    public function lists($map = array()) {
        $lists = parent::lists($map);
        $result = array();
        if($lists) {
            $tree = new \CMS\Tree($lists, 'catid', 'upid');
            //$tree->icon = array('&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;');
            $result = $tree->getTree();
        }
        return $result;
    }
    /**
     * 获取某分类下的所有子分类（所有级别）
     * @param int $catids
     * @return array
     */
    public function getChilds($catids = 0) {
        $lists = $this->lists();
        $result = array();
        if(!$lists || !$catids) {
            return $result;
        }
        if($catids) {
            if(!is_array($catids)) {
                $catids = array($catids);
            }
            foreach($catids as $catid)
            if(isset($lists[$catid])) {
                $cids = array();
                foreach($lists as $cat) {
                    if($cat['upid'] == $catid) {
                        $cids[] = $cat['catid'];
                    }
                }
                if($cids) {
                    foreach($cids as $catid) {
                        $result[] = $catid;
                        foreach($lists as $cat) {
                            if($cat['upid'] == $catid) {
                                $result[] = $cat['catid'];
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

}
