<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: AttachmentModel.class.php 9 2016-09-17 11:22:17Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace app\common\model;

use CMS\Model;
/**
 * 附件表模型
 */
class Attachment extends Model {
    /**
    * 查询完成后，生成附件对应的URL
    *
    * @param mixed $result
    */
    protected function _after_find(&$result) {
        global $Uploader;
        if(isset($result['driver']) && $result['driver'] && isset($result['attachment']) && $result['attachment']) {
            $driver = strtolower($result['driver']);
            $driverconfig = config('upload.'.$driver.'_config');
            if ($driver && $driverconfig) {
                $config = config('upload.default');
                if ($driver == 'local') {
                    $config = array_merge($config, $driverconfig);
                }
                $uid = 0;
                isset($result['uid']) && $uid = $result['uid'];
                $aid = 0;
                isset($result['aid']) && $aid = $result['aid'];
                $aidtype = 'article';
                isset($result['aidtype']) && $aidtype = $result['aidtype'];
                $config['saveName'] = $uid . '_' . $aidtype . '_' . $aid . '_' . uniqid();
                if(isset($Uploader[$driver])) {

                } else {
                    $Uploader[$driver]           = new \CMS\Upload($config, $driver, $driverconfig);
                }
                $result['url'] = $Uploader[$driver]->urlpre.$result['aidtype'].'/'.$result['attachment'];
                if(isset($result['thumb']) && $result['thumb']) {
                    $result['thumb_url'] = $result['url'].'.thumb.jpg';
                }
            }
        }
    }

}
