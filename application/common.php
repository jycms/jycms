<?php
// +----------------------------------------------------------------------
// | 简易CMS应用公共（函数）文件
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id$
// +----------------------------------------------------------------------
/**
 * 系统公共库文件
 * 主要定义系统公共函数库
 */
/**
 * 检测用户是否登录
 * @return integer 0-未登录，大于0-当前登录用户ID
 */
function is_login($full = false) {
    $user = session('user_auth');
    if (empty($user)) {
        return 0;
    } else {
        $result = session('user_auth_sign') == data_auth_sign($user) ? $user['uid'] : 0;
        if ($full) {
            if ($result) {
                $result = $user;
            } else {
                $result = array('uid' => 0, 'nickname' => '游客');
            }
        }
        return $result;
    }
}

/**
 * 检测当前用户是否为管理员
 * @return boolean true-管理员，false-非管理员
 */
function is_administrator($uid = null) {
    $uid = is_null($uid) ? is_login() : $uid;
    return $uid && (intval($uid) === config('database.administrator_id'));
}

/**
 * 检测验证码
 * @param  integer $id 验证码ID
 * @return boolean     检测结果
 */
function check_verify($code, $id = 1) {
    $verify = new \CMS\Verify();
    return $verify->check($code, $id);
}

/**
 * 字符串转换为数组，主要用于把分隔符调整到第二个参数
 * @param  string $str  要分割的字符串
 * @param  string $glue 分割符
 * @return array
 */
function str2arr($str, $glue = ',') {
    return explode($glue, $str);
}

/**
 * 数组转换为字符串，主要用于把分隔符调整到第二个参数
 * @param  array  $arr  要连接的数组
 * @param  string $glue 分割符
 * @return string
 */
function arr2str($arr, $glue = ',') {
    return implode($glue, $arr);
}

/**
 * 字符串截取，支持中文和其他编码
 * @static
 * @access public
 * @param string $str 需要转换的字符串
 * @param string $start 开始位置
 * @param string $length 截取长度
 * @param string $charset 编码格式
 * @param string $suffix 截断显示字符
 * @return string
 */
function msubstr($str, $start, $length, $charset = "utf-8", $suffix = true) {
    $str = trim($str);
    if (function_exists("mb_substr")) {
        $slice = mb_substr($str, $start, $length, $charset);
    } elseif (function_exists('iconv_substr')) {
        $slice = iconv_substr($str, $start, $length, $charset);
        if (false === $slice) {
            $slice = '';
        }
    } else {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        $match = array();
        preg_match_all($re[$charset], $str, $match);
        $slice = join("", array_slice($match[0], $start, $length));
    }
    if($suffix) {
        $suffix = strlen($slice)<strlen($str);
    }
    return $suffix ? $slice . '...' : $slice;
}

/**
 * 系统加密方法
 * @param string $data 要加密的字符串
 * @param string $key  加密密钥
 * @param int $expire  过期时间 单位 秒
 * @return string
 */
function think_encrypt($data, $key = '', $expire = 0) {
    $key = md5(empty($key) ? config('database.auth_key') : $key);
    $data = base64_encode($data);
    $x = 0;
    $len = strlen($data);
    $l = strlen($key);
    $char = '';

    for ($i = 0; $i < $len; $i++) {
        if ($x == $l)
            $x = 0;
        $char .= substr($key, $x, 1);
        $x++;
    }

    $str = sprintf('%010d', $expire ? $expire + time() : 0);

    for ($i = 0; $i < $len; $i++) {
        $str .= chr(ord(substr($data, $i, 1)) + (ord(substr($char, $i, 1))) % 256);
    }
    return str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($str));
}

/**
 * 系统解密方法
 * @param  string $data 要解密的字符串 （必须是think_encrypt方法加密的字符串）
 * @param  string $key  加密密钥
 * @return string
 */
function think_decrypt($data, $key = '') {
    $key = md5(empty($key) ? config('database.auth_key') : $key);
    $data = str_replace(array('-', '_'), array('+', '/'), $data);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    $data = base64_decode($data);
    $expire = substr($data, 0, 10);
    $data = substr($data, 10);

    if ($expire > 0 && $expire < time()) {
        return '';
    }
    $x = 0;
    $len = strlen($data);
    $l = strlen($key);
    $char = $str = '';

    for ($i = 0; $i < $len; $i++) {
        if ($x == $l)
            $x = 0;
        $char .= substr($key, $x, 1);
        $x++;
    }

    for ($i = 0; $i < $len; $i++) {
        if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))) {
            $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
        } else {
            $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
        }
    }
    return base64_decode($str);
}

/**
 * 数据签名认证
 * @param  array  $data 被认证的数据
 * @return string       签名
 */
function data_auth_sign($data) {
    //数据类型检测
    if (!is_array($data)) {
        $data = (array) $data;
    }
    ksort($data); //排序
    $code = http_build_query($data); //url编码并生成query字符串
    $sign = sha1($code); //生成签名
    return $sign;
}

/**
 * 对查询结果集进行排序
 * @access public
 * @param array $list 查询结果
 * @param string $field 排序的字段名
 * @param array $sortby 排序类型
 * asc正向排序 desc逆向排序 nat自然排序
 * @return array
 */
function list_sort_by($list, $field, $sortby = 'asc') {
    if (is_array($list)) {
        $refer = $resultSet = array();
        foreach ($list as $i => $data)
            $refer[$i] = &$data[$field];
        switch ($sortby) {
            case 'asc': // 正向排序
                asort($refer);
                break;
            case 'desc':// 逆向排序
                arsort($refer);
                break;
            case 'nat': // 自然排序
                natcasesort($refer);
                break;
        }
        foreach ($refer as $key => $val)
            $resultSet[] = &$list[$key];
        return $resultSet;
    }
    return false;
}

/**
 * 把返回的数据集转换成Tree
 * @param array $list 要转换的数据集
 * @param string $pid parent标记字段
 * @param string $level level标记字段
 * @return array
 */
function list_to_tree($list, $pk = 'id', $pid = 'pid', $child = '_child', $root = 0) {
    // 创建Tree
    $tree = array();
    if (is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] = & $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] = & $list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent = & $refer[$parentId];
                    $parent[$child][] = & $list[$key];
                }
            }
        }
    }
    return $tree;
}

/**
 * 将list_to_tree的树还原成列表
 * @param  array $tree  原来的树
 * @param  string $child 孩子节点的键
 * @param  string $order 排序显示的键，一般是主键 升序排列
 * @param  array  $list  过渡用的中间数组，
 * @return array        返回排过序的列表数组
 */
function tree_to_list($tree, $child = '_child', $order = 'id', &$list = array()) {
    if (is_array($tree)) {
        foreach ($tree as $key => $value) {
            $reffer = $value;
            if (isset($reffer[$child])) {
                unset($reffer[$child]);
                tree_to_list($value[$child], $child, $order, $list);
            }
            $list[] = $reffer;
        }
        $list = list_sort_by($list, $order, $sortby = 'asc');
    }
    return $list;
}

/**
 * 格式化字节大小
 * @param  number $size      字节数
 * @param  string $delimiter 数字和单位分隔符
 * @return string            格式化后的带单位的大小
 */
function format_bytes($size, $delimiter = '') {
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $size >= 1024 && $i < 5; $i++)
        $size /= 1024;
    return round($size, 2) . $delimiter . $units[$i];
}

/**
 * 设置跳转页面URL
 * 使用函数再次封装，方便以后选择不同的存储方式（目前使用cookie存储）
 */
function set_redirect_url($url) {
    cookie('redirect_url', $url);
}

/**
 * 获取跳转页面URL
 * @return string 跳转页URL
 */
function get_redirect_url() {
    $url = cookie('redirect_url');
    return empty($url) ? __APP__ : $url;
}

/**
 * 处理插件钩子
 * @param string $hook   钩子名称
 * @param mixed $params 传入参数
 * @return void
 */
function hook($hook, $params = array()) {
    \think\Hook::listen($hook, $params);
}

/**
 * 获取插件类的类名
 * @param strng $name 插件名
 */
function get_addon_class($name) {
    $class = "Addons\\{$name}\\{$name}Addon";
    return $class;
}

/**
 * 获取插件类的配置文件数组
 * @param string $name 插件名
 */
function get_addon_config($name) {
    $class = get_addon_class($name);
    if (class_exists($class)) {
        $addon = new $class();
        return $addon->getConfig();
    } else {
        return array();
    }
}

/**
 * 插件显示内容里生成访问插件的url
 * @param string $url url
 * @param array $param 参数
 */
function addons_url($url, $param = array()) {
    $url = parse_url($url);
    $case = config('URL_CASE_INSENSITIVE');
    $addons = $case ? parse_name($url['scheme']) : $url['scheme'];
    $controller = $case ? parse_name($url['host']) : $url['host'];
    $action = trim($case ? strtolower($url['path']) : $url['path'], '/');

    /* 解析URL带的参数 */
    if (isset($url['query'])) {
        parse_str($url['query'], $query);
        $param = array_merge($query, $param);
    }

    /* 基础参数 */
    $params = array(
        '_addons'     => $addons,
        '_controller' => $controller,
        '_action'     => $action,
    );
    $params = array_merge($params, $param); //添加额外参数

    return url('Addons/execute', $params);
}

/**
 * 时间戳格式化
 * @param int $time
 * @return string 完整的时间显示
 */
function time_format($time = NULL, $format = 'Y-m-d H:i') {
    $time = $time === NULL ? NOW_TIME : intval($time);
    return date($format, $time);
}

/**
 * 根据用户ID获取用户名
 * @param  integer $uid 用户ID
 * @return string       用户名
 */
function get_username($uid = 0) {
    static $list;
    if (!($uid && is_numeric($uid))) { //获取当前登录用户名
        return session('user_auth.username');
    }

    /* 获取缓存数据 */
    if (empty($list)) {
        $list = cache('sys_active_user_list');
    }

    /* 查找用户信息 */
    $key = "u{$uid}";
    if (isset($list[$key])) { //已缓存，直接使用
        $name = $list[$key];
    } else { //调用接口获取用户信息
        $User = new User\Api\UserApi();
        $info = $User->info($uid);
        if ($info && isset($info[1])) {
            $name = $list[$key] = $info[1];
            /* 缓存用户 */
            $count = count($list);
            $max = config('USER_MAX_CACHE');
            while ($count-- > $max) {
                array_shift($list);
            }
            cache('sys_active_user_list', $list);
        } else {
            $name = '';
        }
    }
    return $name;
}

/**
 * 根据用户ID获取用户昵称
 * @param  integer $uid 用户ID
 * @return string       用户昵称
 */
function get_nickname($uid = 0) {
    static $list;
    if (!($uid && is_numeric($uid))) { //获取当前登录用户名
        return session('user_auth.username');
    }

    /* 获取缓存数据 */
    if (empty($list)) {
        $list = config('sys_user_nickname_list');
    }

    /* 查找用户信息 */
    $key = "u{$uid}";
    if (isset($list[$key])) { //已缓存，直接使用
        $name = $list[$key];
    } else { //调用接口获取用户信息
        $info = db('Member')->field('nickname')->find($uid);
        if ($info !== false && $info['nickname']) {
            $nickname = $info['nickname'];
            $name = $list[$key] = $nickname;
            /* 缓存用户 */
            $count = count($list);
            $max = config('USER_MAX_CACHE');
            while ($count-- > $max) {
                array_shift($list);
            }
            cache('sys_user_nickname_list', $list);
        } else {
            $name = '';
        }
    }
    return $name;
}

/**
 * 记录行为日志，并执行该行为的规则
 * @param string $action 行为标识
 * @param string $model 触发行为的模型名
 * @param int $record_id 触发行为的记录id
 * @param int $user_id 执行行为的用户id
 * @return boolean
 */
function action_log($action = null, $model = null, $record_id = null, $user_id = null) {

    //参数检查
    if (empty($action) || empty($model) || empty($record_id)) {
        return '参数不能为空';
    }
    if (empty($user_id)) {
        $user_id = is_login();
    }

    //查询行为,判断是否执行
    $action_info = db('Action')->getByName($action);
    if ($action_info['status'] != 1) {
        return '该行为被禁用或删除';
    }

    //插入行为日志
    $data['action_id'] = $action_info['id'];
    $data['user_id'] = $user_id;
    $data['action_ip'] = ip2long(get_client_ip());
    $data['model'] = $model;
    $data['record_id'] = $record_id;
    $data['create_time'] = NOW_TIME;

    //解析日志规则,生成日志备注
    if (!empty($action_info['log'])) {
        if (preg_match_all('/\[(\S+?)\]/', $action_info['log'], $match)) {
            $log['user'] = $user_id;
            $log['record'] = $record_id;
            $log['model'] = $model;
            $log['time'] = NOW_TIME;
            $log['data'] = array('user' => $user_id, 'model' => $model, 'record' => $record_id, 'time' => NOW_TIME);
            foreach ($match[1] as $value) {
                $param = explode('|', $value);
                if (isset($param[1])) {
                    $replace[] = call_user_func($param[1], $log[$param[0]]);
                } else {
                    $replace[] = $log[$param[0]];
                }
            }
            $data['remark'] = str_replace($match[0], $replace, $action_info['log']);
        } else {
            $data['remark'] = $action_info['log'];
        }
    } else {
        //未定义日志规则，记录操作url
        $data['remark'] = '操作url：' . $_SERVER['REQUEST_URI'];
    }

    db('ActionLog')->insert($data);

    if (!empty($action_info['rule'])) {
        //解析行为
        $rules = parse_action($action, $user_id);

        //执行行为
        $res = execute_action($rules, $action_info['id'], $user_id);
    }
}

/**
 * 解析行为规则
 * 规则定义  table:$table|field:$field|condition:$condition|rule:$rule[|cycle:$cycle|max:$max][;......]
 * 规则字段解释：table->要操作的数据表，不需要加表前缀；
 *              field->要操作的字段；
 *              condition->操作的条件，目前支持字符串，默认变量{$self}为执行行为的用户
 *              rule->对字段进行的具体操作，目前支持四则混合运算，如：1+score*2/2-3
 *              cycle->执行周期，单位（小时），表示$cycle小时内最多执行$max次
 *              max->单个周期内的最大执行次数（$cycle和$max必须同时定义，否则无效）
 * 单个行为后可加 ； 连接其他规则
 * @param string $action 行为id或者name
 * @param int $self 替换规则里的变量为执行用户的id
 * @return boolean|array: false解析出错 ， 成功返回规则数组
 */
function parse_action($action, $self) {
    if (empty($action)) {
        return false;
    }

    //参数支持id或者name
    if (is_numeric($action)) {
        $map = array('id' => $action);
    } else {
        $map = array('name' => $action);
    }

    //查询行为信息
    $info = db('Action')->where($map)->find();
    if (!$info || $info['status'] != 1) {
        return false;
    }

    //解析规则:table:$table|field:$field|condition:$condition|rule:$rule[|cycle:$cycle|max:$max][;......]
    $rules = $info['rule'];
    $rules = str_replace('{$self}', $self, $rules);
    $rules = explode(';', $rules);
    $return = array();
    foreach ($rules as $key => &$rule) {
        $rule = explode('|', $rule);
        foreach ($rule as $k => $fields) {
            $field = empty($fields) ? array() : explode(':', $fields);
            if (!empty($field)) {
                $return[$key][$field[0]] = $field[1];
            }
        }
        //cycle(检查周期)和max(周期内最大执行次数)必须同时存在，否则去掉这两个条件
        if (is_array($return[$key]))
            if (!array_key_exists('cycle', $return[$key]) || !array_key_exists('max', $return[$key])) {
                unset($return[$key]['cycle'], $return[$key]['max']);
            }
    }

    return $return;
}

/**
 * 执行行为
 * @param array $rules 解析后的规则数组
 * @param int $action_id 行为id
 * @param array $user_id 执行的用户id
 * @return boolean false 失败 ， true 成功
 */
function execute_action($rules = false, $action_id = null, $user_id = null) {
    if (!$rules || empty($action_id) || empty($user_id)) {
        return false;
    }

    $return = true;
    foreach ($rules as $rule) {

        //检查执行周期
        $map = array('action_id' => $action_id, 'user_id' => $user_id);
        $map['create_time'] = array('gt', NOW_TIME - intval($rule['cycle']) * 3600);
        $exec_count = db('ActionLog')->where($map)->count();
        if ($exec_count > $rule['max']) {
            continue;
        }

        //执行数据库操作
        $Model = db(ucfirst($rule['table']));
        $field = $rule['field'];
        $res = $Model->where($rule['condition'])->setField($field, array('exp', $rule['rule']));

        if (!$res) {
            $return = false;
        }
    }
    return $return;
}

if (!function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $result = array();
        if (null === $indexKey) {
            if (null === $columnKey) {
                $result = array_values($input);
            } else {
                foreach ($input as $row) {
                    $result[] = $row[$columnKey];
                }
            }
        } else {
            if (null === $columnKey) {
                foreach ($input as $row) {
                    $result[$row[$indexKey]] = $row;
                }
            } else {
                foreach ($input as $row) {
                    $result[$row[$indexKey]] = $row[$columnKey];
                }
            }
        }
        return $result;
    }
}

/**
 * 友好的时间显示
 * @param int    $sTime 待显示的时间
 * @param string $type  类型. normal | mohu | full | ymd | other
 * @param string $alt   已失效
 * @return string
 */
function friendly_date($sTime, $type = 'normal', $alt = 'false') {
    if (!$sTime)
        return '';
    //sTime=源时间，cTime=当前时间，dTime=时间差
    $cTime = time();
    $dTime = $cTime - $sTime;
    $dDay = intval(date("z", $cTime)) - intval(date("z", $sTime));
    //$dDay     =   intval($dTime/3600/24);
    $dYear = intval(date("Y", $cTime)) - intval(date("Y", $sTime));
    //normal：n秒前，n分钟前，n小时前，日期
    if ($type == 'normal') {
        if ($dTime < 60) {
            if ($dTime < 10) {
                return '刚刚';
            } else {
                return intval(floor($dTime / 10) * 10) . "秒前";
            }
        } elseif ($dTime < 3600) {
            return intval($dTime / 60) . "分钟前";
            //今天的数据.年份相同.日期相同.
        } elseif ($dYear == 0 && $dDay == 0) {
            //return intval($dTime/3600)."小时前";
            return '今天' . date('H:i', $sTime);
        } elseif ($dYear == 0) {
            return date("m月d日 H:i", $sTime);
        } else {
            return date("Y-m-d H:i", $sTime);
        }
    } elseif ($type == 'mohu') {
        if ($dTime < 60) {
            return $dTime . "秒前";
        } elseif ($dTime < 3600) {
            return intval($dTime / 60) . "分钟前";
        } elseif ($dTime >= 3600 && $dDay == 0) {
            return intval($dTime / 3600) . "小时前";
        } elseif ($dDay > 0 && $dDay <= 7) {
            return intval($dDay) . "天前";
        } elseif ($dDay > 7 && $dDay <= 30) {
            return intval($dDay / 7) . '周前';
        } elseif ($dDay > 30) {
            return intval($dDay / 30) . '个月前';
        }
        //full: Y-m-d , H:i:s
    } elseif ($type == 'full') {
        return date("Y-m-d , H:i:s", $sTime);
    } elseif ($type == 'ymd') {
        return date("Y-m-d", $sTime);
    } else {
        if ($dTime < 60) {
            return $dTime . "秒前";
        } elseif ($dTime < 3600) {
            return intval($dTime / 60) . "分钟前";
        } elseif ($dTime >= 3600 && $dDay == 0) {
            return intval($dTime / 3600) . "小时前";
        } elseif ($dYear == 0) {
            return date("Y-m-d H:i:s", $sTime);
        } else {
            return date("Y-m-d H:i:s", $sTime);
        }
    }
    return date("Y-m-d H:i", $sTime);
}

/**
 * 转换参数为字符串
 *
 * @param anytype $data 要转换的数据，目前主要是针对数组、对象
 * @return string 转换后的数据，主要用来进行错误报告的处理
 */
function anytypetostring($data = '') {
    if ($data) {
        if (is_string($data)) {
            return $data;
        } else {
            return print_r($data, 1);
        }
    } else {
        return '{EMPTY}';
    }
}

/**
 * 生成随机值
 *
 * @param mixed $length
 * @param mixed $numeric
 */
function random($length, $numeric = 0) {
    PHP_VERSION < '4.2.0' && mt_srand((double) microtime() * 1000000);
    if ($numeric) {
        $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
    } else {
        $hash = '';
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
    }
    return $hash;
}

/**
 * 格式化输出信息
 *
 * @param 字符串/数组 $array 要输出的信息
 * @param 逻辑值 $exit 是否需要退出
 */
function pre($array, $exit = false) {
    if ($array) {
        if (is_string($array)) {
            echo '<br>';
            echo htmlspecialchars($array);
            echo '<br>';
        } else {
            echo "<div style='font-size:12px;line-height:14px;text-align:left;color:#000;background-color:#fff;'><pre>";
            print_r($array);
            echo "</pre></div>";
        }
    }
    if ($exit) {
        E('程序调试断点！', 222);
    }
}

function strexists($string, $find) {
    return !(strpos($string, $find) === FALSE);
}

/**
 * 记录来访的蜘蛛
 *
 * @return unknown
 */
function robotlog() {
    global $_G, $db;
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    $remoteaddr = get_client_ip();
    $tdate = date("Y-m-d", NOW_TIME);
    //$useragent = 'Mozilla/5.0 (compatible; archive.org_bot +http://www.archive.org/details/archive.org_bot)';
    $regex = "(!:[\\/\\-]?\\d+(\\.\\d+)+)?";
    $regex = "/[a-z\\s!]*?[\\w\\-]*(?:Bot|Spider|Twiceler|Crawl|ia_archiver|Slurp|ZyBorg|MSIECrawler|UdmSearch|IRLbo)[a-z\\-]*[a-z\\s]*{$regex}/i";
    preg_match($regex, $useragent, $matches);
    if ($matches == false) {
        return false;
    }
    $robotName = trim($matches[0], " \\-");
    $remoteaddr = $_SERVER['REMOTE_ADDR'];
    $model = model('Spider');
    $ipmodel = model('SpiderIp');
    $logmodel = model('SpiderLog');
    $row = $model->where("name='$robotName'")->find();
    if ($row) {
        $data = array(
            'times'     => $row['times'] + 1,
            'last_vist' => NOW_TIME
        );
        $model->where("name='$robotName'")->save($data);
    } else {
        $data = array(
            'name'        => $robotName,
            'times'       => 1,
            'first_visit' => NOW_TIME,
            'last_visit'  => NOW_TIME,
            'agent'       => $useragent
        );
        $model->add($data);
    }
    $row = $ipmodel->where("ip='$remoteaddr'")->find();
    if ($row) {
        $data = array(
            'times'     => $row['times'] + 1,
            'last_vist' => NOW_TIME
        );
        $ipmodel->where("ip='$remoteaddr'")->save($data);
    } else {
        $data = array(
            'ip'          => $remoteaddr,
            'name'        => $robotName,
            'times'       => 1,
            'first_visit' => NOW_TIME,
            'last_visit'  => NOW_TIME
        );
        $ipmodel->add($data);
    }
    $row = $logmodel->where("name='$robotName' AND date='$tdate'")->find();
    if ($row) {
        $data = array(
            'times'     => $row['times'] + 1,
            'last_vist' => NOW_TIME
        );
        $logmodel->where("name='$robotName' AND date='$tdate'")->save($data);
    } else {
        $data = array(
            'name'        => $robotName,
            'times'       => 1,
            'date'        => $tdate,
            'first_visit' => NOW_TIME,
            'last_visit'  => NOW_TIME
        );
        $logmodel->add($data);
    }
}

/**
 * 处理一些自定义的值
 *
 * @param mixed $value
 * @param mixed $direct
 */
function processvalue($value, $direct = false) {
    $value = strtoupper($value);
    switch ($value) {
        case 'CURRENT_DATE':
            if ($direct)
                $result = '当前日期';
            else
                $result = $_SERVER['today'];
            break;
        default:
            $result = $value;
            break;
    }
    return $result;
}

function processOptions($str, $mode = 'select') {
    if ($mode == 'select' || $mode == 'select-input')
        $options = array(
            '' => '请选择'
        );
    else
        $options = array();
    if (!$str)
        return $options;
    if (!is_array($str)) {
        if (substr($str, 0, 4) == 'SQL:') {
            $sql = substr($str, 4);
            DB::query($sql);
            while ($row = DB::fetch()) {
                $options[$row['value']] = $row['text'];
            }
        } elseif (substr($str, 0, 1) == '{') {
            $var = str_replace(array(
                '{',
                '}'
                ), '', $str);
            $vv = eval("return $var;");
            //pre($vv);exit;
            if (!is_array($vv)) {
                $vv = explode("\n", $vv);
                foreach ($vv as $k => $v) {
                    $v = trim($v);
                    list( $key, $value) = explode('=', $v);
                    if (!$value) {
                        $value = $key;
                        $key = $k;
                    }
                    $options[$key] = $value;
                }
            } else {
                //$options = array_merge($options, $vv);
                foreach ($vv as $k => $v) {
                    $options[$k] = $v;
                }
            }
        } else {
            $nokey = 0;
            if (substr($str, 0, 1) == '@') {
                $str = substr($str, 1);
                $nokey = 1;
            }
            //$options = array();
            $vv = explode("\n", $str);
            foreach ($vv as $k => $v) {
                $v = trim($v);
                if ($v) {
                    list( $key, $value) = explode('=', $v);
                    if (!$value) {
                        $value = $key;
                        $key = $nokey ? $key : $k;
                    }
                    $options[$key] = $value;
                }
            }
        }
    } else {
        //pre($str, 1);
        //$options = array_merge($options, $str);
        foreach ($str as $k => $v) {
            $key = $k;
            $text = $v;
            if (is_array($v)) {
                if (isset($v['value']))
                    $key = $v['value'];
                if (isset($v['text']))
                    $text = $v['text'];
            }
            $options[$key] = $text;
        }
    }
    return $options;
}

/**
 * 系统非常规MD5加密方法
 * @param  string $str 要加密的字符串
 * @return string
 */
function user_md5($str, $key = '') {
    if (!$key) {
        $key = config('database.auth_key');
    }
    return '' === $str ? '' : md5(sha1($str) . $key);
}

/**
 * 请求远程地址
 *
 * @param string $url 要请求的网址
 * @param array $data 附加的参数
 * @param string $type 请求方式，默认为GET
 * @param array $header 附加的头信息
 * @param string $request_engine 引擎，默认CURL
 */
function doRequest($url, $data, $type = 'get', $header = array(), $request_engine = 'curl') {
    if ($data) {
        $data = http_build_query($data);
    }
    if ($type == 'get') {
        $url .= strpos($url, '?') ? '&' : '?';
        $url .= $data;
        $data = '';
    }
    if ($request_engine == 'curl' && function_exists('curl_init')) {
        $header[] = 'Expect:';
        if ($data) {
            $header[] = 'Content-length: ' . strlen($data);
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, true);
        if ($data) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_USERAGENT, 'haolie.net/1.0 (+http://www.haolie.net/)');
        curl_setopt($curl, CURLOPT_REFERER, 'http://www.haolie.net');
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
    } else {
        if ($data) {
            $header[] = 'Content-length: ' . strlen($data);
        }
        $headers = implode("\r\n", $header);
        $params = array('http' => array(
                'protocol_version' => '1.1',
                'method'           => 'POST',
                'header'           => $headers,
                'content'          => $data,
                'timeout'          => 30,
        ));
        $ctx = stream_context_create($params);
        $response = file_get_contents($url, false, $ctx);
    }
    return $response;
}

/**
 * 检测当前是否在微信中打开
 * @return boolean
 */
function isInWechat() {
    $ua = I('server.HTTP_USER_AGENT');
    return strpos($ua, 'MicroMessenger') !== false;
}

/**
 * 页面未找到等的处理方式，直接输出状态码
 *
 * @param mixed $code
 */
function _exit($msg = '', $code = 404) {
    send_http_status($code);
    $file = dirname(__FILE__) . '/404.html';
    if (!$msg) {
        $msg = '抱歉,你要找的页面没有找到,可能是没有上传,也可能是被删除.';
    }
    if (file_exists($file)) {
        $tpl = new \think\Template();
        $msg = $tpl->fetch($file, array('msg' => $msg));
    }
    exit($msg);
}

/**
 * 是否可以在线调试
 *
 */
function canTest() {
    if (I('server.SERVER_NAME') == 'localhost') {
        return true;
    }
    $info = dns_get_record('hoping.vicp.net');
    if ($info[0]['ip']) {
        return get_client_ip() == $info[0]['ip'];
    }
    return false;
}

function array_map_recursive($filter, $data) {
    $result = array();
    foreach ($data as $key => $val) {
        $result[$key] = is_array($val) ? array_map_recursive($filter, $val) : call_user_func($filter, $val);
    }
    return $result;
}

function in_array_case($value, $array) {
    return in_array(strtolower($value), array_map('strtolower', (array) $array));
}

/**
 * 字符串命名风格转换
 * type 0 将Java风格转换为C的风格 1 将C风格转换为Java的风格
 * @param string $name 字符串
 * @param integer $type 转换类型
 * @return string
 */
function parse_name($name, $type = 0) {
    if ($type) {
        return ucfirst(preg_replace_callback('/_([a-zA-Z])/', function($match) { return strtoupper($match[1]); }, $name));
    } else {
        return strtolower(trim(preg_replace("/[A-Z]/", "_\\0", $name), "_"));
    }
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
 * @return mixed
 */
function get_client_ip($type = 0, $adv = false) {
    $request = \think\Request::instance();
    return $request->ip($type, $adv);
}

/**
 * 获取输入数据 支持默认值和过滤
 * @param string $key 获取的变量名
 * @param mixed $default 默认值
 * @param string $filter 过滤方法
 * @param bool $merge 是否合并系统默认过滤方法
 * @return mixed
 */
function I($name, $default = '', $filter = null, $datas = null) {
    return input($name, $default, $filter, $datas);
}

/**
 * 实例化多层控制器 格式：[资源://][模块/]控制器
 * @param string $name 资源地址
 * @param string $layer 控制层名称
 * @param integer $level 控制器层次
 * @return Think\Controller|false
 */
function A($name, $layer = '', $level = 0)
{
    static $_action = array();
    $level          = $level ?: (config('url_controller_layer') == $layer ? config('CONTROLLER_LEVEL') : 1);
    if (isset($_action[$name . $layer])) {
        return $_action[$name . $layer];
    }

    $class = parse_res_name($name, $layer, $level);
    $action = new $class();

    if (class_exists($class)) {
        $action                  = new $class();
        $_action[$name . $layer] = $action;
        return $action;
    } else {
        return false;
    }
}

/**
 * 远程调用控制器的操作方法 URL 参数格式 [资源://][模块/]控制器/操作
 * @param string $url 调用地址
 * @param string|array $vars 调用参数 支持字符串和数组
 * @param string $layer 要调用的控制层名称
 * @return mixed
 */
function R($url, $vars = array(), $layer = '')
{
    $info   = pathinfo($url);
    $action = $info['basename'];
    $module = $info['dirname'];
    $class  = A($module, $layer);
    if ($class) {
        if (is_string($vars)) {
            parse_str($vars, $vars);
        }
        return call_user_func_array(array(&$class, $action . config('action_suffix')), $vars);
    } else {
        return false;
    }
}

/**
 * 解析资源地址并导入类库文件
 * 例如 module/controller addon://module/behavior
 * @param string $name 资源地址 格式：[扩展://][模块/]资源名
 * @param string $layer 分层名称
 * @param integer $level 控制器层次
 * @return string
 */
function parse_res_name($name, $layer, $level = 1)
{
    if (strpos($name, '://')) {
        // 指定扩展资源
        list($extend, $name) = explode('://', $name);
    } else {
        $extend = '';
    }
    if (strpos($name, '/') && substr_count($name, '/') >= $level) {
        // 指定模块
        list($module, $name) = explode('/', $name, 2);
    } else {
        $module = defined('MODULE_NAME') ? MODULE_NAME : '';
    }
    $module = strtolower($module);
    $array = explode('/', $name);
    $class = config('app_namespace').'\\'.$module . '\\controller';
    foreach ($array as $name) {
        $class .= '\\' . parse_name($name, 1);
    }
    // 导入资源类库
    if ($extend) {
        // 扩展资源
        $class = $extend . '\\' . $class;
    }
    return $class . $layer;
}


function category_remake($catid) {
    $cModel = model('Categories');
    $cats = $cModel->lists();

    if(!isset($cats[$catid]) || !$cats[$catid]) {
        return array();
    }
    $cat = $cats[$catid];
    if(empty($cat)) return array();

    foreach ($cats as $value) {
        if($value['catid'] == $cat['upid']) {
            $cat['ups'][$value['catid']] = $value;
            $upid = $value['catid'];
            while(!empty($upid)) {
                if(!empty($cats[$upid]['upid'])) {
                    $upid = $cats[$upid]['upid'];
                    $cat['ups'][$upid] = $cats[$upid];
                } else {
                    $upid = 0;
                }
            }
        } elseif($value['upid'] == $cat['catid']) {
            $cat['subs'][$value['catid']] = $value;
        } elseif($value['upid'] == $cat['upid']) {
            $cat['others'][$value['catid']] = $value;
        }
    }
    if(!empty($cat['ups'])) $cat['ups'] = array_reverse($cat['ups'], TRUE);
    return $cat;
}

/**
 * 获取正文中的图片列表
 *
 * @param 数组 $attach 附件列表
 * @return 附件聊表HTML代码
 */
function get_uploadcontent($attach) {
    $result = '';
    $result .= '<li id="attach_list_' . $attach['attachid'] . '" class="lists clearfix">';
    if ($attach['isimage']) {
        $result .= '<span class="thumb fl"><a href="' . $url . '" target="_blank"><img src="' . ($attach['thumb_url'] ? : $attach['url']) . '" ></a></span>';
        if ($attach['thumb_url']) {
            $result .= '<button class="btn" onclick="insertImage(\'' . $attach['thumb_url'] . '\', \'' . $attach['url'] . '\');return false;">插入小图</button>';
        }
        $result .= '<button class="btn" onclick="insertImage(\'' . $attach['url'] . '\');return false;">插入大图</button>';
    } else {
        $result .= '<span class="thumb fl"><a href="'.url('Home/Attachment/download', array('id'=>$attach['attachid'])) . '" target="_blank">' . $attach['filename'] . '</a></span>';
        $result .= '<button class="btn" onclick="insertFile(\'' . $attach['filename'] . '\', \'' . url('Home/Attachment/download', array('id'=>$attach['attachid'])) . '\');return false;">插入文件链接</button>';
    }
    $result .= '<button class="btn" onclick="deleteAttach(\'' . $attach['attachid'] . '\', \'' . url('Home/Attachment/delete', array('id'=>$attach['attachid'], 'aid' => $aid)) . '\');return false;">删除</button>';
    $result .= '</li>';
    return $result;
}
