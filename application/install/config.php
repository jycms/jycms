<?php
// +----------------------------------------------------------------------
// | 简易CMS
// +----------------------------------------------------------------------
// | Copyright (C) 2010 Hoping Software Studio.
// +----------------------------------------------------------------------
// | Version $Id$
// +----------------------------------------------------------------------

define('INSTALL_APP_PATH', realpath('./') . DS);

return [
    'ORIGINAL_TABLE_PREFIX' => 'jycms_', //原前缀
    'response_auto_output'   => false,      //兼容ThinkPHP 3.2.3模式
    // 默认控制器名
    'default_controller'     => 'Index',
    //
    /* SESSION 和 COOKIE 配置 */
    'session'                => [
        'id'             => '',
        // 驱动方式 支持redis memcache memcached
        'type'           => '',
        // 是否自动开启 SESSION
        'auto_start'     => true,
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => 'session_id',
        // SESSION 前缀
        'prefix'         => 'jycms_admin'
    ],
];
