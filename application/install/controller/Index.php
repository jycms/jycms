<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace app\install\controller;
use CMS\Controller;

class Index extends Controller{
    //安装首页
    public function index(){
        if(is_file(RUNTIME_PATH.'/install.lock')){
            // 已经安装过了 执行更新程序
            session('update',true);
            $msg = '请删除install.lock文件后再运行升级!';
        }else{
            $msg = '已经成功安装了JYCMS，请不要重复安装!';
        }

        if(\CMS\Files::file_exists(RUNTIME_PATH.'/install.lock')){
            $this->error($msg, CMS_URL);
        }
        return $this->fetch();
    }

    //安装完成
    public function complete(){
        $step = session('step');

        if(!$step){
            $this->redirect('index');
        } elseif($step != 3) {
            $this->redirect("Install/step{$step}");
        }

        // 写入安装锁定文件
        \CMS\Files::write(RUNTIME_PATH.'/install.lock', 'lock');
        if(!session('update')){
            //创建配置文件
            $this->assign('info',session('config_file'));
        }
        session('step', null);
        session('error', null);
        session('update',null);
        session('user_auth', null);
        session('user_auth_sign', null);
        return $this->fetch();
    }
}
