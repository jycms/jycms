<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: Block.class.php 9 2016-09-17 11:22:17Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace CMS;

class Block {
	var $parameter;
    var $_view;

	function __construct( $parameter ) {
        if(is_string($parameter)) {
            $parameter = unserialize($parameter);
        }
                $this->_view = \think\View::instance();
        foreach($parameter as $k => &$v) {
            if(substr($v, 0, 1)==='$') {
                $v = $this->_view->engine->get(substr($v, 1));
            }
        }
		/**
		 * 处理一下条目数的设置
		 */
		if (isset($parameter['limit'])) {
			$limit = explode(',', $parameter['limit']);
			if (count($limit) > 1) {
				$parameter['items'] = $limit[1];
				$parameter['startrow'] = $limit[0];
			} else {
				$parameter['items'] = intval($parameter['limit']);
			}
		}
		if (isset($parameter['order']) && !isset($parameter['orderby'])) {
			$order = explode(' ', $parameter['order']);
			if ($order) {
				$parameter['orderby'] = $order[0];
			}
			if (isset($order[1])) {
				$parameter['order'] = strtoupper($order[1]);
			} else {
				$parameter['order'] = 'ASC';
			}
		}else if (isset($parameter['orderby']) && !isset($parameter['order'])) {
			$order = explode(' ', $parameter['orderby']);
			if ($order) {
				$parameter['orderby'] = $order[0];
			}
			if (isset($order[1])) {
				$parameter['order'] = strtoupper($order[1]);
			} else {
				$parameter['order'] = 'ASC';
			}
		}
		$this->parameter = $parameter;
	}

	function fields(){
		return array();
	}
	function getData(){
		return array();
	}
}