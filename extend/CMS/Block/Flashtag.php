<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: Flashtag.class.php 9 2016-09-17 11:22:17Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace CMS\Block;

/**
 * @package		简易CMS
 * @subpackage	标签云模块
 */
class Flashtag extends \CMS\Block {

	function getdata() {
		global $_G;
		$parameter = $this->parameter;
		$speed = isset($parameter['speed']) ? intval($parameter['speed']) : 100;
		if (!$speed)
			$speed = 100;
		$width = isset($parameter['width']) ? intval($parameter['width']) : 290;
		if (!$width)
			$width = 290;
		$height = isset($parameter['height']) ? intval($parameter['height']) : 240;
		if (!$height)
			$height = 240;
		$items = isset($parameter['items']) ? intval($parameter['items']) : 100;
		if (!$items)
			$items = 100;

		$toptags = '';
		$model = db('Tagslist');
		$tags = $model->order('allnum DESC')->limit($items)->select();
		$times = $min = $max = 0;
		$min = 1;
		foreach ((array) $tags as $k => $row) {
			$times+=$row['allnum'];
			$min = min($min, $row['allnum']);
			$max = max($max, $row['allnum']);
			$tags[$k] = $row;
		}
		$xmlfile = CMS_TMP . '/tagcloud.xml';
		$xmlcontent = '<?xml version="1.0" encoding="UTF-8"?><root>';
		if ($tags) {
			$per = intval(($max - $min) / 10);
			if (!$per)
				$per = 1;
			foreach ($tags as $v) {
				$link = url('Misc/tag', array('tag'=>$v['tagname']));
				$fs = intval(($v['allnum'] - $min) / $per) * 10;
				$xmlcontent .= '<a href="' . $link . '" type="text" detail="' . $v['tagname'] . '" weight="' . $fs . '" />';
			}
		}
		$xmlcontent .= '</root>';
		if ($tags) {
			$xmlcontent = str_replace('<a href=', '<item link=', $xmlcontent);
		}
		$result = \CMS\Files::write($xmlfile, $xmlcontent);
		$html = '';
		if ($tags) {
			if (!isset($_SERVER['__flashtag__'])) {
				$_SERVER['__flashtag__'] = true;
				$html = '<script type="text/javascript" src="__JS__/swf.js"></script>';
			} else {
				$html = '';
			}
			$rstr = random(6);
			$html .='
	<div id="flashtag' . $rstr . '"><p></p></div>
	<script type="text/javascript">
                       baidu.swf.create(
                            {
                                \'id\'                            : "TrendAnalyser",
                                \'width\'                         : "' . $width . '",
                                \'height\'                        : "' . $height . '",
                                \'ver\'                           : "9.0.0",
                                \'errorMessage\'                  : "请下载最新的Flash播放器！",
                                \'url\'                           : "__JS__/tagCloud.swf",
                                \'vars\'                          : "xml=' . CMS_TMP_URL . '/tagcloud.xml",
                                \'bgcolor\'                       : "#FFFFFF",
                                \'wmode\'                         : "transparent",
                                \'allowscriptaccess\'             : "always"
                            },
                            "flashtag' . $rstr . '");
	</script>';
		}
		return array('html' => $html, 'data' => $tags);
	}

}
