<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: Article.class.php 32 2016-10-23 16:45:40Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace CMS\Block;

/**
 * @package        JYCMS
 * @subpackage    文章模块
 */
class Article extends \CMS\Block {
    function getdata() {
        $parameter = $this->parameter;
        $aids = array();
        if (isset($parameter['aids'])) {
            $aids = explode(',', str_replace(array('<?=', '?>'), '', $parameter['aids']));
        }
        $uids = !empty($parameter['uids']) ? explode(',', $parameter['uids']) : array();
        $starttime = !empty($parameter['starttime']) ? strtotime($parameter['starttime']) : 0;
        $endtime = !empty($parameter['endtime']) ? strtotime($parameter['endtime']) : 0;
        $startrow = isset($parameter['startrow']) ? intval($parameter['startrow']) : 0;
        $items = isset($parameter['items']) ? intval($parameter['items']) : 10;
        $page = (isset($parameter['page']) && $parameter['page'] == 'true') ? true : false;
        $orderby = in_array($parameter['orderby'], array('dateline', 'viewnum', 'commentnum')) ? $parameter['orderby'] : 'dateline';
        $order = in_array($parameter['order'], array('ASC', 'DESC')) ? $parameter['order'] : 'DESC';
        $return = $parameter['return'];
        $catid = array();
        if (!empty($parameter['catid'])) {
            if (!is_array($parameter['catid']))
                $parameter['catid'] = explode(',', $parameter['catid']);
            if ($parameter['catid'][0] == '0') {
                unset($parameter['catid'][0]);
            }
            $catid = $parameter['catid'];
        }

        $picrequired = !empty($parameter['picrequired']) ? 1 : 0;

        $list = array();
        $wheres = array();
        if (isset($parameter['rank'])) {
            $wheres[] = 'at.rank=' . intval($parameter['rank']);
        }
        if ($aids) {
            $wheres[] = 'at.aid IN (' . implode(',', $aids) . ')';
        }
        if ($uids) {
            $wheres[] = 'at.uid IN (' . implode(',', $uids) . ')';
        }
        if ($catid) {
            $childids = model('Categories')->getChilds($catid);
            $catid = array_merge($catid, $childids);
            $catid = array_unique($catid);
            $wheres[] = 'at.catid IN (' . implode(',', $catid) . ')';
        }
        if ($starttime) {
            $wheres[] = "at.dateline >= '$starttime'";
        }
        if ($endtime) {
            $wheres[] = "at.dateline <= '$endtime'";
        }
//        if ($picrequired) {
//            $wheres[] = "at.pic!=''";
//        }
        $wheres[] = "at.status='1'";
        $wheresql = $wheres ? implode(' AND ', $wheres) : '1';

        $model = model('ArticleTitle');
        $html = '';
        if ($page) {
            $total = input('total', 0, 'intval');
            if (!$total) {
                $total = $model->where($wheresql)->alias('at')->count();
            }
            $pgObj = new \CMS\Pager($total, $items, array('total' => $total));
            $startrow = $pgObj->firstRow;
            $html = $pgObj->show();
        }

        $result = $model->alias('at')
            ->join('__CATEGORIES__ c', 'c.catid=at.catid', 'LEFT')
            ->group('aid')
            ->where($wheresql)
            ->order($orderby . ' ' . $order)
            ->limit($startrow, $items)
            ->field('at.*, c.catname')
            ->select();
        //dump($model);
        $aids = $data = array();
        if ($result) {
            foreach ($result as $row) {
                if (isset($row['pic']) && $row['pic']) {
                    $row['pic'] = CMS_URL . 'Data/Uploads/' . $row['pic'];
                }
                if (!$row['author']) {
                    $row['author'] = $row['username'];
                }
                $row['dateline'] = friendly_date($row['dateline']);
                $row['url'] = url('home/article/view', array('aid' => $row['aid'], 'alias' => $row['shorttitle']));
                $row['caturl'] = url('home/article/list', array('catid' => $row['catid']));
                $data[$row['aid']] = $row->toArray();
                $aids[] = $row['aid'];
            }
            $aids = implode(',', $aids);
            $result = db('Tags')->alias('a')
                ->join('__TAGSLIST__ b', 'b.tagid=a.tagid', 'LEFT')
                ->where("a.itemid IN ($aids) AND a.tagtype='portal'")
                ->select();
            if ($result) {
                foreach ($result as $v) {
                    $data[$v['itemid']]['tags'][] = array('tag' => $v['tagname'], 'tagid' => $v['tagid']);
                }
            }
            $model = model('Attachment');
            $result = $model->where('aid IN (' . $aids . ') AND aidtype="article" AND isimage=1')->select();
            if ($result) {
                foreach ($result as $v) {
                    $data[$v['aid']]['pics'][] = (isset($v['thumb_url']) && $v['thumb_url'])?$v['thumb_url'] : $v['url'];
                }
                foreach ($data as $k => &$v) {
                    $pics = 0;
                    isset($v['pics']) && $v['picnum'] = count($v['pics']);
                    if (isset($v['pic']) && $v['pic']) {
                        if ($v['picnum'] >= 4) {
                            $v['pics'] = array_splice($v['pics'], 0, 4);
                        } elseif ($v['picnum'] > 0) {
                            $v['pics'] = array($v['pics'][0]);
                        }
                        if ($v['picnum'] > 0) {
                            $v['pic'] = $v['pics'][0];
                        } else {
                            $v['pic'] = '';
                        }
                    }
                }
            }
        }
        return array('data' => $data, 'html' => $html);
    }

}
