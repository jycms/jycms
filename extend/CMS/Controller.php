<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------
namespace CMS;

use think\Response;

class Controller extends \think\Controller {
    /**
     * 页面标题
     * @var string
     */
    public $pageTitle = '';

    /**
     * 快捷链接
     * @var array
     */
    public $quickLink = array();


    /**
     * 系统初始化，将配置信息加入到内存变量中
     *
     */
    protected function _initialize() {
        $model = $this->request->module();
        if(strtolower($model)==='install') {
            return;
        }
        /* 读取数据库中的配置 */
        $config = cache('DB_CONFIG_DATA');
        if (!$config) {
            $config = model('Config')->getDbConfig();
            cache('DB_CONFIG_DATA', $config);
        }
        config($config); //添加配置
    }

    /**
     * 加载模板输出
     * @access protected
     * @param string    $template 模板文件名
     * @param array     $vars     模板输出变量
     * @param array     $replace     模板替换
     * @param array     $config     模板参数
     * @return mixed
     */
    protected function fetch($template = '', $vars = [], $replace = [], $config = []) {
        //一些已经习惯使用的模板变量
        $module = $this->request->module();
        $this->assign('module', $module);
        $this->assign('controller', $this->request->controller());
        $this->assign('action', $this->request->action());
        $_replace = array(
            '__ROOT__'=> CMS_URL,
            '__STATIC__'=> CMS_URL.'static',
            '__CSS__'=> CMS_URL.'static/'.$module.'/css',
            '__IMG__'=> CMS_URL.'static/'.$module.'/images',
            '__JS__'=> CMS_URL.'static/'.$module.'/js'
        );
        $replace = array_merge($replace, $_replace);
        if ($this->pageTitle) {
            $this->assign('pageTitle', $this->pageTitle);
        }
        if ($this->quickLink) {
            if (isset($this->quickLink['form']) && $this->quickLink['form']) {
                $this->assign('quickForm', $this->quickLink['form']);
                unset($this->quickLink['form']);
            }
            $selfUrl = url();
            foreach ($this->quickLink as $k => &$v) {
                if (isset($v['sub']) && $v['sub']) {
                    foreach ($v['sub'] as $kk => &$vv) {
                        if ($vv['hidden']) {
                            unset($v['sub'][$kk]);
                        } elseif ($vv['link'] == $selfUrl) {
                            $vv['class'] = 'disabled';
                            $vv['link']  = '#';
                        }
                    }
                    if (isset($vv['confirm'])) {
                        $vv['confirm'] = str_replace('"', "'", $vv['confirm']);
                        $vv['confirm'] = str_replace("'", "\'", $vv['confirm']);
                    }
                    if (!$v['sub']) {
                        unset($this->quickLink[$k]);
                    }
                } else {
                    if (isset($v['hidden']) && $v['hidden']) {
                        unset($this->quickLink[$k]);
                    } else{
                        $disabled = false;
                        if(isset($v['disabled'])) {
                            $disabled = $v['disabled'];
                        } else if ($v['link'] == $selfUrl) {
                            $disabled = true;
                        }
                        if($disabled) {
                            $v['class'] = 'disabled';
                            $v['link']  = '#';
                        }
                    }
                }
                if (isset($v['confirm'])) {
                    $v['confirm'] = str_replace('"', "'", $v['confirm']);
                    $v['confirm'] = str_replace("'", "\'", $v['confirm']);
                }
            }
            $this->assign('quickLink', $this->quickLink);
        }
        //一些常变量
        $this->assign('yesorno', array('1' => '是', '0' => '否'));
        $this->assign('sexlist', array('0' => '保密', '1' => '男', '2' => '女'));
        return $this->view->fetch($template, $vars, $replace, $config);
    }

    /**
     * 显示验证码
     * @param boolean $useHz 是否使用汉字验证码
     */
//     public function Verify($useHz = false) {
//         $verify = new \CMS\Verify(array('fontSize' => 25, 'length' => 4, 'imageH' => 30, 'imageW' => 120));
//         if ($useHz) {
//             $verify->useZh = true;
//         }
//         $verify->entry(1);
//     }

    public function ajaxReturn($data) {
        $result = \think\Response::create($data, 'Json');
        return $result;
    }
}
