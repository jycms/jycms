<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: CMS.class.php 32 2016-10-23 16:45:40Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace CMS\taglib;

use think\template\TagLib;

/**
 * JYCMS系统标签库
 */
class CMS extends TagLib {
    // 标签定义
    protected $tags = array(
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'nav'      => array('attr' => 'field,name', 'close' => 1), //获取导航
        'articles' => array('attr' => 'name,return', 'close' => 1), //获取指定条件的文章列表
        'flashtag' => array('attr' => 'name,return', 'close'=>1), //标签云
    );

    /* 导航列表 */
    public function tagNav($tag, $content) {
        $field = empty($tag['field']) ? 'true' : $tag['field'];
        $tree  = empty($tag['tree']) ? false : true;
        $parse = $parse = '<?php ';
        $parse .= '$__NAV__ = cache(\'Nav\');';
        $parse .= 'if(!$__NAV__){ ';
        $parse .= '$__NAV__ = db(\'Nav\')->field(' . $field . ')->where("navtype=0 AND available=1")->order("displayorder")->select();';
        $parse .= 'foreach($__NAV__ as &$nav){ ';
        $parse .= 'if(!$nav[icon]){ ';
        $parse .= 'if($nav[type]==4) $nav[icon]="list";';
        $parse .= 'elseif ($nav[url]=="Home/Index/index") $nav[icon]="home";';
        $parse .= 'else $nav[icon]="file";';
        $parse .= '}}';
        $parse .= 'cache(\'Nav\', $__NAV__);';
        $parse .= '}';
        if ($tree) {
            $parse .= '$__NAV__ = list_to_tree($__NAV__, "id", "parentid", "sub");';
        }
        $parse .= '?><volist name="__NAV__" id="' . $tag['name'] . '">';
        $parse .= $content;
        $parse .= '</volist>';
        return $parse;
    }

    /**
     * 获取指定条件的文章列表
     * @param array $parameter 标签列表
     *      array(
     *          aids:以逗号分隔的文章ID列表
     *          uids:以逗号分隔的作者ID列表
     *          starttime:开始时间（文章最后更新日期）
     *          endtime:结束时间（文章最后更新日期）
     *          items:要取出的条数
     *          page:是否使用分页
     *          orderby:排序字段（可取值分别为：viewnum[浏览数],dateline[文章添加日期],commentnum[评论数]）
     *          order:排序方式（DESC/ASC）
     *          catid:文章分类ID（数组，单独ID，或者以逗号分隔的文章分类ID列表）
     *          picrequired:是否必须有图片
     *          rank:文章推荐标志（推荐级别：0：不推荐，1：分类置顶，2：全部置顶）
     *      )
     * @param string $content 标签模板
     * @return string
     */
    public function tagArticles($parameter, $content) {
        $return = isset($parameter['return'])?$parameter['return']:'';
        $name   = isset($parameter['name'])?$parameter['name']:'';
        if (!$return && !$name) {
            return false;
        }
        $parse = '<?php ';
        $parse .= '$parameter = \'' . str_replace('\'', '\\\'', serialize($parameter)) . '\';';
        $parse .= '$__BLOCK_OBJ__ = new \CMS\Block\Article($parameter);';
        $parse .= '$__BLOCK_DATA__ = $__BLOCK_OBJ__->getData();';
        if ($return) {
            $parse .= '$' . $return . ' = $__BLOCK_DATA__;';
            $parse .= ' ?>';
        } else {
            $parse .= ' ?>';
            $parse .= '<volist name="__BLOCK_DATA__[\'data\']" id="' . $name . '">';
        }
        $parse .= $content;
        if ($return) {

        } else {
            $parse .= '</volist>';
        }
        return $parse;
    }

    /**
     * 获取指定条件的文章列表
     * @param array $parameter 标签列表
     *      array(
     *          aids:以逗号分隔的文章ID列表
     *          uids:以逗号分隔的作者ID列表
     *          starttime:开始时间（文章最后更新日期）
     *          endtime:结束时间（文章最后更新日期）
     *          items:要取出的条数
     *          page:是否使用分页
     *          orderby:排序字段（可取值分别为：viewnum[浏览数],dateline[文章添加日期],commentnum[评论数]）
     *          order:排序方式（DESC/ASC）
     *          catid:文章分类ID（数组，单独ID，或者以逗号分隔的文章分类ID列表）
     *          picrequired:是否必须有图片
     *          rank:文章推荐标志（推荐级别：0：不推荐，1：分类置顶，2：全部置顶）
     *      )
     * @param string $content 标签模板
     * @return string
     */
    public function tagFlashtag($parameter, $content) {
        $return = isset($parameter['return'])?$parameter['return']:'';
        $name   = isset($parameter['name'])?$parameter['name']:'';
        if (!$return && !$name) {
            return false;
        }
        $parse = '<?php ';
        $parse .= '$parameter = \'' . str_replace('\'', '\\\'', serialize($parameter)) . '\';';
        $parse .= '$__BLOCK_OBJ__ = new \CMS\Block\Flashtag($parameter);';
        $parse .= '$__BLOCK_DATA__ = $__BLOCK_OBJ__->getData();';
        if ($return) {
            $parse .= '$' . $return . ' = $__BLOCK_DATA__;';
            $parse .= ' ?>';
        } else {
            $parse .= ' ?>';
            $parse .= '<volist name="__BLOCK_DATA__[\'data\']" id="' . $name . '">';
        }
        $parse .= $content;
        if ($return) {

        } else {
            $parse .= '</volist>';
        }
        return $parse;
    }

}
