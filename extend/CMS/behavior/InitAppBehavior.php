<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id: appInitBehavior.class.php 23 2016-10-15 13:14:20Z IT果农 <htmambo@163.com> $
// +-----------------------------------------------------------------------------------------------

namespace CMS\behavior;

// 初始化系统
class InitAppBehavior {

    // 行为扩展的执行入口必须是run
    public function run(&$content){
        error_reporting(7);
        if (defined('BIND_MODULE') && BIND_MODULE === 'Install')
            return;

        defined('DS') || define('DS', DIRECTORY_SEPARATOR);
        if (!isset($_SERVER['HTTPS']))
            $_SERVER['HTTPS'] = 'off';

        $_SERVER['PHP_SELF'] = htmlspecialchars($_SERVER['SCRIPT_NAME'] ? $_SERVER['SCRIPT_NAME'] : $_SERVER['PHP_SELF']);
        $_SERVER['basefilename'] = basename($_SERVER['PHP_SELF']);

        define('CMS_ROOT', substr($_SERVER['PHP_SELF'], 0, -strlen($_SERVER['basefilename'])));
        define('CMS_URL', strtolower(($_SERVER['HTTPS'] == 'on' ? 'https' : 'http') .
                '://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/') + 1)));

        define('CMS_TMP', './tmp');
        define('CMS_TMP_URL', CMS_URL.'/tmp');

        define('MY_QUERY_ERROR', 10501);

        define('FUNC_PATH', APP_PATH . 'common'.DS);

        define('NOW_TIME', time());
        // JYCMS常量定义
        define('JYCMS_VERSION', '0.1.160904');
        define('JYCMS_ADDON_PATH', EXTEND_PATH.'Addons');

        define('IS_ROBOT', \CMS\Core::is_robot());
        // 异常处理类
        set_exception_handler('CMS\Core::exception_handle');
        // 自定义错误处理函数，设置后 error_reporting 将失效。因为要保证 ajax 输出格式，所以必须触发 error_handle
        set_error_handler('CMS\Core::error_handle');
        register_shutdown_function('CMS\Core::fatalError');
    }
}