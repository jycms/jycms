<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace CMS\behavior;
use think\Hook;


// 初始化钩子信息
class InitHookBehavior {

    // 行为扩展的执行入口必须是run
    public function run(&$content){
        if (defined('BIND_MODULE') && BIND_MODULE === 'Install')
            return;
        $request = \think\Request::instance();
        $model = $request->module();
        if(strtolower($model)==='install') {
            return ;
        }
        define('MODULE_NAME', ucfirst($model));
        define('CONTROLLER_NAME', ucfirst($request->controller()));
        define('ACTION_NAME', $request->action());

        define('IS_POST', $request->isPost());
        define('IS_AJAX', $request->isAjax());
        define('IS_GET', $request->isGet());
        $data = cache('hooks');
        if(!$data){
            $hooks = db('Hooks')->column('name, addons');
            foreach ($hooks as $key => $value) {
                if ($value) {
                    $aModel = db('Addons');
                    $aModel = $aModel->where('status', 1);
                    $names = explode(',', $value);
                    if(count($names)==1){
                        $map = $value;
                    } else {
                        $map = array('IN', $names);
                    }
                    $aModel = $aModel->where(array('name' => $map));
                    $data = $aModel->column('id,name');
                    if($data){
                        $addons = array_intersect($names, $data);
                        Hook::add($key,array_map('get_addon_class',$addons));
                    }
                }
            }
            cache('hooks', Hook::get());
        }else{
            Hook::import($data,false);
        }
    }
}