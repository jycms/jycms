<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace Addons\BaiduShare;
use Addons\Addon;
/**
 * 百度分享插件
 */
class BaiduShareAddon extends Addon {
    //public $custom_config = 'config.html';

    public $info = array(
        'name'=>'BaiduShare',
        'title'=>'百度分享',
        'description'=>'用户将网站内容分享到第三方网站',
        'status'=>1,
        'author'=>'CoreThink',
        'version'=>'0.1'
    );

    public function install(){
        return true;
    }

    public function uninstall(){
        return true;
    }

    //实现的BaiduShare钩子方法
    public function BaiduShare($param){
        $this->assign('info', $param);
        $this->assign('addons_config', $this->getConfig());
        $this->display('share');
    }
}
