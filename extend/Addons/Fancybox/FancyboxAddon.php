<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace Addons\Fancybox;

use Addons\Addon;

/**
 * 图片弹出播放插件
 */
class FancyboxAddon extends Addon {
    public $info = array(
        'name'        => 'Fancybox',
        'title'       => '图片弹出播放',
        'description' => '让文章内容页的图片有弹出图片播放的效果',
        'status'      => 1,
        'author'      => 'che1988',
        'version'     => '0.1'
    );

    public function install() {
        /* 先判断插件需要的钩子是否存在 */
        $this->existHook('documentDetailAfter', 'Fancybox', '正文显示完成后的钩子');
        return true;
    }

    public function uninstall() {
        return true;
    }

    //实现的documentDetailAfter钩子方法
    public function documentDetailAfter($param) {
        $addons_config = $this->getConfig();
        $addons_config['padding'] = intval($addons_config['padding']);
        $this->assign('addons_config', $addons_config);
        $this->display('content');
    }

}
