<?php
// +-----------------------------------------------------------------------------------------------
// | 简易CMS
// +-----------------------------------------------------------------------------------------------
// | [请手动修改文件描述]
// +-----------------------------------------------------------------------------------------------
// | Author: IT果农 <htmambo@163.com> <http://www.haolie.net>
// +-----------------------------------------------------------------------------------------------
// | Version $Id$
// +-----------------------------------------------------------------------------------------------

namespace Addons;

/**
 * 插件类
 */
abstract class Addon {
    /**
     * 视图实例对象
     * @var view
     * @access protected
     */
    protected $view = null;

    /**
     * $info = array(
     *  'name'=>'Editor',
     *  'title'=>'编辑器',
     *  'description'=>'用于增强整站长文本的输入和显示',
     *  'status'=>1,
     *  'author'=>'thinkphp',
     *  'version'=>'0.1'
     *  )
     */
    public $info = array();
    public $addon_path = '';
    public $config_file = '';
    public $custom_config = '';
    public $admin_list = array();
    public $custom_adminlist = '';
    public $access_url = array();

    public function __construct() {

        $request = \think\Request::instance();
        $module = $request->module();
        $_replace = array(
            '__ROOT__'=> CMS_URL,
            '__STATIC__'=> CMS_URL.'static',
            '__CSS__'=> CMS_URL.'static/'.$module.'/css',
            '__IMG__'=> CMS_URL.'static/'.$module.'/images',
            '__JS__'=> CMS_URL.'static/'.$module.'/js'
        );
        $_replace['__ADDONROOT__'] = JYCMS_ADDON_PATH .DS. $this->getName();

        $this->view = \think\View::instance([], $_replace);
        $this->addon_path = JYCMS_ADDON_PATH . DS . $this->getName() . DS;

        if (is_file($this->addon_path . 'config.php')) {
            $this->config_file = $this->addon_path . 'config.php';
        }
    }

    /**
     * 模板主题设置
     * @access protected
     * @param string $theme 模版主题
     * @return Action
     */
    final protected function theme($theme) {
        $this->view->theme($theme);
        return $this;
    }

    //显示方法
    final protected function display($template = '') {
        if ($template == '') {
            $template = CONTROLLER_NAME;
        }
        echo ($this->fetch($template));
    }

    /**
     * 模板变量赋值
     * @access protected
     * @param mixed $name 要显示的模板变量
     * @param mixed $value 变量的值
     * @return Action
     */
    final protected function assign($name, $value = '') {
        $this->view->assign($name, $value);
        return $this;
    }

    //用于显示模板的方法
    final protected function fetch($templateFile = CONTROLLER_NAME) {
        if (!is_file($templateFile)) {
            $templateFile = $this->addon_path . $templateFile . '.'.config('template.view_suffix');
            if (!is_file($templateFile)) {
                throw new \Exception("模板不存在:$templateFile");
            }
        }
        return $this->view->fetch($templateFile);
    }

    final public function getName() {
        $class = get_class($this);
        return substr($class, strrpos($class, '\\') + 1, -5);
    }

    final public function checkInfo() {
        $info_check_keys = array('name', 'title', 'description', 'status', 'author', 'version');
        foreach ($info_check_keys as $value) {
            if (!array_key_exists($value, $this->info))
                return FALSE;
        }
        return TRUE;
    }

    /**
     * 获取插件的配置数组
     */
    final public function getConfig($name = '') {
        static $_config = array();
        if (empty($name)) {
            $name = $this->getName();
        }
        if (isset($_config[$name])) {
            return $_config[$name];
        }
        $config = array();
        $map['name'] = $name;
        $map['status'] = 1;
        $config = db('Addons')->where($map)->column('config');
        if ($config) {
            $config = json_decode($config[0], true);
        } else {
            if (file_exists($this->config_file)) {
                $temp_arr = include $this->config_file;
                foreach ($temp_arr as $key => $value) {
                    if ($value['type'] == 'group') {
                        foreach ($value['options'] as $gkey => $gvalue) {
                            foreach ($gvalue['options'] as $ikey => $ivalue) {
                                $config[$ikey] = $ivalue['value'];
                            }
                        }
                    } else {
                        $config[$key] = $temp_arr[$key]['value'];
                    }
                }
            }
        }
        $_config[$name] = $config;
        return $config;
    }

    /**
     * 解析插件中所包含的钩子
     * @param type $name
     */
    final public function getHooks() {
        $skip = get_class_methods('Addons\Addon');
        $methods = get_class_methods($this);
        $hooks = array();
        foreach ($methods as $k => $v) {
            $v = strtolower($v);
            if (!preg_grep("/$v/i", $skip) && substr($v, 0, 1) != '_')
                $hooks[$v] = $v;
        }
        return $hooks;
    }

    /**
     * 解析数据库语句函数
     * @param string $sql  sql语句   带默认前缀的
     * @param string $tablepre  自己的前缀
     * @return multitype:string 返回最终需要的sql语句
     */
    final public function sql_split($sql, $tablepre) {
        if ($tablepre != "jycms_")
            $sql = str_replace("jycms_", $tablepre, $sql);
        $sql = preg_replace("/TYPE=(InnoDB|MyISAM|MEMORY)( DEFAULT CHARSET=[^; ]+)?/", "ENGINE=\\1 DEFAULT CHARSET=utf8", $sql);

        if ($r_tablepre != $s_tablepre)
            $sql = str_replace($s_tablepre, $r_tablepre, $sql);
        $sql = str_replace("\r", "\n", $sql);
        $ret = array();
        $num = 0;
        $queriesarray = explode(";\n", trim($sql));
        unset($sql);
        foreach ($queriesarray as $query) {
            $ret[$num] = '';
            $queries = explode("\n", trim($query));
            $queries = array_filter($queries);
            foreach ($queries as $query) {
                $str1 = substr($query, 0, 1);
                if ($str1 != '#' && $str1 != '-')
                    $ret[$num] .= $query;
            }
            $num++;
        }
        return $ret;
    }

    /**
     * 获取插件所需的钩子是否存在，没有则新增
     * @param string $str  钩子名称
     * @param string $addons  插件名称
     * @param string $addons  插件简介
     */
    final public function existHook($str, $addons, $msg = '') {
        $hook_mod = db('Hooks');
        $where['name'] = $str;
        $gethook = $hook_mod->where($where)->find();
        if (!$gethook || empty($gethook) || !is_array($gethook)) {
            $data['name'] = $str;
            $data['description'] = $msg;
            $data['type'] = 1;
            $data['update_time'] = NOW_TIME;
            $data['addons'] = $addons;
            if (false !== $hook_mod->create($data)) {
                $hook_mod->add();
            }
        }
    }

    /**
     * 删除钩子
     * @param string $hook  钩子名称
     */
    final public function deleteHook($hook) {
        $model = db('hooks');
        $condition = array(
            'name' => $hook,
        );
        $model->where($condition)->delete();
    }

    //必须实现安装
    abstract public function install();
    //必须卸载插件方法
    abstract public function uninstall();
}
