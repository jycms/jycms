var xhEditor;
var retry = 1;
function initEditor(id) {
	var _Plugin={
		test7:{c:'testClassName',t:'保存远程图片',s:'ctrl+7',e:function(){
			var _this=this;
			_this.saveBookmark();
			retry = 1;
			downRemoteFile();
		}},
		Code:{c:'btnCode',t:'插入代码',h:1,e:function(){
			var _this=this;
			var htmlCode='<div><select id="xheCodeType"><option value="html">HTML/XML</option><option value="js">Javascript</option><option value="css">CSS</option><option value="php">PHP</option><option value="java">Java</option><option value="py">Python</option><option value="pl">Perl</option><option value="rb">Ruby</option><option value="cs">C#</option><option value="c">C++/C</option><option value="vb">VB/ASP</option><option value="">其它</option></select>起始行号：<input type="text" id="xheLine" value="1" size="4"></div><div><textarea id="xheCodeValue" wrap="soft" spellcheck="false" style="width:300px;height:100px;" /></div><div style="text-align:right;"><input type="button" id="xheSave" value="确定" /></div>';
			var jCode=$(htmlCode),jLine=$('#xheLine',jCode),jType=$('#xheCodeType',jCode),jValue=$('#xheCodeValue',jCode),jSave=$('#xheSave',jCode);
			jSave.click(function(){
				_this.loadBookmark();
				var param = jType.val();
				if(jLine.val()>1) param+=' num='+jLine.val();
				_this.pasteHTML('['+param+']'+jValue.val().replace(/\n/g,'<br />')+'[/'+jType.val()+']');
				_this.hidePanel();
				return false;
			});
			_this.saveBookmark();
			_this.showDialog(jCode);
		}}
	};
	$('head').append('<style type="text/css">.testClassName {background:transparent url('+SITEURL+'/static/xheditor-1.2.1/xheditor_skin/default/img/plugin.gif) no-repeat 16px 16px;background-position:2px 2px;}.btnCode {background:transparent url('+SITEURL+'/static/xheditor-1.2.1/xheditor_skin/default/img/code.gif) no-repeat 16px 16px;background-position:2px 2px;}</style>');
	xhEditor=$('#'+id).xheditor({skin:'vista', plugins:_Plugin,tools:'mfull'});
}
function downRemoteFile() {
	$.ajax({
		type: "post",
		url: SITEURL+"index.php?group=user&option=upload&op=downremotefile",
		data: {content:$("#jycms-ttHtmlEditor").val(),formhash:$("input[name=formhash]").val(),fromurl:$("input[name=fromurl]").val(),aid:$('#aid').val()},
		beforeSend: function(){
			if(retry==1)
			showDialog('正在分析并获取远程图片，请稍等！', 'notice');
		},
		error: function(){
				retry++;
				if(retry<5){
					showDialog('系统运行超时，现在将进行第'+retry+'次重试！');
					setTimeout(function () {
						downRemoteFile();
					}, 8000);
				} else {
					showDialog('系统运行超时，已经重试5次，如果要继续下载远程图片，请再次点击！');
				}
		},
		success: function(data, textStatus){
			hideMenu('fwin_dialog', 'dialog');
			console.log(data);
			if (data.indexOf('Maximum execution time of')>1)
			{
				retry++;
				if(retry<5){
					showDialog('系统运行超时，现在将进行第'+retry+'次重试！');
					downRemoteFile();
				} else {
					showDialog('系统运行超时，已经重试5次，如果要继续下载远程图片，请再次点击！');
				}
			} else if(data.indexOf('<b>Fatal error</b>:')>1){
				showDialog('系统运行出错，请重试！');
			} else {
				eval(data);
				if(typeof(content)!='undefined')
				xhEditor.setSource(content);
			}
		},
	});
	return;
}

function insertImage(image, url) {
	url = typeof url == 'undefined' || url === null ? image : url;
	var html = '<p><a href="' + url + '" target="_blank"><img src="'+image+'"></a></p>';
	xhEditor.pasteHTML(html);
}

	function getContentFromEditor() {
		var data = $('#jycms-ttHtmlEditor').val();
		// Trim data
		data = data.replace( /^\s+/, '' ).replace( /\s+$/, '' );
		if ( data != '' ) {
			data = strip_tags(data);
		}
		return data;
	}

	function registerClickTags() {
		jQuery("#gettags .container_clicktags span").click(function(event) {
			event.preventDefault();
			addTag(this.innerHTML);
		});

		jQuery('#st_ajax_loading').hide();
		if ( jQuery('#gettags').css('display') != 'block' ) {
			jQuery('#gettags').toggleClass('closed');
		}
	}
	function addTag(tag) {
		// Trim tag
		tag = tag.replace( /^\s+/, '' ).replace( /\s+$/, '' );
		var tag_entry = document.getElementById("tagname");
		if ( tag_entry.value.length > 0 && !tag_entry.value.match(/,\s*$/) ) {
			tag_entry.value += ", ";
		}

		var re = new RegExp(tag + ",");
		if ( !tag_entry.value.match(re) ) {
			tag_entry.value += tag + ", ";
		}
	}

	function strip_tags(str) {
	   return str.replace(/&lt;\/?[^&gt;]+&gt;/gi, "");
	}
	function validatef(obj) {
		var slen = $('input[id="title"]').val().length;
		if (slen < 1 || slen > 80) {
			alert("标题长度不合法(1-80个字符)!");
			$('input#title').focus();
			return false;
		}
		if ($("input[id='catid']").val() < 1) {
			alert("请选择一个分类！");
			$("input#catid").focus();
			return false;
		}
		var content = $('#jycms-ttHtmlEditor').val();
		obj.form.submit();
		return false;
	}
