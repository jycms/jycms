//ajax
Do.add('ajax', {
    path: Do.getConfig('basepath') + 'lib/ajax/ajax.js',
});
Do.add('alertmsg', {
    path: Do.getConfig('basepath') + 'lib/alertmsg/alertmsg.js'
});
//array
Do.add('array', {
    path: Do.getConfig('basepath') + 'lib/array/array.js',
});
//autosize
Do.add('autosize', {
    path: Do.getConfig('basepath') + 'lib/autosize/autosize.js',
});
//base载入
Do.add('base', {
    path: Do.getConfig('basepath') + 'lib/base/base.js',
    requires: ['basedrag', 'slidebar', 'ajax', 'cookie', 'autosize', 'alertmsg', 'modaltrigger', 'plugins']
});
Do.add('basedrag', {
    path: Do.getConfig('basepath') + 'lib/basedrag/basedrag.js',
});
//board
Do.add('board', {
    path: Do.getConfig('basepath') + 'lib/board/zui.board.js',
    requires: ['board.css', 'droppable']
});
Do.add('board.css', {
    path: Do.getConfig('basepath') + 'lib/board/zui.board.css',
});
//calendar
Do.add('calendar', {
    path: Do.getConfig('basepath') + 'lib/calendar/zui.calendar.js',
    requires : ['calendar.css']
});
Do.add('calendar.css', {
    path: Do.getConfig('basepath') + 'lib/calendar/zui.calendar.css',
    type: 'css'
});
//图片轮播
Do.add('carousel', {
    path: Do.getConfig('basepath') + 'lib/carousel/carousel.js',
});
//cookie
Do.add('cookie', {
    path: Do.getConfig('basepath') + 'lib/cookie/cookie.js',
});
//dashbaord
Do.add('dashboard.css', {
    path: Do.getConfig('basepath') + 'lib/dashboard/zui.dashboard.css',
});
Do.add('dashboard', {
    path: Do.getConfig('basepath') + 'lib/dashboard/zui.dashboard.js',
    requires:['dashboard']
});
//datagrid
Do.add('datagrid', {
    path: Do.getConfig('basepath') + 'lib/datagrid/datagrid.js',
    requires:['validator', 'icheck', 'bootstrap-select']
});
//grid
Do.add('ligerui', {
    path: Do.getConfig('basepath') + 'lib/grid/ligerui.js',
});
Do.add('grid.css', {
    path: Do.getConfig('basepath') + 'lib/grid/css/ligerui-grid.css',
    type: 'css'
});
Do.add('grid', {
    path: Do.getConfig('basepath') + 'lib/grid/ligerGrid.js',
    requires:['ligerui', 'grid.css']
});

//datatable
Do.add('datatable', {
    path: Do.getConfig('basepath') + 'lib/datatable/zui.datatable.js',
    requires:['datatable.css', 'selectable']
});
Do.add('datatable.css', {
    path: Do.getConfig('basepath') + 'lib/datatable/zui.datatable.css',
    type: 'css'
});
//droppable
Do.add('droppable', {
    path: Do.getConfig('basepath') + 'lib/droppable/droppable.js',
});
//draggable
Do.add('draggable', {
    path: Do.getConfig('basepath') + 'lib/draggable/draggable.js',
});
//form
Do.add('form', {
    path: Do.getConfig('basepath') + 'lib/valid/Validform.js'
});

//icheck
Do.add('icheck', {
    path: Do.getConfig('basepath') + 'lib/icheck/icheck.js',
});

//menu
Do.add('menu', {
    path: Do.getConfig('basepath') + 'lib/menu/menu.js'
});
//plugins
Do.add('plugins', {
    path: Do.getConfig('basepath') + 'lib/plugins/plugins.js'
});
Do.add('slidebar', {
    path: Do.getConfig('basepath') + 'lib/slidebar/slidebar.js',
    requires: ['basedrag']
});

//tip
Do.add('tips.css', {
    path: Do.getConfig('basepath') + 'lib/tips/toastr.css',
    type : 'css'
});
Do.add('tips', {
    path: Do.getConfig('basepath') + 'lib/tips/toastr.js',
    requires : ['tips.css']
});

//time
Do.add('time.css', {
    path: Do.getConfig('basepath') + 'lib/datetimepicker/jquery.datetimepicker.css',
    type: 'css'
});
Do.add('time', {
    path: Do.getConfig('basepath') + 'lib/datetimepicker/jquery.datetimepicker.full.js',
    requires: ['time.css']
});

//Ztree
Do.add('ztree', {
    path: Do.getConfig('basepath') + 'lib/ztree/jquery.ztree.all.min.js',
});

//webuploader
Do.add('webuploader.css', {
    path: Do.getConfig('basepath') + 'lib/webuploader/webuploader.css',
    type: 'css'
});
Do.add('webuploader', {
    path: Do.getConfig('basepath') + 'lib/webuploader/webuploader.js',
    requires: ['webuploader.css']
});

//排序
Do.add('sortable', {
    path: Do.getConfig('basepath') + 'lib/sortable/sortable.js',
    requires: ['droppable']
});

//chosen
Do.add('chosen', {
	path: Do.getConfig('basepath') + 'lib/chosen/chosen.js',
	requires: ['chosen.css']
});
Do.add('chosen.css', {
	path: Do.getConfig('basepath') + 'lib/chosen/chosen.css',
	type: 'css'
});

//图表
Do.add('chart', {
    path: Do.getConfig('basepath') + 'lib/chart/chart.js'
});
Do.add('echarts', {
    path: Do.getConfig('basepath') + 'lib/echarts/echarts.js'
});
Do.add('highcharts', {
    path: Do.getConfig('basepath') + 'lib/highcharts/highcharts.js'
});

//裁剪图片
Do.add('cropper.css',{
    path : Do.getConfig('basepath') + 'lib/cropper/cropper.min.css',
    type : 'css'
});
Do.add('cropper', {
    path: Do.getConfig('basepath') + 'lib/cropper/cropper.js',
	requires : ['cropper.css', 'imgready']
});

Do.add('imgcut', {
    path: Do.getConfig('basepath') + 'lib/imgcutter/zui.imgcutter.js',
	requires : ['imgready', 'draggable']
});
Do.add('imgready', {
    path: Do.getConfig('basepath') + 'lib/imageready/imageready.js',
});
Do.add('lightbox', {
    path: Do.getConfig('basepath') + 'lib/lightbox/lightbox.js',
    requires: ['imgready', 'lightbox.css', 'modaltrigger']
});
Do.add('lightbox.css',{
    path : Do.getConfig('basepath') + 'lib/lightbox/lightbox.css',
    type : 'css'
});
Do.add('modaltrigger',{
    path : Do.getConfig('basepath') + 'lib/modaltrigger/modaltrigger.js',
});
//表单验证
Do.add('validator', {
    path: Do.getConfig('basepath') + 'lib/validator/jquery.validator.js?local=zh-CN'
});

//颜色选择器
Do.add('colorpicker.css', {
    path: Do.getConfig('basepath') + 'lib/colorpicker/css/bootstrap-colorpicker.min.css',
    type: 'css'
});
Do.add('colorpicker', {
    path: Do.getConfig('basepath') + 'lib/colorpicker/js/bootstrap-colorpicker.min.js',
    requires: ['colorpicker.css']
});
//热键
Do.add('hotkey',{
    path : Do.getConfig('basepath') + 'lib/hotkey/hotkey.min.js',
});
Do.add('lookup',{
    path : Do.getConfig('basepath') + 'lib/lookup/lookup.js',
});
//图标选择
Do.add('choseniconcss',{
    path : Do.getConfig('basepath') + 'lib/chosenicons/zui.chosenicons.min.css',
    type : 'css'
});
Do.add('chosenicon',{
    path : Do.getConfig('basepath') + 'lib/chosenicons/zui.chosenicons.min.js',
    requires:['chosen', 'choseniconcss']
});

Do.add('scrollbar.css',{
    path : Do.getConfig('basepath') + 'lib/scrollbar/css/perfect-scrollbar.min.css',
    type : 'css'
});
Do.add('scrollbar', {
    path: Do.getConfig('basepath') + 'lib/scrollbar/perfect-scrollbar.jquery.js',
    requires: ['scrollbar.css']
});

//ART模板引擎
Do.add('template',{
	path: Do.getConfig('basepath') + 'lib/template/templatehelper.js',
    requires:['base','templatelib']
});
Do.add('templatelib',{
	path: Do.getConfig('basepath') + 'lib/template/template.js'
});

//拖拽选取
Do.add('selectable', {
	path: Do.getConfig('basepath') + 'lib/selectable/zui.selectable.min.js'
});

//bootstrap-select
Do.add('bootstrap-select-css', {
	path: Do.getConfig('basepath') + 'lib/bootstrapSelect/bootstrap-select.css'
});
Do.add('bootstrap-select-lang', {
	path: Do.getConfig('basepath') + 'lib/bootstrapSelect/bootstrap-select.js',
});
Do.add('bootstrap-select', {
	path: Do.getConfig('basepath') + 'lib/bootstrapSelect/bootstrap-select-zh_CN.js',
    requires:['bootstrap-select-css','bootstrap-select-lang']
});
Do.add('download', {
	path: Do.getConfig('basepath') + 'lib/download/jquery.fileDownload.js'
});