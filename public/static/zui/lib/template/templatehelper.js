
template.helper('getDateDiff', function (timespan) {
    return getDateDiff(timespan);
});
template.helper('toThousands', function (num) {
    return toThousands(num);
});
template.helper('htmldecode', function (html) {
    return htmldecode(html);
});
template.helper('encodeURIComponent', function (url) {
    return encodeURIComponent(url);
});
template.helper('stripTags', function(html){
    if(typeof(html)!=='string') return '';
    return html.replace(/<[^>]+>/g,"");//去掉所有的html标记
});
template.helper('subnum',function(num){
    return Math.floor((parseInt(num)+9999)/10000);
});
function htmlencode(s){
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(s));
    return div.innerHTML;
}
function htmldecode(str){
    var s = "";
    if(str.length == 0) return "";
    s = str.replace(/&/g,"&");
    s = s.replace(/</g,"<");
    s = s.replace(/>/g,">");
    s = s.replace(/ /g," ");
    s = s.replace(/'/g,"\'");
    s = s.replace(/"/g,"\"");
    return s;
    var div = document.createElement('div');
    div.innerHTML = str;
    return div.innerText || div.textContent;
}
/**
 * 时间戳转时间字符串的函数
 */
function getDateDiff(dateTimeStamp) {
    var minute = 1000 * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var halfamonth = day * 15;
    var month = day * 30;
    var now = new Date().getTime();
    if(dateTimeStamp.length==10){
        dateTimeStamp += '000';
    }
    var diffValue = parseInt(now) - parseInt(dateTimeStamp);
    if (diffValue < 0) {
        return;
    }
    var date = new Date(parseInt(dateTimeStamp));
    var result = date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate();
    var monthC = diffValue / month;
    var weekC = diffValue / (7 * day);
    var dayC = diffValue / day;
    var hourC = diffValue / hour;
    var minC = diffValue / minute;
    if(monthC<6){
        if (monthC >= 1) {
            result = parseInt(monthC) + "个月前";
        } else if (weekC >= 1) {
            result = parseInt(weekC) + "周前";
        } else if (dayC >= 1) {
            result = parseInt(dayC) + "天前";
        } else if (hourC >= 1) {
            result = parseInt(hourC) + "个小时前";
        } else if (minC >= 1) {
            result = parseInt(minC) + "分钟前";
        } else {
            result = "刚才";
        }
    }
    return result;
}
