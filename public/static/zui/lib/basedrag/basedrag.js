/* ========================================================================
 * zui-basedrag.js  v1.3 beta2
 * ======================================================================== */
+function ($) {
    'use strict';

    // BASEDRAG CLASS DEFINITION
    // ======================

    var Basedrag = function(element, options) {
        this.$element = $(element)
        this.options  = options
    }

    Basedrag.prototype.init = function() {
        var that = this

        this.options.$obj = this.$element
        if (this.options.obj) this.options.$obj = this.options.obj
        if (this.options.event)
            this.start(this.options.event)
        else {
            if (this.options.selector)
                this.$element.find(this.options.selector).on('mousedown', function(e) { that.start.apply(that, [e]) })
            else
                this.$element.on('mousedown', function(e) { that.start.apply(that, [e]) })
        }
    }

    Basedrag.prototype.start = function(e) {
        document.onselectstart = function(e) { return false } //禁止选择
        var that = this

        if (!this.options.oleft) this.options.oleft = parseInt(this.$element.css('left')) || 0
        if (!this.options.otop)  this.options.otop  = parseInt(this.$element.css('top')) || 0

        $(document).on('mouseup.zui.basedrag', function(e) { that.stop.apply(that, [e]) })
            .on('mousemove.zui.basedrag', function(e) { that.drag.apply(that, [e]) })
    }

    Basedrag.prototype.drag = function(e) {
        if (!e) e = window.event
        var options = this.options,
            left    = (options.oleft + (e.pageX || e.clientX) - options.event.pageX),
            top     = (options.otop + (e.pageY || e.clientY) - options.event.pageY)

        if (top < 1) top = 0
        if (options.move == 'horizontal') {
            if ((options.minW && left >= parseInt(this.options.$obj.css('left')) + options.minW) && (options.maxW && left <= parseInt(this.options.$obj.css('left')) + options.maxW)) {
                this.$element.css('left', left)
            } else if (options.scop) {
                if (options.relObj) {
                    if ((left - parseInt(options.relObj.css('left'))) > options.cellMinW)
                        this.$element.css('left', left)
                    else
                        this.$element.css('left', left)
                }
            }
        } else if (options.move == 'vertical') {
            this.$element.css('top', top)
        } else {
            var $selector = options.selector ? this.options.$obj.find(options.selector) : this.options.$obj

            if (left >= -$selector.outerWidth() * 2 / 3 && top >= 0 && (left + $selector.outerWidth() / 3 < $(window).width()) && (top + $selector.outerHeight() < $(window).height())) {
                this.$element.css({left:left, top:top})
            }
        }
        if (options.drag)
            options.drag.apply(this.$element, [this.$element, e, left, top])

        return this.preventEvent(e)
    }

    Basedrag.prototype.stop = function(e) {
        $(document).off('mousemove.zui.basedrag').off('mouseup.zui.basedrag')
        if (this.options.stop)
            this.options.stop.apply(this.$element, [this.$element, e])
        if (this.options.event)
            this.destroy()
        document.onselectstart = function(e) { return true } //启用选择
        return this.preventEvent(e)
    }

    Basedrag.prototype.preventEvent = function(e) {
        if (e.stopPropagation) e.stopPropagation()
        if (e.preventDefault) e.preventDefault()
        return false
    }

    Basedrag.prototype.destroy = function() {
        this.$element.removeData('zui.basedrag')
        if (!this.options.nounbind) this.$element.off('mousedown')
    }

    // BASEDRAG PLUGIN DEFINITION
    // =======================

    function Plugin(option) {
        var args     = arguments
        var property = option

        return this.each(function () {
            var $this   = $(this)
            var options = $.extend({}, $this.data(), typeof option === 'object' && option)
            var data    = $this.data('zui.basedrag')

            if (!data) $this.data('zui.basedrag', (data = new Basedrag(this, options)))
            if (typeof property === 'string' && $.isFunction(data[property])) {
                [].shift.apply(args)
                if (!args) data[property]()
                else data[property].apply(data, args)
            } else {
                data.init()
            }
        })
    }

    var old = $.fn.basedrag

    $.fn.basedrag             = Plugin
    $.fn.basedrag.Constructor = Basedrag

    // BASEDRAG NO CONFLICT
    // =================

    $.fn.basedrag.noConflict = function () {
        $.fn.basedrag = old
        return this
    }

}(jQuery);
