/* ========================================================================
 * zui-slidebar.js  v1.3 beta2
 * ======================================================================== */
+function ($) {
    'use strict';
    
    // SLIDEBAR CLASS INSTANCE
    // ======================
    $(function() {
        $('#zui-leftside').after('<!-- Adjust the width of Left slide -->').after(FRAG.splitBar).after(FRAG.splitBarProxy)
    })
    
    // SLIDEBAR CLASS DEFINITION
    // ======================
    
    var Slidebar = function(element, options) {
        this.$element   = $(element)
        this.$bar       = this.$element.find('#zui-sidebar')
        this.$sbar      = this.$element.find('#zui-sidebar-s')
        this.$lock      = this.$bar.find('> .toggleCollapse > .lock')
        this.$navtab    = $('#zui-navtab')
        this.$collapse  = this.$sbar.find('.collapse')
        this.$split     = $('#zui-splitBar')
        this.$split2    = $('#zui-splitBarProxy')
        
        this.isfloat    = false
        this.options    = options
    }
    
    Slidebar.prototype.lock = function() {
        var that   = this
        var cleft  = that.$bar.outerWidth() + 4
        var cwidth = $.zui.windowWidth - $('#zui-sidebar').width() - 6
        
        that.faLock()
        that.hoverLock()
        that.$sbar.animate({left: -10}, 20)
        that.$bar.removeClass('shadown')
        that.isfloat = false
        that.$navtab.animate({left:cleft, width:cwidth}, 500, function() {
            $(window).trigger($.zui.eventType.resizeGrid)
        })
        that.$split.show()
    }
    
    Slidebar.prototype.unlock = function() {
        var that    = this
        var barleft = 0 - that.$bar.outerWidth() - 0
        var cwidth  = $.zui.windowWidth - 6
        
        that.faUnLock()
        that.hoverUnLock()
        that.$navtab.animate({left:6, width:cwidth}, 400)
        that.$bar.animate({left: barleft}, 500, function() {
            that.$sbar.animate({left:0}, 200)
            that.$split.hide()
            $(window).trigger($.zui.eventType.resizeGrid)
        })
        that.isfloat = false
    }
    
    Slidebar.prototype.float = function() {
        var that  = this
        
        that.$sbar.animate({left:-10}, 200)
        that.$bar.addClass('shadown').animate({left: 0}, 500)
        that.isfloat = true
    }
    
    Slidebar.prototype.hideFloat = function() {
        var that    = this
        var barleft = 0 - that.$bar.outerWidth() - 0
        
        that.$bar.animate({left: barleft - 10}, 500, function() {
            that.$sbar.animate({left:0}, 100)
        })
        that.isfloat = false
    }
    
    Slidebar.prototype.hoverLock = function() {
        var that = this
        
        that.$lock
            .hover(function() {
                that.tipUnLock()
                that.faUnLock()
            }, function() {
                that.tipLock()
                that.faLock()
            })
    }
    
    Slidebar.prototype.hoverUnLock = function() {
        var that = this
        
        that.$lock
            .hover(function() {
                that.tipLock()
                that.faLock()
            }, function() {
                that.tipUnLock()
                that.faUnLock()
            })
    }
    
    Slidebar.prototype.tipLock = function() {
        this.$lock.tooltip('destroy').tooltip({ title:'保持锁定，始终显示导航栏', container:'body' })
    }
    
    Slidebar.prototype.tipUnLock = function() {
        this.$lock.tooltip('destroy').tooltip({ title:'解除锁定，自动隐藏导航栏', container:'body' })
    }
    
    Slidebar.prototype.faLock = function() {
        this.$lock.find('> i').attr('class', 'icon icon-lock')
    }
    
    Slidebar.prototype.faUnLock = function() {
        this.$lock.find('> i').attr('class', 'icon icon-unlock-alt')
    }
    
    Slidebar.prototype.init = function() {
        var that = this
        
        if (!$.zui.ui.showSlidebar) {
            that.unlock()
        } else {
            that.hoverLock()
        } 
        
        this.$lock.off('click.zui.slidebar').on('click.zui.slidebar', function() {
            if (that.isfloat) {
                that.lock()
            } else {
                that.unlock()
            }
            $.zui.ui.showSlidebar = !$.zui.ui.showSlidebar
        })
        
        this.$collapse.hover(function() {
            that.float()
            that.$navtab.click(function() {
                if (that.isfloat) that.hideFloat()
            })
        })
        
        this.$split.mousedown(function(e) {
            that.$split2.each(function() {
                var $spbar2 = $(this)
                
                setTimeout(function() { $spbar2.show() }, 100)
                $spbar2
                    .css({visibility:'visible', left: that.$split.css('left')})
                    .basedrag($.extend(that.options, {obj:that.$bar, move:'horizontal', event:e, stop: function() {
                        $(this).css('visibility', 'hidden')
                        var move      = parseInt($(this).css('left')) - parseInt(that.$split.css('left'))
                        var sbarwidth = that.$bar.outerWidth() + move
                        var cleft     = parseInt(that.$navtab.css('left')) + move
                        var cwidth    = that.$navtab.outerWidth() - move
                        
                        that.$bar.css('width', sbarwidth)
                        that.$split.css('left', $(this).css('left'))
                        that.$navtab.css({left:cleft, width:cwidth})
                    }}))
                
                return false
            })
        })
        
        // move hnav
        if ($('#zui-hnav-navbar-box').length) {
            that.moveHnav()
        }
    }
    
    Slidebar.prototype.moveHnav = function() {
        var $hnavbox  = $('#zui-hnav-navbar-box'),
            $hnavbar  = $hnavbox.find('> #zui-hnav-navbar'),
            $hmoreL   = $hnavbox.prev(),
            $hmoreR   = $hnavbox.next()
        
        $hmoreL.hover(function() {
            $hnavbar.stop().animate({left:0}, 2000, function() {
                $hmoreL.hide()
            })
        }, function() {
            $hnavbar.stop()
            if ($hnavbox.data('hnav.move')) {
                $hmoreR.show()
            }
        }).on('click', function() {
            $hnavbar.stop().animate({left:0}, 'fast', function() {
                $hmoreL.hide()
            })
            return false
        })
        
        $hmoreR.hover(function() {
            $hnavbar.stop().animate({left:($hnavbox.width() - $hnavbox.data('hnav.liw') - 10)}, 2000, function() {
                $hmoreR.hide()
            })
        }, function() {
            $hnavbar.stop()
            if ($hnavbar.css('left') != '0px') {
                $hmoreL.show()
            }
        }).on('click', function() {
            $hnavbar.stop().animate({left:($hnavbox.width() - $hnavbox.data('hnav.liw') - 10)}, 'fast', function() {
                $hmoreR.hide()
            })
            return false
        })
        //模拟点击第一个菜单项
        //$hnavbar.find('li>a').first().click();
    }
    
    Slidebar.prototype.initHnav = function() {
        var that   = this,
            title  = that.$element.text().trim(),
            $li    = that.$element.parent(),
            $box   = $('#zui-accordionmenu'),
            $trees, $items, $panel, $array
        
        $trees = $li.find('> .items > ul.ztree')
        $items = $li.find('> .items > ul.menu-items')
        if (!($trees.length || $items.length)) return
        if ($trees.length) $array = $trees
        if ($items.length) {
            if (!$array) $array = $items
            else $array = $array.add($items)
            
            $items.find('a').each(function() {
                var $a = $(this), options = $a.data('options')
                
                if (!$a.data('icon.init') && options && typeof options === 'string') {
                    options = options.toObj()
                    if (options && options.icon) {
                        options.icon = options.icon.trim()
                        if (options.icon.startsWith('icon-')) options.icon = options.icon.substr(5)
                        $a.prepend('<i class="icon icon-'+ options.icon +'"></i>').data('icon.init', true).attr('title', $a.text().trim())
                    }
                }
            })
        }
        $box.empty()
        $array.each(function(i) {
            var $t = $(this), panel, cls, bodycls, icon = $t.data('icon'), iconClose = $t.data('iconClose'), icon = icon ? icon : 'dot-circle'
            
            if ($t.data('tit')) title = $t.data('tit')
            
            cls     = i ? 'collapsed' : ''
            bodycls = i ? '' : ' in'
            panel   = FRAG.slidePanel
                          .replaceAll('#id#', 'zui-collapse'+ i)
                          .replaceAll('#title#', title)
                          .replaceAll('#righticon#', '<i class="icon icon-angle-down"></i>')
                          .replaceAll('#class#', cls)
                          .replaceAll('#bodyclass#', bodycls)
            
            if (icon) panel = panel.replaceAll('#icon#', '<i class="icon icon-'+ icon +'"></i>')
            else panel = panel.replaceAll('#icon#', '')

            $panel = $(panel)
            //$panel.find('.panel-heading').remove();
            $panel.find('> .panel-collapse > .panel-body').append($t.removeAttr('data-noinit'))
            $box.append($panel)
            if (!i) $panel.collapse('show')
        })
        
        $('#zui-sidebar').initui()
        
        $li
            .addClass('active')
            .data('zui.slidebar.hnav.panels', $box.find('> .panel'))
            .siblings().removeClass('active')
    }
    
    // SLIDEBAR PLUGIN DEFINITION
    // =======================
    
    function Plugin(option) {
        var args     = arguments
        var property = option
        
        return this.each(function () {
            var $this   = $(this)
            var options = $.extend({}, $this.data(), typeof option === 'object' && option)
            var data    = $this.data('zui.slidebar')
            
            if (!data) $this.data('zui.slidebar', (data = new Slidebar(this, options)))
            
            if (typeof property === 'string' && $.isFunction(data[property])) {
                [].shift.apply(args)
                if (!args) data[property]()
                else data[property].apply(data, args)
            } else {
                data.init()
            }
        })
    }

    var old = $.fn.slidebar

    $.fn.slidebar             = Plugin
    $.fn.slidebar.Constructor = Slidebar
    
    // SLIDEBAR NO CONFLICT
    // =================
    
    $.fn.basedrag.noConflict = function () {
        $.fn.slidebar = old
        return this
    }
    
    // SLIDEBAR DATA-API
    // ==============
    $(document).one($.zui.eventType.afterInitUI, function(e) {
        $('#zui-leftside').slidebar({minW:150, maxW:700})
    })
    
    $(document).on('click.zui.slidebar.data-api', '[data-toggle="slidebar"]', function(e) {
        var $li = $(this).parent(), $box = $('#zui-accordionmenu'), $panels = $li.data('zui.slidebar.hnav.panels')
        
        $box.find('> .panel').detach()
        
        if ($panels && $panels.length) {
            $box.append($panels)
            $li.addClass('active').siblings().removeClass('active')
        } else {
            Plugin.call($(this), 'initHnav')
        }
        
        e.preventDefault()
    })
    
}(jQuery);
