/* ========================================================================
 * zui-plugins.js  v1.3 beta2
 * ======================================================================== */
+function ($) {
    'use strict';

    $(document).on($.zui.eventType.initUI, function(e) {
        var $box    = $(e.target)

        // UI init begin...
        /* grid */
        var $dg = $box.find('[data-toggle="grid"]');
        if($dg.length>0) {
            Do.ready('grid', function(){
                if(typeof($dg)==='undefined' || $dg.length===0)
                var $dg = $box.find('[data-toggle="grid"]');
                $dg.each(function(){
                    var $this = $(this);
                    var options = {};
                    var op = $this.data('options');
                    if(typeof(op)==='string') {
                        options = $.extend(true, {}, options, op.toObj());
                        if(!options.hasOwnProperty('pageParmName')) {
                            options.pageParmName = $.zui.pageInfo.pageVar;
                        }
                        if(!options.hasOwnProperty('pagesizeParmName')) {
                            options.pagesizeParmName = $.zui.pageInfo.pageSize
                        }
                        if(!options.hasOwnProperty('sortnameParmName')) {
                            options.sortnameParmName = $.zui.pageInfo.orderField
                        }
                        if(!options.hasOwnProperty('sortorderParmName')) {
                            options.sortorderParmName = $.zui.pageInfo.orderDirection
                        }
                        $this.removeAttr('data-options');
                    }
                    $this.removeData('options');
                    op = $this.ligerGrid(options);
                    $this.data('zui.datagrid', op);
                });
            });
        }
        /* datagrid */
        var $dg = $box.find('[data-toggle="datagrid"]');
        if($dg.length) {
            Do.ready('datagrid', function(){
                if(typeof($dg)==='undefined' || $dg.length===0)
                var $dg = $box.find('[data-toggle="datagrid"]');
                $dg.each(function(){
                    $(this).datagrid();
                });
            });
        }
        /* datatable */
        var $dg = $box.find('[data-toggle="datatable"]');
        if($dg.length) {
            Do.ready('datatable', function(){
                if(typeof($dg)==='undefined' || $dg.length===0)
                var $dg = $box.find('[data-toggle="datatable"]');
                $dg.each(function(){
                    $(this).datatable();
                });
            });
        }
        /* i-check */
        var $icheck = $box.find('input[data-toggle="icheck"]');
        if($icheck.length) {
            Do.ready('icheck', function(){
                if(typeof($icheck)==='undefined' || $icheck.length===0)
                var $icheck = $box.find('input[data-toggle="icheck"]');
                if ($.fn.iCheck) {
                    $icheck.each(function(i) {
                        var $element = $(this),
                            id       = $element.attr('id'),
                            name     = $element.attr('name'),
                            label    = $element.data('label')

                        if (label) $element.after('<label for="'+ id +'" class="ilabel">'+ label +'</label>')

                        $element
                            .on('ifCreated', function(e) {
                                /* Fixed validate msgbox position */
                                var $parent = $(this).closest('div'),
                                    $ilabel = $parent.next('[for="'+ id +'"]')

                                $parent.attr('data-icheck', name)
                                $ilabel.attr('data-icheck', name)
                            })
                            .iCheck({
                                checkboxClass : 'icheckbox_minimal-purple',
                                radioClass    : 'iradio_minimal-purple',
                                increaseArea  : '50%' // optional
                            })
                            .on('ifChanged', function(element, type) {
                                /* Trigger validation */
                                if(element.currentTarget.type=='radio' && type=='ifUnchecked'){
                                    return;
                                }
                                $.fn.validator && $(this).trigger('validate');
                                $(this).trigger('change')
                            })
                            .removeAttr('data-toggle');

                        if ($element.prop('disabled')) $element.iCheck('disable')
                    })
                    /* i-check check all */
                    $icheck.filter('.checkboxCtrl').on('ifChanged', function(e) {
                        var checked = e.target.checked == true ? 'check' : 'uncheck'
                        var group   = $(this).data('group')

                        $box.find(':checkbox[name="'+ group +'"]').iCheck(checked)
                    })
                }
            });
        }


        /* bootstrap - select */
        var $selectpicker       = $box.find('select[data-toggle="selectpicker"]')
        if ($selectpicker.length>0) {
            var bjui_select_linkage = function($obj, $next) {
                if (!$next || !$next.length) return

                var refurl    = $obj.data('refurl')
                var _setEmpty = function($select) {
                    var $_nextselect = $($select.data('nextselect'))

                    if ($_nextselect && $_nextselect.length) {
                        var emptytxt = $_nextselect.data('emptytxt') || '&nbsp;'

                        $_nextselect.html('<option>'+ emptytxt +'</option>').selectpicker('refresh')
                        _setEmpty($_nextselect)
                    }
                }

                if (($next && $next.length) && refurl) {
                    var val = $obj.data('val'), nextVal = $next.data('val')

                    if (typeof val === 'undefined') val = $obj.val()
                    $.ajax({
                        type     : 'POST',
                        dataType : 'json',
                        url      : refurl.replace('{value}', encodeURIComponent(val)),
                        cache    : false,
                        data     : {},
                        success  : function(json) {
                            if (!json) return

                            var html = '', selected = ''

                            $.each(json, function(i) {
                                var value, label

                                if (json[i] && json[i].length) {
                                    value = json[i][0]
                                    label = json[i][1]
                                } else {
                                    value = json[i].value
                                    label = json[i].label
                                }
                                if (typeof nextVal !== 'undefined') selected = value == nextVal ? ' selected' : ''
                                html += '<option value="'+ value +'"'+ selected +'>' + label + '</option>'
                            })

                            $obj.removeAttr('data-val').removeData('val')
                            $next.removeAttr('data-val').removeData('val')

                            if (!html) {
                                html = $next.data('emptytxt') || '&nbsp;'
                                html = '<option>'+ html +'</option>'
                            }

                            $next.html(html).selectpicker('refresh')
                            _setEmpty($next)
                        },
                        error   : $.zui.ajaxError
                    })
                }
            }
            Do.ready('bootstrap-select', function(){
                if(typeof($selectpicker)=='undefined' || $selectpicker.length===0){
                    var $selectpicker       = $box.find('select[data-toggle="selectpicker"]')
                }

                $selectpicker.each(function() {
                    var $element  = $(this)
                    var options   = $element.data()
                    var $next     = $(options.nextselect)

                    $element.addClass('show-tick')
                    if (!options.style) $element.data('style', 'btn-default')
                    if (!options.width) $element.data('width', 'auto')
                    if (!options.container) $element.data('container', 'body')
                    else if (options.container == true) $element.attr('data-container', 'false').data('container', false)

                    $element.selectpicker()

                    if ($next && $next.length && (typeof $next.data('val') != 'undefined'))
                        bjui_select_linkage($element, $next)
                });

                /* bootstrap - select - linkage && Trigger validation */
                $selectpicker.change(function() {
                    var $element    = $(this)
                    var $nextselect = $($element.data('nextselect'))

                    bjui_select_linkage($element, $nextselect)

                    /* Trigger validation */
                    if ($element.attr('aria-required')) {
                        $.fn.validator && $element.trigger('validate')
                    }
                });
            });

        }


        /* fixed ui style */
        $box.find('button').each(function() {
            var $element = $(this), icon = $element.data('icon')

            $element.addClass('btn')

            if (icon && !$element.find('> i').length) {
                icon = 'icon-'+ icon.replace('icon-', '')

                if (!$element.data('zui.icon')) {
                    $element.html('<i class="icon '+ icon +'"></i> '+ $element.html()).data('zui.icon', true)
                }
            }
        })

        $box.find('input:text, input:password').each(function() {
            var $element = $(this).addClass('form-control'), size = $element.attr('size') || 0, width = size * 10

            width && $element.css('width', width)
        })

        //文件上传
        $box.find('.js-file-upload').each(function(){
            $(this).zhpFileUpload();
        });
        $box.find('input[type="file"]').each(function() {
            $(this).zhpFileUpload();
        });
        //图片上传
        $box.find(".js-img-upload").each(function() {
            $(this).zhpImgUpload();
        });
        //多图片上传
        $box.find('input[type="files"]').each(function() {
            $(this).zhpMultiUpload();
        });
        $box.find(".js-multi-upload").each(function() {
            $(this).zhpMultiUpload();
        });
        //编辑器
        $box.find(".js-editor").each(function() {
            $(this).zhpEditor();
        });
        //时间选择
        $box.find(".js-time").each(function() {
            $(this).zhpTime();
        });
        //日期选择
        $box.find('input[data-toggle="datepicker"]').each(function() {
            $(this).removeAttr('data-toggle');
            $(this).zhpDate();
        });
        $box.find(".js-date").each(function() {
            $(this).zhpDate();
        });
        $box.find('input[type="date"]').each(function() {
            $(this).zhpDate();
        });
        //选择框
        $box.find('select').each(function() {
            $(this).zhpSelect();
        });

        $box.find('textarea').each(function() {
            var $element = $(this).addClass('form-control'), cols = $element.attr('cols') || 0, width = cols * 10, toggle = $element.attr('data-toggle')

            width && $element.css('width', width)

            if (toggle && toggle == 'autoheight' && $.fn.autosize)
                $element.addClass('autosize').autosize()
        })

        $box.find('a.btn').each(function() {
            var $element = $(this), icon = $element.data('icon')

            if (icon && !$element.find('> i').length) {
                icon = 'icon-'+ icon.replace('icon-', '')

                if (!$element.data('zui.icon')) {
                    $element.html('<i class="icon '+ icon +'"></i> '+ $element.html()).data('zui.icon', true)
                }
            }
        })

        /* zTree - plugin */
        var $zTree = $box.find('[data-toggle="ztree"]');
        if($zTree.length) {
            Do.ready('ztree', function(){
                if(typeof($zTree)==='undefined' || $zTree.length===0)
                var $zTree = $box.find('[data-toggle="ztree"]');
                if ($.fn.zTree) {
                    $zTree.each(function() {
                        var $this = $(this), op = $this.data(), options = op.options, _setting

                        if (options && typeof options === 'string') options = options.toObj()
                        if (options) $.extend(op, typeof options === 'object' && options)

                        _setting = op.setting

                        if (!op.nodes) {
                            op.nodes = []
                            $this.find('> li').each(function() {
                                var $li   = $(this)
                                var node  = $li.data()

                                if (node.pid) node.pId = node.pid
                                node.name = $li.html()
                                op.nodes.push(node)
                            })
                            $this.empty()
                        } else {
                            if (typeof op.nodes === 'string') {
                                if (op.nodes.trim().startsWith('[') || op.nodes.trim().startsWith('{')) {
                                    op.nodes = op.nodes.toObj()
                                } else {
                                    op.nodes = op.nodes.toFunc()
                                }
                            }
                            if (typeof op.nodes === 'function') {
                                op.nodes = op.nodes.call(this)
                            }

                            $this.removeAttr('data-nodes')
                        }

                        if (!op.showRemoveBtn) op.showRemoveBtn = false
                        if (!op.showRenameBtn) op.showRenameBtn = false
                        if (op.addHoverDom && typeof op.addHoverDom !== 'function')       op.addHoverDom    = (op.addHoverDom === 'edit')    ? _addHoverDom    : op.addHoverDom.toFunc()
                        if (op.removeHoverDom && typeof op.removeHoverDom !== 'function') op.removeHoverDom = (op.removeHoverDom === 'edit') ? _removeHoverDom : op.removeHoverDom.toFunc()
                        if (!op.maxAddLevel)   op.maxAddLevel   = 3

                        var setting = {
                            view: {
                                addHoverDom    : op.addHoverDom || null,
                                removeHoverDom : op.removeHoverDom || null,
                                addDiyDom      : op.addDiyDom ? op.addDiyDom.toFunc() : null
                            },
                            edit: {
                                enable        : op.editEnable,
                                showRemoveBtn : op.showRemoveBtn,
                                showRenameBtn : op.showRenameBtn
                            },
                            check: {
                                enable    : op.checkEnable,
                                chkStyle  : op.chkStyle,
                                radioType : op.radioType
                            },
                            callback: {
                                onClick       : op.onClick      ? op.onClick.toFunc()      : null,
                                beforeDrag    : op.beforeDrag   ? op.beforeDrag.toFunc()   : _beforeDrag,
                                beforeDrop    : op.beforeDrop   ? op.beforeDrop.toFunc()   : _beforeDrop,
                                onDrop        : op.onDrop       ? op.onDrop.toFunc()       : null,
                                onCheck       : op.onCheck      ? op.onCheck.toFunc()      : null,
                                beforeRemove  : op.beforeRemove ? op.beforeRemove.toFunc() : null,
                                onRemove      : op.onRemove     ? op.onRemove.toFunc()     : null,
                                onNodeCreated : _onNodeCreated,
                                onCollapse    : _onCollapse,
                                onExpand      : _onExpand
                            },
                            data: {
                                simpleData: {
                                    enable: op.simpleData || true
                                },
                                key: {
                                    title: op.title || ''
                                }
                            }
                        }

                        if (_setting && typeof _setting === 'string') _setting = _setting.toObj()
                        if (_setting) $.extend(true, setting, typeof _setting === 'object' && _setting)

                        $.fn.zTree.init($this, setting, op.nodes)

                        var IDMark_A = '_a'
                        var zTree    = $.fn.zTree.getZTreeObj($this.attr('id'))

                        if (op.expandAll) zTree.expandAll(true)

                        // onCreated
                        function _onNodeCreated(event, treeId, treeNode) {
                            if (treeNode.icon) {
                                var $a    = $('#'+ treeNode.tId +'_a')

                                if (!$a.data('icon')) {
                                    $a.data('icon', true)
                                      .addClass('icon')
                                      .find('> span.button').append('<i class="icon icon-'+ treeNode.icon +'"></i>')
                                }
                            }
                            if (op.onNodeCreated) {
                                op.onNodeCreated.toFunc().call(this, event, treeId, treeNode)
                            }
                        }
                        // onCollapse
                        function _onCollapse(event, treeId, treeNode) {
                            if (treeNode.iconClose) {
                                $('#'+ treeNode.tId +'_ico').find('> i').attr('class', 'icon icon-'+ treeNode.iconClose)
                            }

                            if (op.onCollapse) {
                                op.onCollapse.toFunc().call(this, event, treeId, treeNode)
                            }
                        }
                        // onExpand
                        function _onExpand(event, treeId, treeNode) {
                            if (treeNode.icon && treeNode.iconClose) {
                                $('#'+ treeNode.tId +'_ico').find('> i').attr('class', 'icon icon-'+ treeNode.icon)
                            }
                            if (op.onExpand) {
                                op.onExpand.toFunc().call(this, event, treeId, treeNode)
                            }
                        }
                        // add button, del button
                        function _addHoverDom(treeId, treeNode) {
                            var level = treeNode.level
                            var $obj  = $('#'+ treeNode.tId + IDMark_A)
                            var $add  = $('#diyBtn_add_'+ treeNode.getId())
                            var $del  = $('#diyBtn_del_'+ treeNode.getId())
                            var $nameKey = $.fn.zTree.getZTreeObj(treeId).setting.data.key.name

                            if (!$add.length) {
                                if (level < op.maxAddLevel) {
                                    $add = $('<span class="tree_add" id="diyBtn_add_'+ treeNode.getId() +'" title="添加"></span>')
                                    $add.appendTo($obj);
                                    $add.on('click', function(){
                                        var addData = {};
                                        addData[$nameKey] = '新增';
                                        zTree.addNodes(treeNode, addData);
                                    })
                                }
                            }

                            if (!$del.length) {
                                var $del = $('<span class="tree_del" id="diyBtn_del_'+ treeNode.getId() +'" title="删除"></span>')

                                $del
                                    .appendTo($obj)
                                    .on('click', function(event) {
                                        var delFn = function() {
                                            $del.alertmsg('confirm', '确认要删除 '+ treeNode.getName() +' 吗？', {
                                                okCall: function() {
                                                    zTree.removeNode(treeNode)
                                                    if (op.onRemove) {
                                                        var fn = op.onRemove.toFunc()

                                                        if (fn) fn.call(this, event, treeId, treeNode)
                                                    }
                                                },
                                                cancelCall: function () {
                                                    return
                                                }
                                            })
                                        }

                                        if (op.beforeRemove) {
                                            var fn = op.beforeRemove.toFunc()

                                            if (fn) {
                                                var isdel = fn.call(fn, treeId, treeNode)

                                                if (isdel && isdel == true) delFn()
                                            }
                                        } else {
                                            delFn()
                                        }
                                    }
                                )
                            }
                        }

                        // remove add button && del button
                        function _removeHoverDom(treeId, treeNode) {
                            var $add = $('#diyBtn_add_'+ treeNode.getId())
                            var $del = $('#diyBtn_del_'+ treeNode.getId())

                            if ($add && $add.length) {
                                $add.off('click').remove()
                            }

                            if ($del && $del.length) {
                                $del.off('click').remove()
                            }
                        }

                        // Drag
                        function _beforeDrag(treeId, treeNodes) {
                            for (var i = 0; i < treeNodes.length; i++) {
                                if (treeNodes[i].drag === false) {
                                    return false
                                }
                            }
                            return true
                        }

                        function _beforeDrop(treeId, treeNodes, targetNode, moveType) {
                            return targetNode ? targetNode.drop !== false : true
                        }
                    })

                    /* zTree - drop-down selector */

                    var $selectzTree = $box.find('[data-toggle="selectztree"]')

                    $selectzTree.each(function() {
                        var $this   = $(this)
                        var options = $this.data(),
                            $tree   = $(options.tree),
                            w       = parseFloat($this.css('width')),
                            h       = $this.outerHeight()

                        options.width   = options.width || $this.outerWidth()
                        options.height  = options.height || 'auto'

                        if (!$tree || !$tree.length) return

                        var treeid = $tree.attr('id')
                        var $box   = $('#'+ treeid +'_select_box')
                        var setPosition = function($box) {
                            var top        = $this.offset().top,
                                left       = $this.offset().left,
                                $clone     = $tree.clone().appendTo($('body')),
                                treeHeight = $clone.outerHeight()

                            $clone.remove()

                            var offsetBot = $(window).height() - treeHeight - top - h,
                                maxHeight = $(window).height() - top - h

                            if (options.height == 'auto' && offsetBot < 0) maxHeight = maxHeight + offsetBot
                            $box.css({top:(top + h), left:left, 'max-height':maxHeight})
                        }

                        $this.click(function() {
                            if ($box && $box.length) {
                                setPosition($box)
                                $box.show()
                                return
                            }

                            var zindex = 2
                            var dialog = $.CurrentDialog

                            if (dialog && dialog.length) {
                                zindex = dialog.css('zIndex') + 1
                            }
                            $box  = $('<div id="'+ treeid +'_select_box" class="tree-box"></div>')
                                        .css({position:'absolute', 'zIndex':zindex, 'min-width':options.width, height:options.height, overflow:'auto', background:'#FAFAFA', border:'1px #EEE solid'})
                                        .hide()
                                        .appendTo($('body'))

                            $tree.appendTo($box).css('width','100%').data('fromObj', $this).removeClass('hide').show()
                            setPosition($box)
                            $box.show()
                        })

                        $('body').on('mousedown', function(e) {
                            var $target = $(e.target)

                            if (!($this[0] == e.target || ($box && $box.length > 0 && $target.closest('.tree-box').length > 0))) {
                                $box.hide()
                            }
                        })

                        var $scroll = $this.closest('.zui-pageContent')

                        if ($scroll && $scroll.length) {
                            $scroll.scroll(function() {
                                if ($box && $box.length) {
                                    setPosition($box)
                                }
                            })
                        }

                        //destroy selectzTree
                        $this.on('destroy.zui.selectztree', function() {
                            $box.remove()
                        })
                    })
                }
            });
        }
        /* accordion */
        $box.find('[data-toggle="accordion"]').each(function() {
            var $this = $(this), hBox = $this.data('heightbox'), height = $this.data('height')
            var initAccordion = function(hBox, height) {
                var offsety   = $this.data('offsety') || 0,
                    height    = height || ($(hBox).outerHeight() - (offsety * 1)),
                    $pheader  = $this.find('.panel-heading'),
                    h1        = $pheader.outerHeight()

                h1 = (h1 + 1) * $pheader.length
                $this.css('height', height)
                height = height - h1
                $this.find('.panel-collapse').find('.panel-body').css('height', height)
            }

            if ($this.find('> .panel').length) {
                if (hBox || height) {
                    initAccordion(hBox, height)
                    $(window).resize(function() {
                        initAccordion(hBox, height)
                    })

                    $this.on('hidden.bs.collapse', function (e) {
                        var $last = $(this).find('> .panel:last'), $a = $last.find('> .panel-heading > h4 > a')

                        if ($a.hasClass('collapsed'))
                            $last.css('border-bottom', '1px #ddd solid')
                    })
                }
            }
        })

        var $colorpicker = $box.find('[data-toggle="colorpicker"]');
        if($colorpicker.length) {
            Do.ready('colorpicker', function(){
                if(typeof($colorpicker)==='undefined' || $colorpicker.length===0)
                var $colorpicker = $box.find('[data-toggle="colorpicker"]');
                if ($.fn.colorpicker) {
                    /* colorpicker */
                    $colorpicker.each(function() {
                        var $this     = $(this)
                        var isbgcolor = $this.data('bgcolor')

                        $this.colorpicker()
                        if (isbgcolor) {
                            $this.on('changeColor', function(ev) {
                                $this.css('background-color', ev.color.toHex())
                            })
                        }
                    })

                    $box.find('[data-toggle="clearcolor"]').each(function() {
                        var $this   = $(this)
                        var $target = $this.data('target') ? $($this.data('target')) : null

                        if ($target && $target.length) {
                            $this.click(function() {
                                $target.val('')
                                if ($target.data('bgcolor')) $target.css('background-color', '')
                            })
                        }
                    })
                }
            });
        }

        /* tooltip */
        $box.find('[data-toggle="tooltip"]').each(function() {
            $(this).tooltip()
        })

        /* fixed dropdown-menu width */
        $box.find('[data-toggle="dropdown"]').parent().on('show.bs.dropdown', function(e) {
            var $this = $(this), width = $this.outerWidth(), $menu = $this.find('> .dropdown-menu'), menuWidth = $menu.outerWidth()

            if (width > menuWidth) {
                $menu.css('min-width', width)
            }
        })

        /* form validate */
        var vform = $box.find('form[data-toggle="validate"]');
        var aform = $box.find('form[data-toggle="ajaxform"]');
        if(aform.length>0) {
            aform.attr('ajax', true).zhpForm();
        }
        if(vform.length>0){
            Do.ready('validator', 'tips', function() {
                if(typeof(vform)==='undefined' || vform.length===0)
                var vform = $box.find('form[data-toggle="validate"]');
                if(typeof(aform)==='undefined' || aform.length===0)
                var aform = $box.find('form[data-toggle="ajaxform"]');
                vform.each(function() {
                    $(this)
                        .validator({
                            msgMaker: function(msg){
                                if(msg.type==='error'){
                                    toastr.error(msg.msg);
                                }
                            },
                            valid: function(form) {
                                $(form).zuiajax('ajaxForm', $(form).data())
                            },
                            validClass : '',
                            invalidClass: '',
                            theme      : 'red_right_effect'
                        })
                        .on('invalid.field', function(e, field, errors) {
                        	$(field.element).parents('.form-group').addClass('has-error');
                    	})
                        .on('valid.field', function(e, field, errors) {
                        	$(field.element).parents('.form-group').removeClass('has-error');
                    	})
                        .on('invalid.form', function(e, form, errors) {
                            toastr.error($.zui.regional.validatemsg.replaceMsg(errors.length));
                        })
                });
            })
        }

        $box.find('form').each(function() {
            $(this).show();
        });

        var $highcharts = $box.find('[data-toggle="highcharts"]')
        if($highcharts.length>0){
            Do.ready('highcharts', function(){
                if(typeof($highcharts)==='undefined' || $highcharts.length===0)
                var $highcharts = $box.find('[data-toggle="highcharts"]')
                $highcharts.each(function(){
                    var $element = $(this)
                    var options  = $element.data()

                    $.get(options.url, function(chartData){
                        $element.highcharts(chartData)
                    }, 'json')
                })
            });
        }

        var $echarts = $box.find('[data-toggle="echarts"]');
        if($echarts.length>0){
            Do.ready('echarts', function(){
                if(typeof($echarts)==='undefined' || $echarts.length===0)
                var $echarts = $box.find('[data-toggle="echarts"]');
                $echarts.each(function(){
                    var $element = $(this)
                    var options  = $element.data()
                    var theme    = options.theme ? options.theme : 'default'
                    var typeArr  = options.type.split(',')

                    require.config({
                        paths: {
                            echarts: Do.getConfig('basepath') + 'lib/echarts'
                        }
                    })

                    require(
                        [
                            'echarts',
                            'echarts/theme/' + theme,
                            'echarts/chart/' + typeArr[0],
                            typeArr[1] ? 'echarts/chart/' + typeArr[1] : 'echarts'
                        ],
                        function (ec,theme) {
                            var myChart = ec.init($element[0],theme)

                            $.get(options.url, function(chartData){
                                myChart.setOption(chartData)
                            }, 'json')
                        }
                    )
                })

            });
        }


    })

}(jQuery);