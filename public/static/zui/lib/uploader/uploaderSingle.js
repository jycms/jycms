
    //上传图片
    $.fn.zhpImgUpload = function (options) {
        var defaults = {
            paste: document.body,
            //dnd: '#uploader .queueList',
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            },
            compress: {
                width: 800,
                height: 800,
                // 图片质量，只有type为`image/jpeg`的时候才有效。
                quality: 90,
                // 是否允许放大，如果想要生成小图的时候不失真，此选项应该设置为false.
                allowMagnify: false,
                // 是否允许裁剪。
                crop: false,
                // 是否保留头部meta信息。
                preserveHeaders: true,
                // 如果发现压缩后文件大小比原来还大，则使用原来图片
                // 此属性可能会影响图片自动纠正功能
                noCompressIfLarger: false,
                // 单位字节，如果图片大小小于此值，不会采用压缩。
                compressSize: 0
            },
            disableGlobalDnd: false,
            chunked: true,
            //fileNumLimit: options.maxnum ? options.maxnum : false, //TODO 文件总数，应该再有一个关联项来记录已经上传的个数
            //fileSizeLimit: 6 * 1024 * 1024, //上传总尺寸 10M
            //fileSingleSizeLimit: 1 * 1024 * 1024, //单个文件尺寸 1M
            deleteUrl: zhpConfig.deleteUrl,
            uploadUrl: zhpConfig.uploadUrl,
            sortable: true,
            hasdesc: false,
            uploadtype: 0,
            type: '',
            cut: false,
            cutratio: 1,
            cutwidth: 300,
            cutheight: 300,
            value: ''
        }
        if (options.type)
            defaults.accept.extensions = options.type;
        options = $.extend(defaults, options);
        this.each(function () {
            var upButton = $(this);
            //加载HTML标签中的设置
            var obj = {};
            var data = new Array();
            $.each(this.attributes, function () {
                if (this.name.indexOf("data-") == 0) {
                    if (this.value == 'true')
                        this.value = true;
                    if (this.value == 'false')
                        this.value = false;
                    var key = this.name.replace("data-", "");
                    obj[key] = this.value;
                    data.push(this.name);
                }
            });
            $.each(data, function (index, value) {
                upButton.removeAttr(value);
            })

            options = $.extend(options, obj);

            var uploaderId = $(this).attr('id');
            if (!uploaderId) {
                uploaderId = $.zui.uuid();
            }

            var $previewArea = options.preview ? options.preview : upButton.attr('preview');
            // 图片容器
            if (!$previewArea) {
                $previewArea = uploaderId + '_preview';
            }
            var $preview = $('#' + $previewArea);
            if (!$preview.attr('id')) {
                var sss = '';
                if (options.src)
                    sss = ' src="' + options.src + '"';
                $preview = $('<div id="' + $previewArea + '"><img' + sss + '></div>').appendTo(upButton.parent());
            }
            $preview.addClass('preview');
            $preview.addClass('webuploader');
            var urlValId = upButton.attr('data');
            var urlVal = $('#' + urlValId);
            if (!urlVal.length) {
                urlVal = $('<input>').attr('id', urlValId).attr('name', urlValId).attr('type', 'hidden');
                urlVal.val(options.value);
                urlVal.appendTo(upButton.parent());
            }
            if (options.required) {
                urlVal.attr('datatype', '*');
            }

            var $progress = $('<div style="position: absolute; bottom: 0;padding: 5px; display: none;"></div>');
            $progress.append('<div class="progress"><div class="progress-bar"></div></div>');
            $progress.appendTo($preview);
            // 上传按钮
            var $btnsId = uploaderId + '_btns';
            var $btns = $('<div>').attr('id', $btnsId).css('float', 'left');
            var $uploadBtn = $('<div>').addClass('webuploader-pick btn btn-primary').css({'float': 'left', 'margin-left': '2px'}).html(zhpLang('Upload'));
            var $selectImg = $('<div>').addClass('btns').css({position: 'absolute', top: '2px', left: '2px'}).append($btns).append($uploadBtn).hide();
            $selectImg.appendTo($preview);
            /*创建上传*/
            Do.ready('webuploader', 'dialog', 'cropper', function () {
                console.log('singer,' + uploaderId);
                window[uploaderId] = WebUploader.create($.extend({
                    swf: zhpConfig.libDir + 'webuploader/Uploader.swf',
                    server: options.uploadUrl,
                    pick: {
                        id: upButton,
                        label: options.text ? options.text : zhpLang('Select pictures'),
                        multiple: false
                    },
                    resize: true,
                    auto: false,
                    cutData: []
                }, options));
                // 添加“添加文件”的按钮，
                window[uploaderId].addButton({
                    id: '#' + $btnsId,
                    label: options.text ? options.text : zhpLang('Select pictures'),
                    style: 'btn btn-primary'
                });

                /**
                 * 将文件添加到队列前的操作
                 * @param {type} file
                 * @returns {undefined}
                 */
                window[uploaderId].onBeforeFileQueued = function (file) {
                    $uploadBtn.show();
                    upButton.hide();
                    $selectImg.show();
                    //修复一下按钮的内部控件的尺寸
                    $selectImg.children().first().children().next().width($('#' + $btnsId).width()).height($('#' + $btnsId).height());
                    this.reset();
                }

                /**
                 * 添加文件到上传队列
                 * @param {type} file
                 * @returns {uploaderSingle_L34}
                 */
                window[uploaderId].onFileQueued = function (file) {
                    var $img = $preview.find('img');
                    if (!this.cutReady)
                        this.cutReady = 1;
                    var $this = this;
                    window[uploaderId].makeThumb(file, function (error, src) {
                        if (error) {
                            $preview.replaceWith('<span>不能预览</span>');
                            return;
                        }
                        $img.attr('src', src);
                        //$uploadBtn.css({display: 'block'});
                        $progress.css({width: $img.width()}).show();
                        $.zui.imgReady(src, function () {
                            if (options.cut) {
                                if ($this.cutReady == 1) {
                                    $img.cropper({
                                        strict: true,
                                        aspectRatio: options.cutratio,
                                        autoCrop: true,
                                        background: true,
                                        center: true,
                                        checkImageOrigin: false,
                                        cropBoxMovable: true,
                                        cropBoxResizable: true,
                                        doubleClickToggle: false,
                                        dragCrop: false,
                                        guides: true,
                                        highlight: true,
                                        minCanvasHeight: 0,
                                        minCanvasWidth: 0,
                                        minContainerHeight: 100,
                                        minContainerWidth: 200,
                                        minCropBoxHeight: 0,
                                        minCropBoxWidth: 0,
                                        modal: true,
                                        mouseWheelZoom: true,
                                        movable: true,
                                        preview: ".img-preview",
                                        responsive: false,
                                        rotatable: false,
                                        scalable: false,
                                        strict: true,
                                                touchDragZoom: true,
                                        wheelZoomRatio: 0.1,
                                        zoomable: true,
                                        crop: function (e) {
                                            var json = [
                                                '{"x":' + e.x,
                                                '"y":' + e.y,
                                                '"oh":' + e.currentTarget.naturalHeight,
                                                '"ow":' + e.currentTarget.naturalWidth,
                                                '"height":' + e.height,
                                                '"width":' + e.width,
                                                '"cutheight":' + options.cutheight,
                                                '"cutwidth":' + options.cutwidth,
                                                '"rotate":"' + e.rotate + '"}'
                                            ].join();
                                            $this.cutData = json;
                                        }
                                    });
                                    $this.cutReady = 2;
                                } else {
                                    $img.cropper('replace', src);
                                    $img.data().cropper.crop();
                                }
                            }
                        });
                    }, 960, 540);

                    file.on('statuschange', function (cur, prev) {
                        if (prev === 'progress') {
                            $progress.hide().width(0);
                            $uploadBtn.hide();
                        }
                        // 成功
                        if (cur === 'error' || cur === 'invalid') {
                            zalert(file.statusText);
                            window[uploaderId].reset();
                            $img.attr('src', '');
                        } else if (cur === 'interrupt') {
                            //zalert('上传暂停');
                        } else if (cur === 'progress') {
                            $uploadBtn.hide();
                            $progress.show();
                        } else if (cur === 'complete') {
                        }
                    });
                }

                /**
                 * 从队列中删除文件
                 * @param {type} file
                 * @returns {uploaderSingle_L34}
                 */
                window[uploaderId].onFileDequeued = function (file) {
                }

                /**
                 * 开始上传
                 * @param {type} file
                 * @returns {undefined}
                 */
                window[uploaderId].onUploadStart = function (file) {
                    window[uploaderId].option('formData', {'uploadtype': options.uploadtype, 'ajax': 1, 'cutdata': this.cutData});
                    upButton.attr('disabled', true);
                    upButton.find('.webuploader-pick span').text(' 等待');
                }

                /**
                 * 处理进度条
                 * @param {type} file
                 * @param {type} percentage
                 * @returns {undefined}
                 */
                window[uploaderId].onUploadProgress = function (file, percentage) {
                    var $percent = $progress.children().children().first();
                    $percent.css('width', percentage * 100 + '%');
                    $percent.text(Math.round(percentage * 100) + '%');

                }

                /**
                 * 上传完成
                 * @param {type} file
                 * @param {type} result
                 * @returns {undefined}
                 */
                window[uploaderId].onUploadSuccess = function (file, result) {
                    var $this = this;
                    upButton.attr('disabled', false);
                    upButton.find('.webuploader-pick span').text(' 上传');
                    if (result.status) {
                        urlVal.val(result.file.savepath + result.file.savename);
                        //TODO 上传完成后的操作
                        //options.complete(result.file);
                        var $img = $preview.find('img');//$('#uploader_avatar_preview img').data()
                        $img.cropper('destroy');
                        $img.attr('src', result.file.url);
                        $this.cutReady = 1;
                    } else {
                        zalert(result.info);
                    }
                }

                /**
                 * 上传出错
                 * @param {type} param1
                 * @param {type} param2
                 */
                window[uploaderId].onError = function (code) {
                    var msg = code;
                    switch (code) {
                        case 'Q_TYPE_DENIED':
                            msg = '对不起，你选择了系统不支持的图片类型！';
                            break;
                        case 'Q_EXCEED_NUM_LIMIT':
                            msg = '你选择了太多的文件了！最多只能选择' + window[uploaderId].options.maxnum + '个文件！';
                            break;
                        case 'F_EXCEED_SIZE':
                            msg = '文件太大了，你只能提交不大于' + window[uploaderId].options.fileSingleSizeLimit + '字节的文件！';
                            break;
                        case 'F_DUPLICATE':
                            msg = '这个文件已经选择了，请选择其它文件！';
                            break;
                        case 'Q_EXCEED_SIZE_LIMIT':
                            msg = '你选择的这些文件超出了最大限额，你本次总上传空间为' + window[uploaderId].options.fileSizeLimit + '字节！';
                            break;
                    }
                    zalert(msg);
                }

                /**
                 * 上传出错
                 * @param {type} param1
                 * @param {type} param2
                 */
                window[uploaderId].onUploadError = function (file) {
                    zalert('文件上传失败');
                }

                $uploadBtn.on('click', function () {
                    if ($(this).hasClass('disabled')) {
                        return false;
                    }
                    window[uploaderId].upload();
                });

            });
        });
    }
