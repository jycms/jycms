    //多图上传，同一页面只允许有一个批量上传控件
    $.fn.zhpMultiUpload = function (options) {
        var defaults = {
            paste: document.body,
            //dnd: '#uploader .queueList',
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            },
            compress: {
                width: 800,
                height: 800,
                // 图片质量，只有type为`image/jpeg`的时候才有效。
                quality: 90,
                // 是否允许放大，如果想要生成小图的时候不失真，此选项应该设置为false.
                allowMagnify: false,
                // 是否允许裁剪。
                crop: false,
                // 是否保留头部meta信息。
                preserveHeaders: true,
                // 如果发现压缩后文件大小比原来还大，则使用原来图片
                // 此属性可能会影响图片自动纠正功能
                noCompressIfLarger: false,
                // 单位字节，如果图片大小小于此值，不会采用压缩。
                compressSize: 0
            },
            disableGlobalDnd: false,
            chunked: true,
            fileNumLimit: options.maxnum ? options.maxnum : false, //TODO 文件总数，应该再有一个关联项来记录已经上传的个数
            //fileSizeLimit: 6 * 1024 * 1024, //上传总尺寸 10M
            //fileSingleSizeLimit: 1 * 1024 * 1024, //单个文件尺寸 1M
            deleteUrl: zhpConfig.deleteUrl,
            uploadUrl: zhpConfig.uploadUrl,
            sortable: true,
            hasdesc: false
        }
        if (options.type)
            defaults.accept.extensions = options.type;
        var options = $.extend(defaults, options);
        this.each(function () {
            var upButton = $(this);
			var $dataname = $(this).attr('data');

            //加载HTML标签中的设置
            var obj = {};
            var data = new Array();
            $.each(this.attributes, function () {
                if (this.name.indexOf("data-") == 0) {
                    if (this.value == 'true')
                        this.value = true;
                    if (this.value == 'false')
                        this.value = false;
                    var key = this.name.replace("data-", "");
                    obj[key] = this.value;
                    data.push(this.name);
                }
            });
            $.each(data, function (index, value) {
                upButton.removeAttr(value);
            })
			upButton.append($('<div id="filePicker"></div>'));
            options = $.extend(options, obj);
            var uploaderId = $(this).attr('id');
            if (!uploaderId) {
                uploaderId = $.zui.uuid();
            }

            var $previewArea = options.preview ? options.preview : upButton.attr('preview');

            var $queue = $('<ul class="filelist grid"></ul>');
            
            // 图片容器
            if (!$previewArea) {
                $previewArea = uploaderId + '_preview';
            }
            var $preview = $('#' + $previewArea);
            if (!$preview.attr('id')) {
                $preview = $('<div id="' + $previewArea + '"></div>').appendTo(upButton);
            } else {
                $preview.addClass('webuploader');
            }
            $preview.addClass('preview');
            if (!$preview.find('ul').length) {
                $queue.appendTo($preview);
            } else {
                var $queue = $preview.find('ul').addClass('filelist grid');
            }
            $queue.css('width', upButton.width());
            // 状态栏，包括进度和控制按钮
            var $statusBar = $('<div class="statusBar" style="display:none;"></div>')
                    .appendTo(upButton),
                    // 总体进度条
                    $progress = $('<div class="progress"><div class="progress-bar"></div></div>').appendTo($statusBar).hide(),
                    // 文件总体选择信息。
                    $info = $('<div class="info"></div>').appendTo($statusBar),
                    // 添加的文件数量
                    fileCount = 0,
                    // 添加的文件总大小
                    fileSize = 0,
                    // 优化retina, 在retina下这个值是2
                    ratio = window.devicePixelRatio || 1,
                    // 缩略图大小
                    thumbnailWidth = 300 * ratio,
                    thumbnailHeight = 180 * ratio,
                    // 可能有pedding, ready, uploading, confirm, done.
                    state = 'pedding',
                    // 所有文件的进度信息，key为file id
                    percentages = {};

            var $btnsId = uploaderId + '_btns';
            var $btns = $('<div>').attr('id', $btnsId).css('float', 'left');
            var $uploadBtn = $('<div>').addClass('webuploader-pick btn btn-primary').css({'float': 'left', 'margin-left': '2px'}).html(zhpLang('Upload'));
            //var $selectImg = $('<div>').addClass('btns').css({position: 'absolute', top: '2px', left: '2px'}).append($btns).append($uploadBtn).hide();
            //$selectImg.appendTo($preview);
            upButton.append($btns).append($uploadBtn);

            var $data = $('#' + $dataname + '_list');
            if (!$data.length)
                var $data = $('<div style="display:none;"></div>').attr('id', $dataname + '_list').appendTo(upButton);
            else {
                $data.find('input').each(function (index, e) {
                    $li = $(e);
                    $li.attr('id', uploaderId + (++index));
                })
            }

            /*创建上传*/
            Do.ready('webuploader', 'tips', 'sortable', 'dialog', function () {
                window[uploaderId] = WebUploader.create($.extend({
                    swf: zhpConfig.libDir + 'webuploader/Uploader.swf',
                    server: options.uploadUrl,
                    pick: {
                        id: '#filePicker',
                        label: options.text ? options.text : zhpLang('Select pictures'),
                        multiple: true
                    },
                    resize: false,
                    auto: false,
                    cutData: []
                }, options));
                // 添加“添加文件”的按钮，
                window[uploaderId].addButton({
                    id: '#' + $btnsId,
                    label: options.text ? options.text : zhpLang('Select pictures'),
                    style: 'btn btn-primary'
                });
				$('#filePicker').hide();
                /**
                 * 将文件添加到队列前的操作
                 * @param {type} file
                 * @returns {undefined}
                 */
                window[uploaderId].onBeforeFileQueued = function (file) {
                }

                /**
                 * 添加文件到上传队列
                 * @param {type} file
                 * @returns {uploaderSingle_L34}
                 */
                window[uploaderId].onFileQueued = function (file) {
                    fileCount++;
                    fileSize += file.size;
                    if (fileCount === 1) {
                        $statusBar.show();
                    }
                    //创建view
                    $btns.show();
                    $uploadBtn.removeClass('disabled');
                    $('#filePicker').hide();
                    var order = file.id.replace('WU_FILE_', '');
                    var hd = '';
                    if (window[uploaderId].options.hasdesc) {
                        hd = '<p class="imgDesc"><textarea name="file_desc[' + order + ']" class="form-control large"></textarea></p>';
                    }
                    var $li = $('<li class="" id="' + file.id + '" data-order="' + order + '">' +
                            '<p class="imgWrap"></p>' + hd +
                            '</li>');
                    var $cbtns = getBtns(file);
                    $li.append($cbtns);
                    var $wrap = $li.find('p.imgWrap'),
                            $info = $('<p class="error"></p>'),
                            showError = function (code) {
                                switch (code) {
                                    case 'exceed_size':
                                        text = '文件大小超出';
                                        break;
                                    case 'interrupt':
                                        text = '上传暂停';
                                        break;
                                    default:
                                        text = '上传失败，请重试';
                                        break;
                                }
                                $info.text(text).appendTo($li);
                            }
                    if (file.getStatus() === 'invalid') {
                        showError(file.statusText);
                    } else {
                        // @todo 图片裁剪，点击显示大图
                        $wrap.text('预览中');
                        window[uploaderId].makeThumb(file, function (error, src) {
                            if (error) {
                                $wrap.text('不能预览');
                                return;
                            }
                            var img = $('<img src="' + src + '">');
                            $wrap.empty().append(img);
                        }, thumbnailWidth, thumbnailHeight);
                        percentages[ file.id ] = [file.size, 0];
                        file.rotation = 0;
                    }
                    file.on('statuschange', function (cur, prev) {
                        // 成功
                        if (cur === 'error' || cur === 'invalid') {
                            showError(file.statusText);
                            percentages[ file.id ][ 1 ] = 1;
                        } else if (cur === 'interrupt') {
                            showError('interrupt');
                        } else if (cur === 'queued') {
                            percentages[ file.id ][ 1 ] = 0;
                        } else if (cur === 'progress') {
                            $info.remove();
                        } else if (cur === 'complete') {
                            $li.append('<span class="success"></span>');
                        }
                        $li.removeClass('state-' + prev).addClass('state-' + cur);
                    });

                    $li.on('mouseenter', function () {
                        $cbtns.stop().animate({height: 30});
                    });

                    $li.on('mouseleave', function () {
                        $cbtns.stop().animate({height: 0});
                    });

                    $li.appendTo($queue);
                    sortFile();
                    //$title = $('<p class="title">this is a cover</p>');
                    //$('.filelist p.title').remove();
                    //$('.filelist').children().first().append($title);
					//view 创建完成

					setState('ready');
                    updateTotalProgress();
                }

                /**
                 * 从队列中删除文件
                 * @param {type} file
                 * @returns {uploaderSingle_L34}
                 */
                window[uploaderId].onFileDequeued = function (file) {
                    fileCount--;
                    fileSize -= file.size;
                    if (!fileCount) {
                        setState('pedding');
                    }
					//销毁View
                    var $li = $('#' + file.id);
                    delete percentages[ file.id ];
                    updateTotalProgress();
                    $li.off().find('.file-panel').off().end().remove();

                    updateTotalProgress();
                }

                /**
                 * 开始上传
                 * @param {type} file
                 * @returns {undefined}
                 */
                window[uploaderId].onUploadStart = function (file) {
                    window[uploaderId].option('formData', {'uploadtype': options.uploadtype, 'ajax': 1, 'packid': $('#id').val()});
                    upButton.attr('disabled', true);
                    upButton.find('.webuploader-pick span').text(' 等待');
                }

                /**
                 * 处理进度条
                 * @param {type} file
                 * @param {type} percentage
                 * @returns {undefined}
                 */
                window[uploaderId].onUploadProgress = function (file, percentage) {
                    var $li = $('#' + file.id),
                            $percent = $li.find('.progress span');

                    $percent.css('width', percentage * 100 + '%');
                    percentages[ file.id ][ 1 ] = percentage;
                    updateTotalProgress();
                }

                /**
                 * 上传完成
                 * @param {type} file
                 * @param {type} result
                 * @returns {undefined}
                 */
                window[uploaderId].onUploadSuccess = function (file, result) {
                    if (result.status == 0) {
                        toastr.error(result.info);
                        window[uploaderId].cancelFile(file);
                        window[uploaderId].stop(true);
                    } else {
                        //TODO 上传的结果需要实时展现，并支持删除，下一版实现
                        var id = uploaderId + file.id.replace('WU_FILE_', '');
                        $data.append("<input type='hidden' id='" + id + "' name='" + $dataname + "[" + file.id.replace('WU_FILE_', '') + "]'  value='" + result.file.savepath + result.file.savename + "' />");
                    }
                }

                /**
                 * 上传出错
                 * @param {type} param1
                 * @param {type} param2
                 */
                window[uploaderId].onError = function (code) {
                    var msg = code;
                    switch (code) {
                        case 'Q_TYPE_DENIED':
                            msg = '对不起，你选择了系统不支持的图片类型！';
                            break;
                        case 'Q_EXCEED_NUM_LIMIT':
                            msg = '你选择了太多的文件了！最多只能选择' + window[uploaderId].options.maxnum + '个文件！';
                            break;
                        case 'F_EXCEED_SIZE':
                            msg = '文件太大了，你只能提交不大于' + window[uploaderId].options.fileSingleSizeLimit + '字节的文件！';
                            break;
                        case 'F_DUPLICATE':
                            msg = '这个文件已经选择了，请选择其它文件！';
                            break;
                        case 'Q_EXCEED_SIZE_LIMIT':
                            msg = '你选择的这些文件超出了最大限额，你本次总上传空间为' + window[uploaderId].options.fileSizeLimit + '字节！';
                            break;
                    }
                    zalert(msg);
                }

                /**
                 * 上传出错
                 * @param {type} param1
                 * @param {type} param2
                 */
                window[uploaderId].onUploadError = function (file) {
                    zalert('文件上传失败');
                }

                /**
                 * 其它操作处理
                 * @param {type} param1
                 * @param {type} param2
                 * @param {type} param3
                 */
                window[uploaderId].on('all', function (type) {
                    switch (type) {
                        case 'uploadFinished':
                            setState('confirm');
                            break;
                        case 'startUpload':
                            setState('uploading');
                            break;

                        case 'stopUpload':
                            setState('paused');
                            break;

                    }
                });

                $queue.find('li').each(function (index, e) {
                    var $li = $(e);
                    var id = 'WU_FILE_' + (++index);
                    //这里显示的是以前上传的文件，直接给上传文件个数扣除
                    if (window[uploaderId].options.fileNumLimit != false) {
                        window[uploaderId].options.fileNumLimit--;
                        if (window[uploaderId].options.fileNumLimit == 0)
                            window[uploaderId].options.fileNumLimit = -1;
                    }
                    var $cbtns = getBtns(id);
                    $li.append($cbtns).addClass('state-complete').attr('id', id).attr('data-order', index);
                    $li.on('mouseenter', function () {
                        $cbtns.stop().animate({height: 30});
                    });

                    $li.on('mouseleave', function () {
                        $cbtns.stop().animate({height: 0});
                    });
                })

                function getBtns(file) {
                    if (typeof (file) == 'string') {
						var $id = file.replace('WU_FILE_', '');
					} else {
						var $id = file.id.replace('WU_FILE_', '');
					}
                    var $cbtns = $('<div class="file-panel" data-id="'+$id+'">' +
                            '<span class="cancel">' + zhpLang('Delete') + '</span></div>');
                    $cbtns.on('click', function () {
                        if ($cbtns.parent().hasClass("state-complete") || typeof (file) == 'string') {
                            if (!window[uploaderId].options.deleteUrl)
                                return;
							var $id = $(this).attr('data-id');
                            var $vid = $('#' + uploaderId + $id);
                            $path = $vid.val();
							if(!$path) {
								zalert('奇怪，我好像没发现你要删除什么东西？');
								return false;
							}
                            $.ajax({
                                type: 'get',
                                url: window[uploaderId].options.deleteUrl,
                                data: {path: $path, 'uploadtype': options.uploadtype, 'ajax': 1},
                                cache: false,
                                dataType: 'json',
                                success: function (result) {
                                    if (result.status == 0) {
                                        zalert(result.info);
                                        return;
                                    } else {
                                        if (window[uploaderId].options.fileNumLimit < 1)
                                            window[uploaderId].options.fileNumLimit = 0;
                                        window[uploaderId].options.fileNumLimit++;
                                        if (window[uploaderId].options.fileNumLimit > window[uploaderId].options.maxnum)
                                            window[uploaderId].options.fileNumLimit = window[uploaderId].options.maxnum;
                                        if (typeof (file) == 'string')
                                            $('#' + file).remove();
                                        else
                                            window[uploaderId].removeFile(file);
                                        $vid.remove();
                                    }
                                },
                                error: function (result) {
                                    if (result.readyState) {
                                        var result = $.parseJSON(result.responseText);
                                    }
                                    var msg = result.info;
                                    if (!msg)
                                        msg = '未知错误！';
                                    zalert(msg);
                                    console.log('error');
                                    console.log(result);
                                    return;
                                }
                            })
                        } else {
                            window[uploaderId].removeFile(file);
                        }
                    });
                    return $cbtns;
                }

                function sortFile() {
                    if (window[uploaderId].options.sortable)
                        $('.filelist').sortable({
                            selector: 'li',
                            initstart: function (e) {
                                if (window[uploaderId].options.hasdesc) {
                                    e.element.find('textarea').focus();
                                }
                            },
                            start: function () {
                                //$('.filelist p.title').remove();
                                $('.filelist li.invisible').append($('<span class="hints">Drag to here</span>'));
                            },
                            finish: function (e) {
                                $('#displayorder').val($('.filelist').data('zui.sortable').orders.toString());
                                $('.filelist span.hints').remove();
                                if (window[uploaderId].options.hasdesc) {
                                    e.element.find('textarea').focus();
                                }
                                //var $title = $('<p class="title">this is a cover</p>');
                                //$('.filelist').children().first().append($title);
                            },
                        });
                }

                sortFile();

                //更新总进度条
                function updateTotalProgress() {
                    var loaded = 0,
                            total = 0,
                            spans = $progress.children(),
                            percent;
                    $.each(percentages, function (k, v) {
                        total += v[ 0 ];
                        loaded += v[ 0 ] * v[ 1 ];
                    });
                    percent = total ? loaded / total : 0;
                    spans.text(Math.round(percent * 100) + '%');
                    spans.css('width', Math.round(percent * 100) + '%');
                    updateStatus();
                }
                //处理提示信息
                function updateStatus() {
                    var text = '', stats;
                    if (state === 'ready') {
                        text = '选中' + fileCount + '张图片，共' +
                                WebUploader.formatSize(fileSize) + '。';
                    } else if (state === 'confirm') {
                        stats = window[uploaderId].getStats();
                        if (stats.uploadFailNum) {
                            text = '已成功上传' + stats.successNum + '张图片，' +
                                    stats.uploadFailNum + '张图片上传失败，<a class="retry" href="#">重新上传</a>失败图片或<a class="ignore" href="#">忽略</a>'
                        }
                    } else {
                        stats = window[uploaderId].getStats();
                        text = '共' + fileCount + '张（' +
                                WebUploader.formatSize(fileSize) +
                                '），已上传' + stats.successNum + '张';
                        if (stats.uploadFailNum) {
                            text += '，失败' + stats.uploadFailNum + '张';
                        }
                    }
                    $info.html(text);
                }

				//状态监控
                function setState(val) {
                    var file, stats;
                    if (val === state) {
                        return;
                    }
                    $uploadBtn.removeClass('state-' + state);
                    $uploadBtn.addClass('state-' + val);
                    state = val;
                    switch (state) {
                        case 'pedding':
                            //$queue.hide();
                            $uploadBtn.hide();
                            $statusBar.addClass('element-invisible');
                            window[uploaderId].refresh();
                            break;
                        case 'ready':
                            $('#'+$btnsId).removeClass('element-invisible');
                            //$queue.show();
                            $uploadBtn.show();
                            $statusBar.removeClass('element-invisible');
                            window[uploaderId].refresh();
                            break;
                        case 'uploading':
                            $('#'+$btnsId).addClass('element-invisible');
                            $progress.show();
                            $uploadBtn.text('暂停上传');
                            break;
                        case 'paused':
                            $progress.show();
                            $uploadBtn.text('继续上传');
                            break;
                        case 'confirm':
                            $progress.hide();
                            $uploadBtn.text(zhpLang('Upload')).addClass('disabled');
                            stats = window[uploaderId].getStats();
                            if (stats.successNum && !stats.uploadFailNum) {
                                setState('finish');
                                return;
                            }
                            break;
                        case 'finish':
                            $('#'+$btnsId).removeClass('element-invisible');
                            stats = window[uploaderId].getStats();
                            if (stats.successNum) {
                                toastr.info('文件已经成功上传，编辑完信息后请点击“提交”...');
                            } else {
                                // 没有成功的图片，重设
                                state = 'done';
                                location.reload();
                            }
                            break;
                    }
                    updateStatus();
                }

				$uploadBtn.on('click', function () {
                    if ($(this).hasClass('disabled')) {
                        return false;
                    }
                    if (state === 'ready') {
                        window[uploaderId].upload();
                    } else if (state === 'paused') {
                        window[uploaderId].upload();
                    } else if (state === 'uploading') {
                        window[uploaderId].stop();
                    }
                });

				//批量上传时的排序
                fileSort = function (list1, list2) {
                    var order = $('.filelist').data('zui.sortable').orders;
                    var id1 = list1.id.replace('WU_FILE_', '');
                    var id2 = list2.id.replace('WU_FILE_', '');
                    var id1 = $('#' + list1.id).attr('data-order');
                    var id2 = $('#' + list2.id).attr('data-order');
                    return id1 - id2;
                }

                $info.on('click', '.retry', function () {
                    window[uploaderId].retry();
                });

                $info.on('click', '.ignore', function () {
                    zalert('oops!');
                });
                $uploadBtn.addClass('state-' + state);
                updateTotalProgress();
            });
        });
    }
