/**
* jQuery ligerUI 1.3.2
*
* http://ligerui.com
*
* Author daomi 2015 [ gd_star@163.com ]
*
*/

(function ($)
{
    var l = $.ligerui;

    $.fn.ligerGrid = function (options)
    {
        return $.ligerui.run.call(this, "ligerGrid", arguments);
    };

    $.ligerDefaults.Grid = {
        gridTitle: null,
        width: 'auto',                          //宽度值
        height: 'auto',                         //宽度值
        columnWidth: null,                      //默认列宽度
        resizable: true,                        //table是否可伸缩
        dataUrl: false,                             //ajax url
        urlParms: null,                         //url带参数
        data: null,                             //初始化数据
        usePager: true,                         //是否分页
        page: 1,                                //默认当前页
        pageSize: 10,                           //每页默认的结果数
        pageSizeOptions: [10, 20, 30, 40, 50],  //可选择设定的每页结果数
        showPagenum: 10,
        postData: [],                           //提交到服务器的参数
        columns: [],                            //数据源
        minColToggle: 1,                        //最小显示的列
        dataType: 'server',                     //数据源：本地(local)或(server),本地是将读取p.data。不需要配置，取决于设置了data或是url
        dataAction: 'server',                   //提交数据的方式：本地(local)或(server),选择本地方式时将在客服端分页、排序。
        allowAdjustColWidth: true,              //是否允许调整列宽
        checkbox: false,                        //是否显示复选框
        isSingleCheck: false,                   //复选框选择的时候是否单选模式
        allowHideColumn: true,                  //是否显示'切换列层'按钮
        dateFormat: 'yyyy-MM-dd',               //默认时间显示格式
        inWindow: false,                        //是否以窗口的高度为准 height设置为百分比时可用
        method: 'post',                         //获取数据http方式
        async: true,
        fixedCellHeight: true,                  //是否固定单元格的高度
        heightDiff: 0,                          //高度补差,当设置height:100%时，可能会有高度的误差，可以通过这个属性调整
        pageParmName: 'page',               //页索引参数名，(提交给服务器)
        pagesizeParmName: 'pagesize',        //页记录数参数名，(提交给服务器)
        sortnameParmName: 'sortname',        //页排序列名(提交给服务器)
        sortorderParmName: 'sortorder',      //页排序方向(提交给服务器)
        allowUnSelectRow: false,                //是否允许反选行
        mouseoverRowCssClass: 'datagrid-hover',
        enabledSort: true,                      //是否允许排序
        rowAttrRender: null,                    //行自定义属性渲染器(包括style，也可以定义)
        groupColumnName: null,                  //分组 - 列名
        groupColumnDisplay: '分组',              //分组 - 列显示名字
        groupRender: null,                      //分组 - 渲染器
        delayLoad: false,                       //初始化时是否不加载
        where: null,                            //数据过滤查询函数,(参数一 data item，参数二 data item index)
        selectRowButtonOnly: false,             //复选框模式时，是否只允许点击复选框才能选择行
        selectable: true,
        whenRClickToSelect: false,              //右击行时是否选中
        contentType: null,                      //Ajax contentType参数
        checkboxColWidth: 27,                   //复选框列宽度
        detailColWidth: 29,                     //明细列宽度
        minColumnWidth: 80,
        tree: null,                             //treeGrid模式
        isChecked: null,                        //复选框 初始化函数
        isSelected: null,                       //选择 初始化函数
        frozen: true,                           //是否固定列
        frozenDetail: false,                    //明细按钮是否在固定列中
        frozenCheckbox: true,                   //复选框按钮是否在固定列中
        detail: null,
        detailHeight: 260,
        isShowDetailToggle: null,               //是否显示展开/收缩明细的判断函数
        rownumbers: false,                      //是否显示行序号
        frozenRownumbers: true,                 //行序号是否在固定列中
        rownumbersColWidth: 26,
        autoCheckChildren: true,                //是否自动选中子节点
        rowHeight: 33,                          //行默认的高度
        headerRowHeight: 33,                    //表头行的高度
        toolbar: null,                          //工具条
        toolbarCustom: false, // Html code || function || jQuery dom object, custom elements, displayed on the toolbar
        rowSelectable: true,                    //是否允许选择
        scrollToPage: false,                    //滚动时分页
        scrollToAppend: true,                   //滚动时分页 是否追加分页的形式
        onToggleCol: null,                      //切换列事件
        onChangeSort: null,                     //改变排序事件
        onSuccess: null,                        //成功获取服务器数据的事件
        onDblClickRow: null,                    //双击行事件
        onSelectRow: null,                      //选择行事件
        onBeforeSelectRow:null,                 //选择前事件
        onUnSelectRow: null,                    //取消选择行事件
        onBeforeCheckRow: null,                 //选择前事件，可以通过return false阻止操作(复选框)
        onCheckRow: null,                       //选择事件(复选框)
        onBeforeCheckAllRow: null,              //选择前事件，可以通过return false阻止操作(复选框 全选/全不选)
        onCheckAllRow: null,                    //选择事件(复选框 全选/全不选)onextend
        onBeforeShowData: null,                 //显示数据前事件，可以通过reutrn false阻止操作
        onAfterShowData: null,                  //显示完数据事件
        onError: null,                          //错误事件
        onReload: null,                         //刷新事件，可以通过return false来阻止操作
        onToFirst: null,                        //第一页，可以通过return false来阻止操作
        onToPrev: null,                         //上一页，可以通过return false来阻止操作
        onToNext: null,                         //下一页，可以通过return false来阻止操作
        onToLast: null,                         //最后一页，可以通过return false来阻止操作
        onLoading: null,                        //加载时函数
        onLoaded: null,                         //加载完函数
        onContextmenu: null,                    //右击事件
        onGroupExtend: null,                    //分组展开事件
        onGroupCollapse: null,                  //分组收缩事件
        onTreeExpand: null,                     //树展开事件
        onTreeCollapse: null,                   //树收缩事件
        onTreeExpanded: null,                   //树展开事件
        onTreeCollapsed: null,                  //树收缩事件
        onLoadData: null,                       //加载数据前事件
        onHeaderCellBulid: null,
        disableSelect: true,
    };

    $.ligerDefaults.Grid_columns = {
        id: null,
        name: null,
        totalSummary: null,
        label: null,
        headerRender: null,
        isAllowHide: true,
        isSort: false,
        type: null,
        columns: null,
        width: 120,
        minWidth: 80,
        format: null,
        align: 'left',
        hide: false,
        render: null,
        textField: null  //真正显示的字段名,如果设置了，在编辑状态时,会调用创建编辑器的setText和getText方法
    };

    //接口方法扩展
    $.ligerMethos.Grid = $.ligerMethos.Grid || {};

    //排序器扩展
    $.ligerDefaults.Grid.sorters = $.ligerDefaults.Grid.sorters || {};

    //格式化器扩展
    $.ligerDefaults.Grid.formatters = $.ligerDefaults.Grid.formatters || {};

    $.ligerDefaults.Grid.sorters['date'] = function (val1, val2)
    {
        return val1 < val2 ? -1 : val1 > val2 ? 1 : 0;
    };
    $.ligerDefaults.Grid.sorters['int'] = function (val1, val2)
    {
        return parseInt(val1) < parseInt(val2) ? -1 : parseInt(val1) > parseInt(val2) ? 1 : 0;
    };
    $.ligerDefaults.Grid.sorters['float'] = function (val1, val2)
    {
        return parseFloat(val1) < parseFloat(val2) ? -1 : parseFloat(val1) > parseFloat(val2) ? 1 : 0;
    };
    $.ligerDefaults.Grid.sorters['string'] = function (val1, val2)
    {
        if (!val1) return false;
        return val1.localeCompare(val2);
    };


    $.ligerDefaults.Grid.formatters['date'] = function (value, column)
    {
        function getFormatDate(date, dateformat)
        {
            var g = this, p = this.options;
            if (isNaN(date)) return null;
            var format = dateformat;
            var o = {
                "M+": date.getMonth() + 1,
                "d+": date.getDate(),
                "h+": date.getHours(),
                "m+": date.getMinutes(),
                "s+": date.getSeconds(),
                "q+": Math.floor((date.getMonth() + 3) / 3),
                "S": date.getMilliseconds()
            }
            if (/(y+)/.test(format))
            {
                format = format.replace(RegExp.$1, (date.getFullYear() + "")
            .substr(4 - RegExp.$1.length));
            }
            for (var k in o)
            {
                if (new RegExp("(" + k + ")").test(format))
                {
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                : ("00" + o[k]).substr(("" + o[k]).length));
                }
            }
            return format;
        }
        if (!value) return "";
        // /Date(1328423451489)/
        if (typeof (value) == "string" && /^\/Date/.test(value))
        {
            value = value.replace(/^\//, "new ").replace(/\/$/, "");
            eval("value = " + value);
        }
        if (value instanceof Date)
        {
            var format = column.format || this.options.dateFormat || "yyyy-MM-dd";
            return getFormatDate(value, format);
        }
        else
        {
            return value.toString();
        }
    }

    //引用类型,数据形式表现为[id,text]
    $.ligerDefaults.Grid.formatters['ref'] = function (value)
    {
        if ($.isArray(value)) return value.length > 1 ? value[1] : value[0];
        return value;
    };


    $.ligerui.controls.Grid = function (element, options)
    {
        //hoping: 修正多行表头
        options.origH = options.headerRowHeight;
        $.ligerui.controls.Grid.base.constructor.call(this, element, options);
    };

    $.ligerui.controls.Grid.ligerExtend($.ligerui.core.UIComponent, {
        __getType: function ()
        {
            return '$.ligerui.controls.Grid';
        },
        __idPrev: function ()
        {
            return 'grid';
        },
        _extendMethods: function ()
        {
            return $.ligerMethos.Grid;
        },
        _init: function ()
        {
            $.ligerui.controls.Grid.base._init.call(this);
            var g = this, p = this.options;
            p.dataType = p.dataUrl ? "server" : "local";
            if (p.dataType == "local")
            {
                p.data = p.data || [];
                p.dataAction = "local";
            }
            if (!p.frozen)
            {
                p.frozenCheckbox = false;
                p.frozenDetail = false;
                p.frozenRownumbers = false;
            }
            if (p.tree)//启用分页模式
            {
                p.tree.childrenName = p.tree.childrenName || "children";
                p.tree.isParent = p.tree.isParent || function (rowData)
                {
                    var exist = p.tree.childrenName in rowData;
                    return exist;
                };
                p.tree.isExtend = p.tree.isExtend || function (rowData)
                {
                    if ('isextend' in rowData && rowData['isextend'] == false)
                        return false;
                    return true;
                };
            }
            //hoping: 表格里不允许选择
            if (p.disableSelect)
                $('#'+g.id).bind("selectstart",function(){return false;});
        },
        _render: function ()
        {
            var g = this, p = this.options;
            g.grid = $(g.element);
            g.grid.addClass("zui-datagrid");
            var gridhtmlarr = [];
            gridhtmlarr.push("<div class='datagrid-title'></div>");
            gridhtmlarr.push("<div class='datagrid-toolbar' style='display:none'><div class='btn-group'></div></div><div class='l-clear'></div>");
            gridhtmlarr.push("<div class='l-grid'>");

            gridhtmlarr.push("    <div class='datagrid-box-l'>");
            gridhtmlarr.push("        <div class='datagrid-box-h'>");
            gridhtmlarr.push("        </div>");
            gridhtmlarr.push("        <div class='datagrid-box-b datagrid-box-b1'>");
            gridhtmlarr.push("        </div>");
            gridhtmlarr.push("    </div>");

            gridhtmlarr.push("    <div class='l-grid2'>");
            gridhtmlarr.push("        <div class='datagrid-box-h'>");
            gridhtmlarr.push("        </div>");
            gridhtmlarr.push("        <div class='datagrid-box-b datagrid-box-b2 l-scroll'>");
            gridhtmlarr.push("        </div>");
            gridhtmlarr.push("    </div>");

            gridhtmlarr.push("    <div class='l-grid-popup'><table><tbody></tbody></table></div>");

            gridhtmlarr.push("</div>");
            gridhtmlarr.push("<div class='datagrid-paging-box'></div>");
            var ss = $("<div class='datagrid-wrap-h'><table class='table table-bordered'><colgroup></colgroup><thead></thead></table></div>");

            g.grid.html(gridhtmlarr.join(''));
            //头部
            g.header = $(".datagrid-title:first", g.grid);
            //底部工具条
            g.footbar = $(".datagrid-paging-box:first", g.grid);
            //显示/隐藏列
            g.popup = $(".l-grid-popup:first", g.grid);
            //顶部工具栏
            g.topbar = $(".btn-group:first", g.grid);

            g.gridview = $(".l-grid:first", g.grid);
            g.gridview.attr("id", g.id + "grid");

            g.gridview1 = $(".datagrid-box-l:first", g.gridview);
            $('.datagrid-box-h', g.gridview1).append(ss)
            g.gridview2 = $(".l-grid2:first", g.gridview);
            $('.datagrid-box-h', g.gridview2).append(ss)
            //表头
            g.gridheader = $(".datagrid-box-h:first", g.gridview2);
            g.gridheader.colgroup = $('colgroup', g.gridheader);
            //表主体
            g.gridbody = $(".datagrid-box-b:first", g.gridview2);
            g.gridbody.colgroup = $('colgroup', g.gridbody);

            //frozen
            g.f = {};
            //表头
            g.f.gridheader = $(".datagrid-box-h:first", g.gridview1);
            g.f.gridheader.colgroup = $('colgroup', g.gridview1);
            //表主体
            g.f.gridbody = $(".datagrid-box-b:first", g.gridview1);
            g.f.gridbody.colgroup = $('colgroup', g.f.gridbody);

            g.currentData = null;
            g.changedCells = {};

            g.cacheData = {}; //缓存数据

            if (p.height == "auto")
            {
                g.bind("SysGridHeightChanged", function ()
                {
                    if (g.enabledFrozen())
                        g.gridview.height(Math.max(g.gridview1.height(), g.gridview2.height()));
                });
            }

            var pc = $.extend({}, p);

            this._bulid();
            this._setColumns(p.columns);

            delete pc['columns'];
            delete pc['data'];
            delete pc['url'];
            g.set(pc);
            if (!p.delayLoad)
            {
                if (p.dataUrl)
                    g.set({ url: p.dataUrl });
                else if (p.data)
                    g.set({ data: p.data });
            }
        },
        _setFrozen: function (frozen)
        {
            this.gridview1.css({display:frozen?'block':'none'});
        },
        _setToolbar: function (value)
        {
            var g = this, p = this.options;
            if (value)
            {
                g.topbar.parent().show();
                $.each(value, function(){
                    if (this.text)
                    {
                        var jtitle = $("<button type='button' class='btn'>" + this.text + "</button>");
                        if (this.icon)
                        {
                            jtitle.prepend("<i class='icon-" + this.icon + "'></i> ");
                        }
                        if(this.class)
                        {
                            jtitle.addClass(this.class);
                        }
                        g.topbar.append(jtitle);
                    }
                });
            } else
            {
                if(!p.toolbarCustom)
                    g.topbar.parent().remove();
            }
        },
        _setToolbarCustom: function (custom) {
            var g = this;
            var that = $('#'+g.id);
            var $custom, $custombox = $('<div style="display:table-cell;"></div>')
                g.topbar.parent().show();

            if (typeof custom === 'string') {
                var custom = $(custom)

                if (custom && custom.length) {
                    $custom = custom
                } else {
                    custom = custom.toFunc()
                    if (custom) {
                        $custom = custom.call(that)
                        if (typeof $custom === 'string') $custom = $($custom)
                    }
                }
            } else if (typeof custom === 'function') {
                $custom = custom.call(that)
                if (typeof $custom === 'string') $custom = $($custom)
            } else {
                $custom = custom
            }
            if ($custom && $custom.length && typeof $custom !== 'string') {
                $custom.appendTo($custombox)
                g.topbar.parent().height(38).append($custombox).initui()
            }
            
        },
        _setHeight: function (h)
        {
            var g = this, p = this.options;
            g.unbind("SysGridHeightChanged");
            if (h == "auto")
            {
                g.bind("SysGridHeightChanged", function ()
                {
                    if (g.enabledFrozen())
                        g.gridview.height(Math.max(g.gridview1.height(), g.gridview2.height()));
                });
                return;
            }
            h = g._calculateGridBodyHeight(h);
            if (h > 0)
            {
                g.gridbody.height(h);

                if (p.frozen)
                {
                    //解决冻结列和活动列由上至下滚动错位的问题
                    var w = g.gridbody.width(), w2 = $(":first-child", g.gridbody).width();
                    //hoping: 滚动条的宽度减小一个像素
                    var $w = 17;
                    if(typeof($.fn.perfectScrollbar)=='function') $w = 0;

                    if (w2 && (w2 + $w > w))
                    {
                        if (h > $w)
                            g.f.gridbody.height(h - $w);
                    } else
                    {
                        g.f.gridbody.height(h);
                    }
                }
                //hoping: 修正多行表头
                var gridHeaderHeight = p.origH * (g._columnMaxLevel - 1) + p.headerRowHeight - 1;
                g.gridview.height(h + gridHeaderHeight);
            }
            g._updateHorizontalScrollStatus.ligerDefer(g, 10);
        },
        _calculateGridBodyHeight: function (h)
        {
            var g = this, p = this.options;
            if (typeof h == "string" && h.indexOf('%') > 0)
            {
                if (p.inWindow)
                    h = $(window).height() * parseInt(h) * 0.01;
                else
                    h = g.grid.parent().height() * parseInt(h) * 0.01;
            }
            if (p.gridTitle) h -= g.header.outerHeight();
            if (p.usePager) h -= g.footbar.outerHeight();
            if (p.toolbar || p.toolbarCustom) h -= g.topbar.parent().outerHeight();
            //居然还需要手工减去TOP点。
            h -= g.element.offsetTop;
            //hoping: 修正多行表头
            var gridHeaderHeight = p.origH * (g._columnMaxLevel - 1) + p.headerRowHeight + 1;
            h -= (gridHeaderHeight || 0);
            return h;
        },
        _updateHorizontalScrollStatus: function ()
        {
            var g = this;
            //hoping: 修改滚动条样式
            Do.ready('scrollbar', function () {
                g.grid.parent().css({overflow:"hidden"}).perfectScrollbar('destroy')
                g.gridbody.find('table').width(g.gridtablewidth);
                g.gridbody.perfectScrollbar();
            });
        },
        _updateFrozenWidth: function ()
        {
            var g = this, p = this.options;
            if (g.enabledFrozen())
            {
                g.gridview1.width(g.f.gridtablewidth);
                var view2width = g.gridview.width() - g.f.gridtablewidth;
                g.gridview2.css({ "margin-left": g.f.gridtablewidth });
                if (view2width > 0) g.gridview2.css({ width: view2width });
            }
        },
        _setWidth: function (value)
        {
            var g = this, p = this.options;
            if (g.enabledFrozen()) {
                g._onResize();
            }
        },
        _setDataUrl: function (value)
        {
            this.options.dataUrl = value;
            if (value)
            {
                this.options.dataType = "server";
                this.loadData(true);
            }
            else
            {
                this.options.dataType = "local";
            }
        },
        _setData: function (value)
        {
            this.loadData(this.options.data);
            this.trigger('afterSetData');
        },
        //刷新数据
        loadData: function (loadDataParm,sourceType)
        {
            var g = this, p = this.options;
            g.loading = true;
            g.trigger('loadData');
            var clause = null;
            var loadServer = true;
            if (typeof (loadDataParm) == "function")
            {
                clause = loadDataParm;
                if (g.lastData)
                {
                    g.data = g.lastData;
                } else
                {
                    g.data = g.currentData;
                    if (!g.data) g.data = {};
                    if (!g.data[$.zui.ajaxReturn.root]) g.data[$.zui.ajaxReturn.root] = [];
                    g.lastData = g.data;
                }
                loadServer = false;
            }
            else if (typeof (loadDataParm) == "boolean")
            {
                loadServer = loadDataParm;
            }
            else if (typeof (loadDataParm) == "object" && loadDataParm)
            {
                loadServer = false;
                p.dataType = "local";
                p.data = loadDataParm;
            }
            else if (typeof (loadDataParm) == "number")
            {
                p.newPage = loadDataParm;
            }
            //参数初始化
            if (!p.newPage) p.newPage = 1;
            if (p.dataAction == "server")
            {
                if (!p.sortOrder) p.sortOrder = "asc";
            }
            var param = [];
            if (p.postData)
            {
                var parms = $.isFunction(p.postData) ? p.postData() : p.postData;
                if (parms.length)
                {
                    $(parms).each(function ()
                    {
                        param.push({ name: this.name, value: this.value });
                    });
                    for (var i = parms.length - 1; i >= 0; i--)
                    {
                        if (parms[i].temp)
                            parms.splice(i, 1);
                    }
                }
                else if (typeof parms == "object")
                {
                    for (var name in parms)
                    {
                        param.push({ name: name, value: parms[name] });
                    }
                }
            }
            if (p.dataAction == "server")
            {
                if (p.usePager)
                {
                    param.push({ name: p.pageParmName, value: p.newPage });
                    param.push({ name: p.pagesizeParmName, value: p.pageSize });
                }
                if (p.sortName)
                {
                    param.push({ name: p.sortnameParmName, value: p.sortName });
                    param.push({ name: p.sortorderParmName, value: p.sortOrder });
                }
            };
            $(".l-bar-btnload span", g.footbar).addClass("disabled");
            if (p.dataType == "local")
            {
                //原语句: g.filteredData = p.data || g.currentData;
                //该语句修改了p.data, 导致过滤数据后, 丢失了初始数据.
                g.filteredData = $.extend(true, {}, p.data || g.currentData);
                if (clause)
                    g.filteredData[$.zui.ajaxReturn.root] = g._searchData(g.filteredData[$.zui.ajaxReturn.root], clause);
                if (p.usePager)
                    g.currentData = g._getCurrentPageData(g.filteredData);
                else
                {
                    g.currentData = g.filteredData;
                }
                g._convertTreeData();
                g._showData();
            }
            else if (p.dataAction == "local" && !loadServer)
            {
                if (g.data && g.data[$.zui.ajaxReturn.root])
                {
                    g.filteredData = g.data;
                    if (clause)
                        g.filteredData[$.zui.ajaxReturn.root] = g._searchData(g.filteredData[$.zui.ajaxReturn.root], clause);
                    g.currentData = g._getCurrentPageData(g.filteredData);
                    g._convertTreeData();
                    g._showData();
                }
            }
            else
            {
                g.loadServerData(param, clause, sourceType);
                //g.loadServerData.ligerDefer(g, 10, [param, clause]);
            }
            g.loading = false;
        },
        _convertTreeData: function ()
        {
            var g = this, p = this.options;
            if (p.tree && p.tree.idField && p.tree.parentIDField)
            {
                g.currentData[$.zui.ajaxReturn.root] = g.arrayToTree(g.currentData[$.zui.ajaxReturn.root], p.tree.idField, p.tree.parentIDField);
                g.currentData[$.zui.ajaxReturn.total] = g.currentData[$.zui.ajaxReturn.root].length;
            }
        },
        loadServerData: function (param, clause, sourceType)
        {
            var g = this, p = this.options;
            var url = p.dataUrl;
            if ($.isFunction(url)) url = url.call(g);
            var urlParms = $.isFunction(p.urlParms) ? p.urlParms.call(g) : p.urlParms;
            if (urlParms)
            {
                for (name in urlParms)
                {
                    url += url.indexOf('?') == -1 ? "?" : "&";
                    url += name + "=" + urlParms[name];
                }
            }
            var ajaxOptions = {
                type: p.method,
                url: url,
                data: param,
                async: p.async,
                dataType: 'json',
                beforeSend: function ()
                {
                    if (g.hasBind('loading'))
                    {
                        g.trigger('loading');
                    }
                    //else
                    //{
                        g.toggleLoading(true);
                    //}
                },
                success: function (data)
                {
                    g.trigger('success', [data, g]);
                    if (!data || !data[$.zui.ajaxReturn.root] || !data[$.zui.ajaxReturn.root].length)
                    {
                        g.currentData = g.data = {};
                        g.currentData[$.zui.ajaxReturn.root] = g.data[$.zui.ajaxReturn.root] = [];
                        if (data && data[$.zui.ajaxReturn.total])
                        {
                            g.currentData[$.zui.ajaxReturn.total] = g.data[$.zui.ajaxReturn.total] = data[$.zui.ajaxReturn.total];
                        } else
                        {
                            g.currentData[$.zui.ajaxReturn.total] = g.data[$.zui.ajaxReturn.total] = 0;
                        }
                        g._convertTreeData();
                        g._showData(sourceType);
                        return;
                    }
                    g.data = data;
                    //保存缓存数据-记录总数
                    if (g.data[$.zui.ajaxReturn.total] != null)
                    {
                        g.cacheData.records = g.data[$.zui.ajaxReturn.total];
                    }
                    if (p.dataAction == "server") //服务器处理好分页排序数据
                    {
                        g.currentData = g.data;
                        //读取缓存数据-记录总数(当没有返回总记录数)
                        if (g.currentData[$.zui.ajaxReturn.total] == null && g.cacheData.records)
                        {
                            g.currentData[$.zui.ajaxReturn.total] = g.cacheData.records;
                        }
                        if(!p.urlParms) {
                            p.urlParms = [];
                        }
                        p.urlParms[$.zui.ajaxReturn.total] = g.data[$.zui.ajaxReturn.total];
                        if(g.data.hasOwnProperty('Sum')) {
                            p.urlParms['Sum'] = JSON.stringify(g.data['Sum']);
                        }
                    }
                    else  //在客户端处理分页排序数据
                    {
                        g.filteredData = g.data;
                        if (clause) g.filteredData[$.zui.ajaxReturn.root] = g._searchData(g.filteredData[$.zui.ajaxReturn.root], clause);
                        if (p.usePager)
                            g.currentData = g._getCurrentPageData(g.filteredData);
                        else
                            g.currentData = g.filteredData;
                    }
                    g._convertTreeData();
                    g._showData.ligerDefer(g, 10, [sourceType]);
                },
                complete: function ()
                {
                    g.trigger('complete', [g]);
                    if (g.hasBind('loaded'))
                    {
                        g.trigger('loaded', [g]);
                    }
                    else
                    {
                        g.toggleLoading.ligerDefer(g, 10, [false]);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    g.currentData = g.data = {};
                    g.currentData[$.zui.ajaxReturn.root] = g.data[$.zui.ajaxReturn.root] = [];
                    g.currentData[$.zui.ajaxReturn.total] = g.data[$.zui.ajaxReturn.total] = 0;
                    g.toggleLoading.ligerDefer(g, 10, [false]);
                    $(".l-bar-btnload span", g.footbar).removeClass("disabled");
                    g.trigger('error', [XMLHttpRequest, textStatus, errorThrown]);
                    $.zui.debug([XMLHttpRequest, textStatus, errorThrown]);
                }
            };
            if (p.contentType) ajaxOptions.contentType = p.contentType;
            if (p.contentType == "application/json" && typeof (parms) != "string")
            {
                ajaxOptions.data = converParmJson(param)
            }
            $.ajax(ajaxOptions);

            function converParmJson(parm)
            {
                if (!parm) return "null";
                var o = parm, result = {};
                if ($.isArray(o))
                {
                    for (var i = 0; i < o.length; i++)
                    {
                        result[o[i].name] = o[i].value;
                    }
                } else
                {
                    result = o;
                }
                return liger.toJSON(result);
            }
        },
        toggleLoading: function (show)
        {
            if(show){
                this.trigger('zui.ajaxStart');
            } else {
                this.trigger('zui.ajaxStop');
            }
        },
        setWidth: function (w)
        {
            return this._setWidth(w);
        },
        setHeight: function (h)
        {
            return this._setHeight(h);
        },
        //是否启用复选框列
        enabledCheckbox: function ()
        {
            return this.options.checkbox ? true : false;
        },
        //是否固定列
        enabledFrozen: function ()
        {
            var g = this, p = this.options;
            if (!p.frozen) return false;
            var cols = g.columns || [];
            if (g.enabledDetail() && p.frozenDetail || g.enabledCheckbox() && p.frozenCheckbox
            || p.frozenRownumbers && p.rownumbers) return true;
            for (var i = 0, l = cols.length; i < l; i++)
            {
                if (cols[i].frozen)
                {
                    return true;
                }
            }
            this._setFrozen(false);
            return false;
        },
        //是否启用明细列
        enabledDetail: function ()
        {
            if (this.options.detail && this.options.detail.onShowDetail) return true;
            return false;
        },
        //是否启用分组
        enabledGroup: function ()
        {
            return this.options.groupColumnName ? true : false;
        },
        _createRowid: function ()
        {
            return "r" + (1000 + this.recordNumber);
        },
        _isRowId: function (str)
        {
            return (str in this.records);
        },
        _addNewRecord: function (o, previd, pid)
        {
            var g = this, p = this.options;
            g.recordNumber++;
            o['__id'] = g._createRowid();
            o['__previd'] = previd;
            if (previd && previd != -1)
            {
                var prev = g.records[previd];
                if (prev['__nextid'] && prev['__nextid'] != -1)
                {
                    var prevOldNext = g.records[prev['__nextid']];
                    if (prevOldNext)
                        prevOldNext['__previd'] = o['__id'];
                }
                prev['__nextid'] = o['__id'];
                o['__index'] = prev['__index'] + 1;
            }
            else
            {
                o['__index'] = 0;
            }
            if (p.tree)
            {
                if (pid && pid != -1)
                {
                    var parent = g.records[pid];
                    o['__pid'] = pid;
                    o['__level'] = parent['__level'] + 1;
                }
                else
                {
                    o['__pid'] = -1;
                    o['__level'] = 1;
                }
                o['__hasChildren'] = o[p.tree.childrenName] ? true : false;
            }
            g.rows[o['__index']] = o;
            g.records[o['__id']] = o;
            return o;
        },
        _updateGridData: function ()
        {
            var g = this, p = this.options;
            g.recordNumber = 0;
            g.rows = [];
            g.records = {};
            var previd = -1;
            function load(data, pid)
            {
                if (!data || !data.length) return;
                for (var i = 0, l = data.length; i < l; i++)
                {
                    var o = data[i];
                    g.formatRecord(o);
                    g._addNewRecord(o, previd, pid);
                    previd = o['__id'];
                    if (o['__hasChildren'])
                    {
                        load(o[p.tree.childrenName], o['__id']);
                    }
                }
            }
            load(g.currentData[$.zui.ajaxReturn.root], -1);
            return g.rows;
        },
        reRender: function (e)
        {
            var g = this, p = this.options;
            e = e || {};
            var rowdata = e.rowdata,
                column = e.column,
                //只重渲染统计行
                totalOnly = e.totalOnly;
            if (column && (column.isdetail || column.ischeckbox)) return;
            if (totalOnly)
            {
                $(g.columns).each(function ()
                {
                    reRenderTotal(this);
                });
            }
            else if (rowdata && column)
            {
                var cell = g.getCellObj(rowdata, column);
                $(cell).html(g._getCellHtml(rowdata, column));
            }
            else if (rowdata)
            {
                $(g.columns).each(function ()
                {
                    g.reRender({ rowdata: rowdata, column: this });
                });
            }
            else if (column)
            {
                for (var rowid in g.records)
                {
                    g.reRender({ rowdata: g.records[rowid], column: column });
                }
                reRenderTotal(column);
            }
            else
            {
                g._showData();
            }

            function reRenderTotal(column)
            {
                if (!column.totalSummary) return;
                for (var i = 0; i < g.totalNumber; i++)
                {
                    var tobj = document.getElementById(g.id + "|total" + i + "|" + column['__id']);
                    $("div:first", tobj).html(g._getTotalCellContent(column, g.groups && g.groups[i] ? g.groups[i] : g.currentData[$.zui.ajaxReturn.root]));
                }
            }
        },
        //格式化数据
        formatRecord: function (o)
        {
            delete o['__id'];
            delete o['__previd'];
            delete o['__nextid'];
            delete o['__index'];
            if (this.options.tree)
            {
                delete o['__pid'];
                delete o['__level'];
                delete o['__hasChildren'];
            }
            return o;
        },
        getColumn: function (columnParm)
        {
            var g = this, p = this.options;
            if (typeof columnParm == "string") // column id
            {
                if (g._isColumnId(columnParm))
                    return g._columns[columnParm];
                else
                    return g.columns[parseInt(columnParm)];
            }
            else if (typeof (columnParm) == "number") //column index
            {
                return g.columns[columnParm];
            }
            else if (typeof columnParm == "object" && columnParm.nodeType == 1) //column header cell
            {
                var ids = columnParm.id.split('|');
                var columnid = ids[ids.length - 1];
                return g._columns[columnid];
            }
            return columnParm;
        },
        getColumnByName: function (columnname)
        {
            var g = this, p = this.options;
            for (i = 0; i < g.columns.length; i++)
            {
                if (g.columns[i].name == columnname)
                {
                    return g.columns[i];
                }
            }
            return null;
        },
        getColumnType: function (columnname)
        {
            var g = this, p = this.options;
            for (i = 0; i < g.columns.length; i++)
            {
                if (g.columns[i].name == columnname)
                {
                    if (g.columns[i].type) return g.columns[i].type;
                    return "string";
                }
            }
            return null;
        },
        //是否包含汇总
        isTotalSummary: function ()
        {
            var g = this, p = this.options;
            for (var i = 0; i < g.columns.length; i++)
            {
                if (g.columns[i].totalSummary) return true;
            }
            return false;
        },
        //根据层次获取列集合
        //如果columnLevel为空，获取叶节点集合
        getColumns: function (columnLevel)
        {
            var g = this, p = this.options;
            var columns = [];
            for (var id in g._columns)
            {
                var col = g._columns[id];
                if (columnLevel != undefined)
                {
                    if (col['__level'] == columnLevel) columns.push(col);
                }
                else
                {
                    if (col['__leaf']) columns.push(col);
                }
            }
            return columns;
        },
        //改变排序
        changeSort: function (columnName, sortOrder)
        {
            var g = this, p = this.options;
            if (g.loading) return true;
            if (p.dataAction == "local")
            {
                var columnType = g.getColumnType(columnName);
                if (!g.sortedData)
                    g.sortedData = g.filteredData;
                if (!g.sortedData || !g.sortedData[$.zui.ajaxReturn.root])
                    return;
                if (p.sortName == columnName)
                {
                    g.sortedData[$.zui.ajaxReturn.root].reverse();
                } else
                {
                    g.sortedData[$.zui.ajaxReturn.root].sort(function (data1, data2)
                    {
                        return g._compareData(data1, data2, columnName, columnType);
                    });
                }
                if (p.usePager)
                    g.currentData = g._getCurrentPageData(g.sortedData);
                else
                    g.currentData = g.sortedData;
                g._showData();
            }
            p.sortName = columnName;
            p.sortOrder = sortOrder;
            if (p.dataAction == "server")
            {
                g.loadData(p.where);
            }
        },
        //改变分页
        changePage: function (ctype)
        {
            var g = this, p = this.options;
            if (g.loading) return true;
            var pageCount = $(".page-total span", g.footbar).html();
            pageCount = pageCount.split('/');
            p.pageCount = pageCount.pop();
            if(parseInt(ctype)==ctype){
                p.newPage = parseInt(ctype);
            } else {
                switch (ctype)
                {
                    case 'first': if (p.page == 1) return; p.newPage = 1; break;
                    case 'prev': if (p.page == 1) return; if (p.page > 1) p.newPage = parseInt(p.page) - 1; break;
                    case 'next': if (p.page >= p.pageCount) return; p.newPage = parseInt(p.page) + 1; break;
                    case 'last': if (p.page >= p.pageCount) return; p.newPage = p.pageCount; break;
                    case 'input':
                        var nv = parseInt($('.pcontrol input', g.footbar).val());
                        if (isNaN(nv)) nv = 1;
                        if (nv < 1) nv = 1;
                        else if (nv > p.pageCount) nv = p.pageCount;
                        $('.pcontrol input', g.footbar).val(nv);
                        p.newPage = nv;
                        break;
                }
            }
            if (p.newPage == p.page) return false;
            if (p.newPage == 1) {
                $(".page-first", g.footbar).addClass("disabled");
                $(".page-prev", g.footbar).addClass("disabled");
            }
            else {
                $(".page-first", g.footbar).removeClass("disabled");
                $(".page-prev", g.footbar).removeClass("disabled");
            }
            if (p.newPage == p.pageCount) {
                $(".page-last", g.footbar).addClass("disabled");
                $(".page-next", g.footbar).addClass("disabled");
            }
            else {
                $(".page-last", g.footbar).removeClass("disabled");
                $(".page-next", g.footbar).removeClass("disabled");
            }
            g.trigger('changePage', [p.newPage]);
            if (p.dataAction == "server")
            {
                if (!p.postData)
                {
                    p.postData = [];
                }
                if ($.isArray(p.postData))
                {
                    p.postData.push({ name: "changepage", value: "1", temp: true });
                } else
                {
                    p.postData["changepage"] = "1";
                }
                g.loadData(p.where);
            }
            else
            {
                g.currentData = g._getCurrentPageData(g.filteredData);
                //增加以下一句调用: 在显示数据之前, 应该先调用转换tree的函数.
                //否则会导致树的数据重复显示
                if (p.tree)
                {
                    var childrenName = p.tree.childrenName;
                    $(g.filteredData[$.zui.ajaxReturn.root]).each(function (index, item)
                    {
                        if (item[childrenName])
                            item[childrenName] = [];
                    });
                    g._convertTreeData();
                }
                g._showData();
            }
        },
        getSelectedRow: function ()
        {
            for (var i in this.selected)
            {
                var o = this.selected[i];
                if (o['__id'] in this.records)
                    return o;
            }
            return null;
        },
        getSelectedRows: function ()
        {
            var arr = [];
            for (var i in this.selected)
            {
                var o = this.selected[i];
                if (o['__id'] in this.records)
                    arr.push(o);
            }
            return arr;
        },
        getSelectedRowObj: function ()
        {
            for (var i in this.selected)
            {
                var o = this.selected[i];
                if (o['__id'] in this.records)
                    return this.getRowObj(o);
            }
            return null;
        },
        getSelectedRowObjs: function ()
        {
            var arr = [];
            for (var i in this.selected)
            {
                var o = this.selected[i];
                if (o['__id'] in this.records)
                    arr.push(this.getRowObj(o));
            }
            return arr;
        },
        getCellObj: function (rowParm, column)
        {
            var rowdata = this.getRow(rowParm);
            column = this.getColumn(column);
            return document.getElementById(this._getCellDomId(rowdata, column));
        },
        getRowObj: function (rowParm, frozen)
        {
            var g = this, p = this.options;
            if (rowParm == null) return null;
            if (typeof (rowParm) == "string")
            {
                if (g._isRowId(rowParm))
                    return document.getElementById(g.id + (frozen ? "|1|" : "|2|") + rowParm);
                else
                    return document.getElementById(g.id + (frozen ? "|1|" : "|2|") + g.rows[parseInt(rowParm)]['__id']);
            }
            else if (typeof (rowParm) == "number")
            {
                return document.getElementById(g.id + (frozen ? "|1|" : "|2|") + g.rows[rowParm]['__id']);
            }
            else if (typeof (rowParm) == "object" && rowParm['__id']) //rowdata
            {
                return g.getRowObj(rowParm['__id'], frozen);
            }
            return rowParm;
        },
        getRow: function (rowParm)
        {
            var g = this, p = this.options;
            if (rowParm == null) return null;
            if (typeof (rowParm) == "string")
            {
                if (g._isRowId(rowParm))
                    return g.records[rowParm];
                else
                    return g.rows[parseInt(rowParm)];
            }
            else if (typeof (rowParm) == "number")
            {
                return g.rows[parseInt(rowParm)];
            }
            else if (typeof (rowParm) == "object" && rowParm.nodeType == 1 && !rowParm['__id']) //dom对象
            {
                return g._getRowByDomId(rowParm.id);
            }
            return rowParm;
        },
        _setColumnVisible: function (column, hide)
        {
            var g = this, p = this.options;
            if (!hide)  //显示
            {
                column._hide = false;
                document.getElementById(column['__domid']).style.display = "";
                //判断分组列是否隐藏,如果隐藏了则显示出来
                if (column['__pid'] != -1)
                {
                    var pcol = g._columns[column['__pid']];
                    if (pcol._hide)
                    {
                        document.getElementById(pcol['__domid']).style.display = "";
                        this._setColumnVisible(pcol, hide);
                    }
                }
            }
            else //隐藏
            {
                column._hide = true;
                document.getElementById(column['__domid']).style.display = "none";
                //判断同分组的列是否都隐藏,如果是则隐藏分组列
                if (column['__pid'] != -1)
                {
                    var hideall = true;
                    var pcol = this._columns[column['__pid']];
                    for (var i = 0; pcol && i < pcol.columns.length; i++)
                    {
                        if (!pcol.columns[i]._hide)
                        {
                            hideall = false;
                            break;
                        }
                    }
                    if (hideall)
                    {
                        pcol._hide = true;
                        document.getElementById(pcol['__domid']).style.display = "none";
                        this._setColumnVisible(pcol, hide);
                    }
                }
            }
        },
        //显示隐藏列
        toggleCol: function (columnparm, visible, toggleByPopup)
        {
            var g = this, p = this.options;
            var column;
            if (typeof (columnparm) == "number")
            {
                column = g.columns[columnparm];
            }
            else if (typeof (columnparm) == "object" && columnparm['__id'])
            {
                column = columnparm;
            }
            else if (typeof (columnparm) == "string")
            {
                if (g._isColumnId(columnparm)) // column id
                {
                    column = g._columns[columnparm];
                }
                else  // column name
                {
                    $(g.columns).each(function ()
                    {
                        if (this.name == columnparm)
                            g.toggleCol(this, visible, toggleByPopup);
                    });
                    return;
                }
            }
            if (!column) return;
            var columnindex = column['__leafindex'];
            var headercell = document.getElementById(column['__domid']);
            if (!headercell) return;
            headercell = $(headercell);
            var cells = [];
            for (var i in g.rows)
            {
                var obj = g.getCellObj(g.rows[i], column);
                if (obj) cells.push(obj);
            }
            for (var i = 0; i < g.totalNumber; i++)
            {
                var tobj = document.getElementById(g.id + "|total" + i + "|" + column['__id']);
                if (tobj) cells.push(tobj);
            }
            var colwidth = column._width;
            //显示列
            if (visible && column._hide)
            {
                if (column.frozen)
                    g.f.gridtablewidth += (parseInt(colwidth) + 1);
                else
                    g.gridtablewidth += (parseInt(colwidth) + 1);
                g._setColumnVisible(column, false);
                $(cells).show();
            }
                //隐藏列
            else if (!visible && !column._hide)
            {
                if (column.frozen)
                    g.f.gridtablewidth -= (parseInt(colwidth) + 1);
                else
                    g.gridtablewidth -= (parseInt(colwidth) + 1);
                g._setColumnVisible(column, true);
                $(cells).hide();
            }
            if (column.frozen)
            {
                $("div:first", g.f.gridheader).width(g.f.gridtablewidth);
                $("div:first", g.f.gridbody).width(g.f.gridtablewidth);
            }
            else
            {
                $("div:first", g.gridheader).width(g.gridtablewidth);
                if (g.gridtablewidth)
                {
                    $("div:first", g.gridbody).width(g.gridtablewidth);
                } else
                {
                    $("div:first", g.gridbody).css("width", "auto");
                }
            }
            g._updateFrozenWidth();
            if (!toggleByPopup)
            {
                $(':checkbox[columnindex=' + columnindex + "]", g.popup).each(function ()
                {
                    this.checked = visible;
                });
            }
        },
        //设置列宽
        setColumnWidth: function (columnparm, newwidth)
        {
            var g = this, p = this.options;
            if (!newwidth) return;
            newwidth = parseInt(newwidth, 10);
            var column;
            if (typeof (columnparm) == "number")
            {
                column = g.columns[columnparm];
            }
            else if (typeof (columnparm) == "object" && columnparm['__id'])
            {
                column = columnparm;
            }
            else if (typeof (columnparm) == "string")
            {
                if (g._isColumnId(columnparm)) // column id
                {
                    column = g._columns[columnparm];
                }
                else  // column name
                {
                    $(g.columns).each(function ()
                    {
                        if (this.name == columnparm)
                            g.setColumnWidth(this, newwidth);
                    });
                    return;
                }
            }
            if (!column) return;
            var mincolumnwidth = p.minColumnWidth;
            if (column.minWidth) mincolumnwidth = column.minWidth;
            newwidth = newwidth < mincolumnwidth ? mincolumnwidth : newwidth;
            var diff = newwidth - column._width;
            if (g.trigger('beforeChangeColumnWidth', [column, newwidth]) == false) return;
            column._width = newwidth;
            g.gridheader.colgroup.find('> col:eq(' + column.columnindex + ')').width(newwidth)
            g.gridbody.colgroup.find('> col:eq(' + column.columnindex + ')').width(newwidth)
            g.f.gridheader.colgroup.find('> col:eq(' + column.columnindex + ')').width(newwidth)
            g.f.gridbody.colgroup.find('> col:eq(' + column.columnindex + ')').width(newwidth)
            if (column.frozen)
            {
                g.f.gridtablewidth += diff;
                $("div:first", g.f.gridheader).width(g.f.gridtablewidth);
                $("div:first", g.f.gridbody).width(g.f.gridtablewidth);
            }
            else
            {
                g.gridtablewidth += diff;
                $("div:first", g.gridheader).width(g.gridtablewidth);
                $("div:first", g.gridbody).width(g.gridtablewidth);
            }
            $(document.getElementById(column['__domid'])).css('width', newwidth);
            var cells = [];
            for (var rowid in g.records)
            {
                var obj = g.getCellObj(g.records[rowid], column);
                if (obj) cells.push(obj);
            }
            for (var i = 0; i < g.totalNumber; i++)
            {
                var tobj = document.getElementById(g.id + "|total" + i + "|" + column['__id']);
                if (tobj) cells.push(tobj);
            }
            $(cells).css('width', newwidth).find("> div.l-grid-row-cell-inner:first").css('width', newwidth);   //-8?

            g._updateFrozenWidth();
            g._updateHorizontalScrollStatus.ligerDefer(g, 10);

            g.trigger('afterChangeColumnWidth', [column, newwidth]);
        },
        //改变列表头内容
        changeHeaderText: function (columnparm, headerText)
        {
            var g = this, p = this.options;
            var column;
            if (typeof (columnparm) == "number")
            {
                column = g.columns[columnparm];
            }
            else if (typeof (columnparm) == "object" && columnparm['__id'])
            {
                column = columnparm;
            }
            else if (typeof (columnparm) == "string")
            {
                if (g._isColumnId(columnparm)) // column id
                {
                    column = g._columns[columnparm];
                }
                else  // column name
                {
                    $(g.columns).each(function ()
                    {
                        if (this.name == columnparm)
                            g.changeHeaderText(this, headerText);
                    });
                    return;
                }
            }
            if (!column) return;
            var columnindex = column['__leafindex'];
            var headercell = document.getElementById(column['__domid']);
            $(".datagrid-label", headercell).html(headerText);
            if (p.allowHideColumn)
            {
                $(':checkbox[columnindex=' + columnindex + "]", g.popup).parent().next().html(headerText);
            }
        },
        //改变列的位置
        changeCol: function (from, to, isAfter)
        {
            var g = this, p = this.options;
            if (!from || !to) return;
            var fromCol = g.getColumn(from);
            var toCol = g.getColumn(to);
            fromCol.frozen = toCol.frozen;
            var fromColIndex, toColIndex;
            var fromColumns = fromCol['__pid'] == -1 ? p.columns : g._columns[fromCol['__pid']].columns;
            var toColumns = toCol['__pid'] == -1 ? p.columns : g._columns[toCol['__pid']].columns;
            fromColIndex = $.inArray(fromCol, fromColumns);
            toColIndex = $.inArray(toCol, toColumns);
            var sameParent = fromColumns == toColumns;
            var sameLevel = fromCol['__level'] == toCol['__level'];
            toColumns.splice(toColIndex + (isAfter ? 1 : 0), 0, fromCol);
            if (!sameParent)
            {
                fromColumns.splice(fromColIndex, 1);
            }
            else
            {
                if (isAfter) fromColumns.splice(fromColIndex, 1);
                else fromColumns.splice(fromColIndex + 1, 1);
            }
            g._setColumns(p.columns);
            g.reRender();
        },


        collapseDetail: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            if (!rowdata) return;
            for (var i = 0, l = g.columns.length; i < l; i++)
            {
                if (g.columns[i].isdetail)
                {
                    var row = g.getRowObj(rowdata);
                    var cell = g.getCellObj(rowdata, g.columns[i]);
                    $(row).next("tr.l-grid-detailpanel").hide();
                    $(".l-grid-row-cell-detailbtn:first", cell).removeClass("l-open");
                    g.trigger('SysGridHeightChanged');
                    return;
                }
            }
        },
        extendDetail: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            if (!rowdata) return;
            for (var i = 0, l = g.columns.length; i < l; i++)
            {
                if (g.columns[i].isdetail)
                {
                    var row = g.getRowObj(rowdata);
                    var cell = g.getCellObj(rowdata, g.columns[i]);
                    $(row).next("tr.l-grid-detailpanel").show();
                    $(".l-grid-row-cell-detailbtn:first", cell).addClass("l-open");
                    g.trigger('SysGridHeightChanged');
                    return;
                }
            }
        },
        getParent: function (rowParm)
        {
            var g = this, p = this.options;
            if (!p.tree) return null;
            var rowdata = g.getRow(rowParm);
            if (!rowdata) return null;
            if (rowdata['__pid'] in g.records) return g.records[rowdata['__pid']];
            else return null;
        },
        getChildren: function (rowParm, deep)
        {
            var g = this, p = this.options;
            if (!p.tree) return null;
            var rowData = g.getRow(rowParm);
            if (!rowData) return null;
            var arr = [];
            function loadChildren(data)
            {
                if (data[p.tree.childrenName])
                {
                    for (var i = 0, l = data[p.tree.childrenName].length; i < l; i++)
                    {
                        var o = data[p.tree.childrenName][i];
                        arr.push(o);
                        if (deep)
                            loadChildren(o);
                    }
                }
            }
            loadChildren(rowData);
            return arr;
        },
        isLeaf: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            if (!rowdata) return;
            return rowdata['__hasChildren'] ? false : true;
        },
        hasChildren: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = this.getRow(rowParm);
            if (!rowdata) return;
            return (rowdata[p.tree.childrenName] && rowdata[p.tree.childrenName].length) ? true : false;
        },
        existRecord: function (record)
        {
            for (var rowid in this.records)
            {
                if (this.records[rowid] == record) return true;
            }
            return false;
        },
        _removeSelected: function (rowdata)
        {
            var g = this, p = this.options;
            if (p.tree)
            {
                var children = g.getChildren(rowdata, true);
                if (children)
                {
                    for (var i = 0, l = children.length; i < l; i++)
                    {
                        var index2 = $.inArray(children[i], g.selected);
                        if (index2 != -1) g.selected.splice(index2, 1);
                    }
                }
            }
            var index = $.inArray(rowdata, g.selected);
            if (index != -1) g.selected.splice(index, 1);
        },
        _getParentChildren: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            var listdata;
            if (p.tree && g.existRecord(rowdata) && rowdata['__pid'] in g.records)
            {
                listdata = g.records[rowdata['__pid']][p.tree.childrenName];
            }
            else
            {
                listdata = g.currentData[$.zui.ajaxReturn.root];
            }
            return listdata;
        },
        _addData: function (rowdata, parentdata, neardata, isBefore)
        {
            var g = this, p = this.options;
            if (!g.currentData) g.currentData = {};
            if (!g.currentData[$.zui.ajaxReturn.root]) g.currentData[$.zui.ajaxReturn.root] = [];
            var listdata = g.currentData[$.zui.ajaxReturn.root];
            if (neardata)
            {
                if (p.tree)
                {
                    if (parentdata)
                        listdata = parentdata[p.tree.childrenName];
                    else if (neardata['__pid'] in g.records)
                        listdata = g.records[neardata['__pid']][p.tree.childrenName];
                }
                var index = $.inArray(neardata, listdata);
                listdata.splice(index == -1 ? -1 : index + (isBefore ? 0 : 1), 0, rowdata);
            }
            else
            {
                if (p.tree && parentdata)
                {
                    listdata = parentdata[p.tree.childrenName];
                }
                listdata.push(rowdata);
            }

        },
        upgrade: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            if (!rowdata || !p.tree) return;
            rowdata[p.tree.childrenName] = rowdata[p.tree.childrenName] || [];
            rowdata['__hasChildren'] = true;
            var rowobjs = [g.getRowObj(rowdata)];
            if (g.enabledFrozen()) rowobjs.push(g.getRowObj(rowdata, true));
            $("> td > div > .l-grid-tree-space:last", rowobjs).addClass("l-grid-tree-link l-grid-tree-link-open");
        },
        demotion: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            if (!rowdata || !p.tree) return;
            var rowobjs = [g.getRowObj(rowdata)];
            if (g.enabledFrozen()) rowobjs.push(g.getRowObj(rowdata, true));
            $("> td > div > .l-grid-tree-space:last", rowobjs).removeClass("l-grid-tree-link l-grid-tree-link-open l-grid-tree-link-close");
            if (g.hasChildren(rowdata))
            {
                var children = g.getChildren(rowdata);
                for (var i = 0, l = children.length; i < l; i++)
                {
                    g.deleteRow(children[i]);
                }
            }
            rowdata['__hasChildren'] = false;
        },

        collapseAll: function ()
        {
            var g = this, p = this.options;
            $(g.rows).each(function (rowIndex, rowParm)
            {
                var targetRowObj = g.getRowObj(rowParm);
                var linkbtn = $(".l-grid-tree-link", targetRowObj);
                if (linkbtn.hasClass("l-grid-tree-link-close")) return;
                g.toggle(rowParm);
            });
        },
        expandAll: function ()
        {
            var g = this, p = this.options;
            $(g.rows).each(function (rowIndex, rowParm)
            {
                var targetRowObj = g.getRowObj(rowParm);
                var linkbtn = $(".l-grid-tree-link", targetRowObj);
                if (linkbtn.hasClass("l-grid-tree-link-open")) return;
                g.toggle(rowParm);
            });
        },

        collapse: function (rowParm)
        {
            var g = this, p = this.options;
            var targetRowObj = g.getRowObj(rowParm);
            var linkbtn = $(".l-grid-tree-link", targetRowObj);
            if (linkbtn.hasClass("l-grid-tree-link-close")) return;
            g.toggle(rowParm);
        },
        expand: function (rowParm)
        {
            var g = this, p = this.options;
            var targetRowObj = g.getRowObj(rowParm);
            var linkbtn = $(".l-grid-tree-link", targetRowObj);
            if (linkbtn.hasClass("l-grid-tree-link-open")) return;
            g.toggle(rowParm);
        },
        toggle: function (rowParm)
        {
            if (!rowParm) return;
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            var targetRowObj = [g.getRowObj(rowdata)];
            if (g.enabledFrozen()) targetRowObj.push(g.getRowObj(rowdata, true));
            var level = rowdata['__level'], indexInCollapsedRows;
            var linkbtn = $(".l-grid-tree-link:first", targetRowObj);
            var opening = true;
            g.collapsedRows = g.collapsedRows || [];
            var isToExpanding = linkbtn.hasClass("l-grid-tree-link-close");

            function toggleChildren(items)
            {
                for (var i = 0, l = items.length; i < l; i++)
                {
                    var o = items[i];
                    var subchildren = g.getChildren(o, false);
                    var haschildren = subchildren && subchildren.length ? true : false;
                    var currentRow = $([g.getRowObj(o['__id'])]);
                    if (g.enabledFrozen()) currentRow = currentRow.add(g.getRowObj(o['__id'], true));
                    if (haschildren)
                    {
                        //如果这个子节点原来是折叠状态的,那么子节点的子节点不做处理
                        if ($(".l-grid-tree-link", currentRow).hasClass("l-grid-tree-link-close"))
                        {
                            currentRow.show();
                        }
                        //如果是展开状态的
                        else
                        {
                            currentRow.show();
                            toggleChildren(subchildren);
                        }
                    }
                    else
                    {
                        $(".l-grid-tree-link", currentRow).removeClass("l-grid-tree-link-close").addClass("l-grid-tree-link-open");
                        currentRow.show();
                    }

                }
            }
            function update()
            {
                var children = [];
                //折叠,那么直接隐藏所有子节点即可
                if (!opening)
                {
                    children = g.getChildren(rowdata, true);
                    $(children).each(function ()
                    {
                        $(g.getRowObj(this)).hide();
                        if (g.enabledFrozen()) $(g.getRowObj(this, true)).hide();
                    });
                    g.trigger('treeCollapsed', [rowdata]);
                    return;
                }

                //展开,下面逻辑处理(选择性递归)
                children = g.getChildren(rowdata, false);

                toggleChildren(children);


                g.trigger('treeExpanded', [rowdata]);
            }


            if (isToExpanding) //收缩
            {
                function linkbtn_expand()
                {
                    linkbtn.removeClass("l-grid-tree-link-close").addClass("l-grid-tree-link-open");
                    indexInCollapsedRows = $.inArray(rowdata, g.collapsedRows);
                    if (indexInCollapsedRows != -1) g.collapsedRows.splice(indexInCollapsedRows, 1);
                }
                var e = {
                    update: function ()
                    {
                        linkbtn_expand();
                        update();
                    }
                };
                if (g.hasBind('treeExpand') && g.trigger('treeExpand', [rowdata, e]) == false) return false;
                linkbtn_expand();
            }
            else //折叠
            {
                function linkbtn_collapse()
                {
                    linkbtn.addClass("l-grid-tree-link-close").removeClass("l-grid-tree-link-open");
                    indexInCollapsedRows = $.inArray(rowdata, g.collapsedRows);
                    if (indexInCollapsedRows == -1) g.collapsedRows.push(rowdata);
                }
                var e = {
                    update: function ()
                    {
                        linkbtn_collapse();
                        update();
                    }
                };
                if (g.hasBind('treeCollapse') && g.trigger('treeCollapse', [rowdata, e]) == false) return false;
                opening = false;
                linkbtn_collapse();
            }

            update();
        },
        _bulid: function ()
        {
            var g = this;
            g._clearGrid();
            //创建头部
            g._initBuildHeader();
            //宽度高度初始化
            g._initHeight();
            //创建底部工具条
            g._initFootbar();
            //创建分页
            g._buildPager();
            //创建事件
            g._setEvent();
        },
        _setColumns: function (columns)
        {
            var g = this;
            //初始化列
            g._initColumns();
            //创建表头
            g._initBuildGridHeader();
            //创建 显示/隐藏 列 列表
            g._initBuildPopup();
        },
        _initBuildHeader: function ()
        {
            var g = this, p = this.options;
            if (p.gridTitle)
            {
                $(g.header).html(p.gridTitle);
            }
            else
            {
                g.header.hide();
            }
            if (p.toolbar || p.toolbarCustom)
            {
                //原语句不知道为什么, toolbar的父div高度为0. 导致样式有问题.
                //if (g.topbar.height() == 0)
                //    g.topbar.parent().height(25);
                //else
                    g.topbar.parent().height(g.topbar.height());
            }
            else
            {
                g.topbar.parent().remove();
            }
        },
        _createColumnId: function (column)
        {
            if (column.id != null && column.id != "") return column.id.toString();
            return "c" + (100 + this._columnCount);
        },
        _isColumnId: function (str)
        {
            return (str in this._columns);
        },
        _initColumns: function ()
        {
            var g = this, p = this.options;
            g._columns = {};             //全部列的信息
            g._columnCount = 0;
            g._columnLeafCount = 0;
            g._columnMaxLevel = 1;
            if (!p.columns) return;
            function removeProp(column, props)
            {
                for (var i in props)
                {
                    if (props[i] in column)
                        delete column[props[i]];
                }
            }
            //设置id、pid、level、leaf，返回叶节点数,如果是叶节点，返回1
            function setColumn(column, level, pid, previd)
            {
                removeProp(column, ['__id', '__pid', '__previd', '__nextid', '__domid', '__leaf', '__leafindex', '__level', '__colSpan', '__rowSpan']);
                if (level > g._columnMaxLevel) g._columnMaxLevel = level;
                g._columnCount++;
                column['__id'] = g._createColumnId(column);
                column['__domid'] = g.id + "|hcell|" + column['__id'];
                g._columns[column['__id']] = column;
                if (!column.columns || !column.columns.length)
                    column['__leafindex'] = g._columnLeafCount++;
                column['__level'] = level;
                column['__pid'] = pid;
                column['__previd'] = previd;
                if (!column.columns || !column.columns.length)
                {
                    column['__leaf'] = true;
                    return 1;
                }
                var leafcount = 0;
                var newid = -1;
                for (var i = 0, l = column.columns.length; i < l; i++)
                {
                    var col = column.columns[i];
                    leafcount += setColumn(col, level + 1, column['__id'], newid);
                    newid = col['__id'];
                }
                column['__leafcount'] = leafcount;
                return leafcount;
            }
            var lastid = -1;
            //行序号
            if (p.rownumbers)
            {
                var frozenRownumbers = g.enabledGroup() ? false : p.frozen && p.frozenRownumbers;
                var col = { isrownumber: true, issystem: true, width: p.rownumbersColWidth, align: 'right', frozen: frozenRownumbers };
                setColumn(col, 1, -1, lastid);
                lastid = col['__id'];
            }
            //明细列
            if (g.enabledDetail())
            {
                var frozenDetail = g.enabledGroup() ? false : p.frozen && p.frozenDetail;
                var col = { isdetail: true, issystem: true, width: p.detailColWidth, frozen: frozenDetail };
                setColumn(col, 1, -1, lastid);
                lastid = col['__id'];
            }
            //复选框列
            if (g.enabledCheckbox())
            {
                var frozenCheckbox = g.enabledGroup() ? false : p.frozen && p.frozenCheckbox;
                var col = { ischeckbox: true, issystem: true, width: p.detailColWidth, frozen: frozenCheckbox };
                setColumn(col, 1, -1, lastid);
                lastid = col['__id'];
            }
            for (var i = 0, l = p.columns.length; i < l; i++)
            {
                var col = p.columns[i];
                //增加以下一句. 因为在低版本IE中, 可能因为修改了prototype,
                //导致这里取出undefined, 并进一步导致后续的函数调用出错.
                if (!col) continue;
                setColumn(col, 1, -1, lastid);
                lastid = col['__id'];
            }
            //设置colSpan和rowSpan
            for (var id in g._columns)
            {
                var col = g._columns[id];
                if (col['__leafcount'] > 1)
                {
                    col['__colSpan'] = col['__leafcount'];
                }
                if (col['__leaf'] && col['__level'] != g._columnMaxLevel)
                {
                    col['__rowSpan'] = g._columnMaxLevel - col['__level'] + 1;
                }
            }
            //叶级别列的信息
            g.columns = g.getColumns();
            $(g.columns).each(function (i, column)
            {
                column.columnname = column.name;
                column.columnindex = i;
                column.type = column.type || "string";
                column.islast = i == g.columns.length - 1;
                column.isSort = column.isSort == false ? false : true;
                column.frozen = column.frozen ? true : false;
                column._width = g._getColumnWidth(column);
                column._hide = column.hide ? true : false;
            });
        },
        _getColumnWidth: function (column)
        {
            var g = this, p = this.options;
            if (column._width) return column._width;
            var colwidth = column.width || p.columnWidth;
            if (!colwidth || colwidth == "auto")
            {
                var autoColumnNumber = 0, noAutoColumnWidth = 0;
                $(g.columns).each(function (i, col)
                {
                    var colwidth = col.width || p.columnWidth;
                    var isAuto = (!colwidth || colwidth == "auto") ? true : false;
                    if (isAuto) autoColumnNumber++;
                    else noAutoColumnWidth += (parseInt(g._getColumnWidth(col)) + 1);
                });
                //hoping: 自动宽度时得排除一下滚动条的宽度，此方法不足的地方就是从服务器读取数据时只能一直在右边留一个滚动条的位置出来
                var _top = $(g.gridview2).children().next().offset().top;
                var _r = 100;
                if(g.options.usePager) _r = g.options.pageSize;
                if(g.rows.length) _r = g.rows.length;
                _r++;
                var _h = _r * g.options.rowHeight;
                var _t = 1;
                var _hh = 0;
                if(g.options.usePager) _hh = g.footbar.height();
                //检测一下是否显示了竖向滚动条
                if($(window).height()<_top + _h + _hh){
                    if(typeof($.fn.perfectScrollbar)=='function'){
                        _t = 1;
                    } else {
                        _t = 18;
                    }
                }
                colwidth = parseInt((g.grid.width() - noAutoColumnWidth) / autoColumnNumber) - _t;
            }
            if (typeof (colwidth) == "string" && colwidth.indexOf('%') > 0)
            {
                /*
                 * 修复行控件宽度被忽略的bug
                 */
                var lwidth = 0;
                if (g.enabledDetail())
                {
                    lwidth += p.detailColWidth;
                }
                if (g.enabledCheckbox())
                {
                    lwidth += p.checkboxColWidth;
                }
                if (g.options.rownumbers)
                {
                    lwidth += g.options.rownumbersColWidth;
                }
                column._width = colwidth = parseInt(parseInt(colwidth) * 0.01 * (g.grid.width() - lwidth - (g.columns.length / 2) - 1));
            }
            if (column.minWidth && colwidth < column.minWidth) colwidth = column.minWidth;
            if (column.maxWidth && colwidth > column.maxWidth) colwidth = column.maxWidth;
            return colwidth;
        },
        _createHeaderCell: function (column)
        {
            var g = this, p = this.options;
            var jcell = $("<th class='l-grid-hd-cell'><div class='l-grid-hd-cell-inner'><div class='datagrid-space'></div><div class='datagrid-label'></div></div></th>");
            if(p.enabledSort){
                jcell.addClass('cursorpointer');
            }
            jcell.attr("id", column['__domid']);
            if (!column['__leaf'])
                jcell.addClass("l-grid-hd-cell-mul");
            if (column.columnindex == g.columns.length - 1)
            {
                jcell.addClass("l-grid-hd-cell-last");
            }
            if (column.isrownumber)
            {
                jcell.addClass("datagrid-linenumber-td");
                jcell.html("<div class='l-grid-hd-cell-inner'></div>");
            }
            if (column.ischeckbox)
            {
                jcell.addClass("l-grid-hd-cell-checkbox");
                //hoping: 为了美观，这里对高度做一判断
                var _t = 0;
                if(p.headerRowHeight>13)
                    var _t = parseInt((p.headerRowHeight - 13)/2);
                jcell.html("<div class='l-grid-hd-cell-inner'><div class='l-grid-hd-cell-btn-checkbox' style='margin-top:"+_t+"px;'></div></div>");
            }
            if (column.isdetail)
            {
                jcell.addClass("l-grid-hd-cell-detail");
                jcell.html("<div class='l-grid-hd-cell-inner'><div class='datagrid-space'></div><div class='datagrid-label l-grid-hd-cell-btn-detail'></div></div>");
            }
            if (column.heightAlign)
            {
                $(".l-grid-hd-cell-inner:first", jcell).css("textAlign", column.heightAlign);
            }
            if (column['__colSpan']) jcell.attr("colSpan", column['__colSpan']);
            if (column['__rowSpan'])
            {
                jcell.attr("rowSpan", column['__rowSpan']);
                jcell.height(p.headerRowHeight * column['__rowSpan']);
                //hoping: 以前的修改，似乎没用。
                //var paddingTop = (p.headerRowHeight * column['__rowSpan'] - p.headerRowHeight) / 2 - 5;
                //$(".l-grid-hd-cell-inner:first", jcell).css("paddingTop", paddingTop);
            } else
            {
                //hoping: 还是表头高度
                jcell.height(p.origH);
            }
            if (column['__leaf'])
            {
                jcell.width(column['_width']);
                jcell.attr("columnindex", column['__leafindex']);
            }
            var cellHeight = jcell.height();
            if (!column['__rowSpan'] && cellHeight > 10) $(">div:first", jcell).height(cellHeight);
            if (column._hide) jcell.hide();
            if (column.name) jcell.attr({ columnname: column.name });
            var headerText = "";
            if (column.label && column.label != "") {
                headerText = column.label;
            } else if (column.headerRender) {
                headerText = column.headerRender(column);
            } else if (column.name) {
                headerText = column.name;
            } else {
                headerText = "&nbsp;";
            }
            $(".datagrid-label:first", jcell).html(headerText);
            if (!column.issystem && column['__leaf'] && column.resizable !== false && p.allowAdjustColWidth) {
                //调整宽度
                var $resizeMark = g.grid.find('.resizeProxy')

                if (!$resizeMark.length) {
                    $resizeMark = $('<div class="resizeProxy" style="left:0; display:none;position:absolute;"></div>')
                    $resizeMark.appendTo(g.grid)
                }

                g.colResizable[column['__id']] = jcell.ligerResizable({
                    handles: 'e',
                    onStartResize: function (e, ev)
                    {
                        this.proxy.hide();
                        $resizeMark.css({ height: g.grid.height(), top: 0, left: ev.pageX - g.grid.offset().left + parseInt(g.gridview.scrollLeft()) }).show();
                    },
                    onResize: function (e, ev)
                    {
                        g.colresizing = true;
                        $resizeMark.css({ left: ev.pageX - g.grid.offset().left + parseInt(g.gridview.scrollLeft()) });
                        //$('body').add(jcell).css('cursor', 'e-resize');
                    },
                    onStopResize: function (e)
                    {
                        g.colresizing = false;
                        $('body').add(jcell).css('cursor', 'default');
                        $resizeMark.hide();
                        g.setColumnWidth(column, parseInt(column._width) + e.diffX);
                        return false;
                    }
                });
            }
            g.trigger('headerCellBulid', [jcell, column]);
            return jcell;
        },
        _initBuildGridHeader: function ()
        {
            var g = this, p = this.options;
            g.gridtablewidth = 0;
            g.f.gridtablewidth = 0;
            if (g.colResizable)
            {
                for (var i in g.colResizable)
                {
                    g.colResizable[i].destroy();
                }
                g.colResizable = null;
            }
            g.colResizable = {};
            $("thead:first", g.gridheader).html("");
            $("thead:first", g.f.gridheader).html("");
            for (var level = 1; level <= g._columnMaxLevel; level++)
            {
                var columns = g.getColumns(level);           //获取level层次的列集合
                var islast = level == g._columnMaxLevel;     //是否最末级
                var tr = $("<tr class='l-grid-hd-row'></tr>");
                var trf = $("<tr class='l-grid-hd-row'></tr>");
                if (!islast) tr.add(trf).addClass("l-grid-hd-mul");
                $("thead:first", g.gridheader).append(tr);
                $("thead:first", g.f.gridheader).append(trf);
                var _f_cols = [], _cols = [];
                $(columns).each(function (i, column) {
                    if(column.frozen){
                        if(p.frozen){
                            trf.append(g._createHeaderCell(column));
                        }
                    } else {
                        tr.append(g._createHeaderCell(column));
                    }
                    //(column.frozen ? trf : tr).append(g._createHeaderCell(column));
                    if (column['__leaf'])
                    {
                        var colwidth = column['_width'];
                        if (column.frozen) {
                            if(p.frozen) {
                                _cols.push('<col style="width:' + (parseInt(colwidth) ? parseInt(colwidth) : 0) + 'px; display: none;">');
                                _f_cols.push('<col style="width:' + (parseInt(colwidth) ? parseInt(colwidth) : 0) + 'px;">');
                                g.f.gridtablewidth += (parseInt(colwidth) ? parseInt(colwidth) : 0) + 1;
                            }
                        } else {
                            _cols.push('<col style="width:' + (parseInt(colwidth) ? parseInt(colwidth) : 0) + 'px;">');
                            _f_cols.push('<col style="width:' + (parseInt(colwidth) ? parseInt(colwidth) : 0) + 'px; display: none;">');
                            g.gridtablewidth += (parseInt(colwidth) ? parseInt(colwidth) : 0) + 1;
                        }
                    }
                });
                g.gridheader.colgroup.html(_cols.join(''));
                g.f.gridheader.colgroup.html(_f_cols.join(''));
            }
            if (g._columnMaxLevel > 0)
            {
                //hoping: 还是表头高度
                var h = p.origH * g._columnMaxLevel;
                g.gridheader.add(g.f.gridheader).height(h);
                if (p.rownumbers && p.frozenRownumbers) g.f.gridheader.find("th:first").height(h);
            }
            g._updateFrozenWidth();
            $("div:first", g.gridheader).width(g.gridtablewidth);
        },
        _initBuildPopup: function ()
        {
            var g = this, p = this.options;
            $(':checkbox', g.popup).unbind();
            $('tbody tr', g.popup).remove();
            $(g.columns).each(function (i, column)
            {
                if (column.issystem) return;
                if (column.isAllowHide == false) return;
                var chk = 'checked="checked"';
                if (column._hide) chk = '';
                var header = column.label;
                $('tbody', g.popup).append('<tr><td class="l-column-left"><input type="checkbox" ' + chk + ' class="l-checkbox" columnindex="' + i + '"/></td><td class="l-column-right">' + header + '</td></tr>');
            });
            //表头 - 显示/隐藏'列控制'按钮事件
            if (p.allowHideColumn)
            {
                $('tr', g.popup).hover(function ()
                {
                    $(this).addClass('l-popup-row-over');
                },
                function ()
                {
                    $(this).removeClass('l-popup-row-over');
                });
                var onPopupCheckboxChange = function ()
                {
                    if ($('input:checked', g.popup).length + 1 <= p.minColToggle)
                    {
                        return false;
                    }
                    g.toggleCol(parseInt($(this).attr("columnindex")), this.checked, true);
                };
                    $(':checkbox', g.popup).bind('click', onPopupCheckboxChange);
            }
        },
        _initHeight: function ()
        {
            var g = this, p = this.options;
            if (p.height == 'auto')
            {
                g.gridbody.height('auto');
                g.f.gridbody.height('auto');
            }
            if (p.width)
            {
                g.grid.width(p.width);
            }
            g._onResize.call(g);
        },
        _initFootbar: function ()
        {
            var g = this, p = this.options;
            if (!p.usePager)
            {
                g.footbar.hide();
            }
        },
        _searchData: function (data, clause)
        {
            var g = this, p = this.options;
            var newData = new Array();
            for (var i = 0; i < data.length; i++)
            {
                if (clause(data[i], i))
                {
                    newData[newData.length] = data[i];
                }
            }
            return newData;
        },
        _clearGrid: function ()
        {
            var g = this, p = this.options;
            g._fixRows();
            for (var i in g.rows)
            {
                var rowobj = $(g.getRowObj(g.rows[i]));
                if (g.enabledFrozen())
                    rowobj = rowobj.add(g.getRowObj(g.rows[i], true));
                rowobj.unbind();
            }
            //清空数据
            g.gridbody.html("");
            g.f.gridbody.html("");
            g.recordNumber = 0;
            g.records = {};
            g.rows = [];
            g._fixRows();
            //清空选择的行
            g.selected = [];
            g.totalNumber = 0;
        },
        _fixRows : function()
        {
            var g = this, p = this.options;
            if (!g.rows) return;
            for (var i in g.rows)
            {
                if ($.isFunction(g.rows[i]))
                {
                    delete g.rows[i];
                }
            }
        },

        _fillGridBody: function (data, frozen,sourceType)
        {
            var g = this, p = this.options;
            //加载数据
            var gridhtmlarr = sourceType == "scrollappend" ? [] : ['<div class="datagrid-wrap-b"><table class="table table-bordered"><colgroup></colgroup><tbody>'];
            if (g.enabledGroup()) //启用分组模式
            {
                var groups = []; //分组列名数组
                var groupsdata = []; //切成几块后的数据
                g.groups = groupsdata;
                for (var rowparm in data)
                {
                    var item = data[rowparm];
                    var groupColumnValue = item[p.groupColumnName];
                    var valueIndex = $.inArray(groupColumnValue, groups);
                    if (valueIndex == -1)
                    {
                        groups.push(groupColumnValue);
                        valueIndex = groups.length - 1;
                        groupsdata.push([]);
                    }
                    groupsdata[valueIndex].push(item);
                }
                $(groupsdata).each(function (i, item)
                {
                    if (groupsdata.length == 1)
                        gridhtmlarr.push('<tr class="l-grid-grouprow l-grid-grouprow-last l-grid-grouprow-first"');
                    if (i == groupsdata.length - 1)
                        gridhtmlarr.push('<tr class="l-grid-grouprow l-grid-grouprow-last"');
                    else if (i == 0)
                        gridhtmlarr.push('<tr class="l-grid-grouprow l-grid-grouprow-first"');
                    else
                        gridhtmlarr.push('<tr class="l-grid-grouprow"');
                    gridhtmlarr.push(' groupindex"=' + i + '" >');
                    gridhtmlarr.push('<td colSpan="' + g.columns.length + '" class="l-grid-grouprow-cell">');
                    gridhtmlarr.push('<span class="l-grid-group-togglebtn">&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                    if (p.groupRender)
                        gridhtmlarr.push(p.groupRender(groups[i], item, p.groupColumnDisplay));
                    else
                        gridhtmlarr.push(p.groupColumnDisplay + ':' + groups[i]);


                    gridhtmlarr.push('</td>');
                    gridhtmlarr.push('</tr>');

                    gridhtmlarr.push(g._getHtmlFromData(item, frozen));
                    //汇总
                    if (g.isTotalSummary())
                        gridhtmlarr.push(g._getTotalSummaryHtml(item, "l-grid-totalsummary-group", frozen));
                });
            }
            else
            {
                gridhtmlarr.push(g._getHtmlFromData(data, frozen));
            }
            if (!sourceType == "scrollappend")
            {
                gridhtmlarr.push('</tbody></table></div>');
            }

            if (sourceType == "scrollappend")
            {
                (frozen ? g.f.gridbody : g.gridbody).find("tbody:first").append(gridhtmlarr.join(''));
            }
            else {
                var html = $(gridhtmlarr.join(''));
                if(frozen){
                    html.find('colgroup').html(g.f.gridheader.colgroup.html());
                    g.f.gridbody.html(html.html());
                    g.f.gridbody.colgroup = $('colgroup', g.f.gridbody);
                } else {
                    html.find('colgroup').html(g.gridheader.colgroup.html());
                    g.gridbody.html(html.html());
                    g.gridbody.colgroup = $('colgroup', g.gridbody);
                }
            }

            if (frozen)
            {
                g.f.gridbody.find(">l-jplace").remove();
                g.f.gridbody.append('<div class="l-jplace"></div>');
            }
            //分组时不需要
            if (!g.enabledGroup())
            {
                //创建汇总行
                g._bulidTotalSummary(frozen);
            }
            $("> div:first", g.gridbody).width(g.gridtablewidth);
            g._onResize();
        },
        _showData: function (sourceType)
        {
            var g = this, p = this.options;
            g.changedCells = {};
            var data = g.currentData[$.zui.ajaxReturn.root];
            if (p.usePager)
            {
                //更新总记录数
                if (p.dataAction === "server" && g.data && g.data[$.zui.ajaxReturn.total])
                    p.total = g.data[$.zui.ajaxReturn.total];
                else if (g.filteredData && g.filteredData[$.zui.ajaxReturn.root])
                    p.total = g.filteredData[$.zui.ajaxReturn.root].length;
                else if (g.data && g.data[$.zui.ajaxReturn.root])
                    p.total = g.data[$.zui.ajaxReturn.root].length;
                else if (data)
                    p.total = data.length;

                p.page = p.newPage;
                if (!p.total) p.total = 0;
                if (!p.page) p.page = 1;
                p.pageCount = Math.ceil(p.total / p.pageSize);
                if (!p.pageCount) p.pageCount = 1;
                //更新分页
                g._buildPager();
            }
            //加载中
            if (g.trigger('beforeShowData', [g.currentData]) == false) return;
            if (sourceType != "scrollappend")
            {
                g._clearGrid();
            }
            if (!data || !data.length)
            {
                g.gridview.addClass("l-grid-empty");
                $(g.element).addClass("l-empty");
                $("<div></div>").addClass("datagrid-wrap-b").appendTo(g.gridbody).css({
                    width: g.gridheader.find(">div:first").width(),
                    height: g.gridbody.height()
                });
                g._onResize.ligerDefer(g, 50);
                return;
            }
            else
            {
                g.gridview.removeClass("l-grid-empty");
                $(g.element).removeClass("l-empty");
            }
            $(".l-bar-btnload:first span", g.footbar).removeClass("disabled");
            g._updateGridData();
            if (g.enabledFrozen())
                g._fillGridBody(g.rows, true, sourceType);
            g._fillGridBody(g.rows, false, sourceType);
            g.trigger('SysGridHeightChanged');
            if (sourceType == "scroll")
            {
                g.trigger('sysScrollLoaded');
            }
            if (p.mouseoverRowCssClass)
            {
                for (var i in g.rows)
                {
                    var rowobj = $(g.getRowObj(g.rows[i]));
                    if (g.enabledFrozen())
                        rowobj = rowobj.add(g.getRowObj(g.rows[i], true));
                    rowobj.bind('mouseover.gridrow', function ()
                    {
                        g._onRowOver(this, true);
                    }).bind('mouseout.gridrow', function ()
                    {
                        g._onRowOver(this, false);
                    });
                }
            }
            g._fixHeight();
            g.gridbody.trigger('scroll.grid');
            if(p.rownumbers){
                //计算一下行数所需要的宽度
                var l1 = p.rownumbersColWidth + 2
                if(p.checkbox){
                    l1 = p.rownumbersColWidth + p.checkboxColWidth + 4;
                }
                var that = $('#'+g.id);
                var obj = that.find('.l-grid2');
                if(!obj.length || !obj[0].hasOwnProperty('offsetLeft') || obj[0].offsetLeft != l1){
                    that.find('.l-grid2').css({'left':l1, 'width':$(window).width()-l1});
                    that.find('.datagrid-box-l').width(l1);
                    that.find('.datagrid-box-l').find('.datagrid-linenumber-td').width(p.rownumbersColWidth);
                    that.find('.datagrid-box-l').find('.datagrid-linenumber-td').width(p.rownumbersColWidth);
                }
            }
            g.trigger('afterShowData', [g.currentData]);
        },
        _fixHeight: function ()
        {
            var g = this, p = this.options;
            if (p.fixedCellHeight || !p.frozen) return;
            var column1, column2;
            for (var i in g.columns)
            {
                var column = g.columns[i];
                if (column1 && column2) break;
                if (column.frozen && !column1)
                {
                    column1 = column;
                    continue;
                }
                if (!column.frozen && !column2)
                {
                    column2 = column;
                    continue;
                }
            }
            if (!column1 || !column2) return;
            for (var rowid in g.records)
            {
                var cell1 = g.getCellObj(rowid, column1), cell2 = g.getCellObj(rowid, column2);
                var height = Math.max($(cell1).height(), ($(cell2).height()));
                $(cell1).add(cell2).height(height);
            }
        },
        _getRowDomId: function (rowdata, frozen)
        {
            return this.id + "|" + (frozen ? "1" : "2") + "|" + rowdata['__id'];
        },
        _getCellDomId: function (rowdata, column)
        {
            return this._getRowDomId(rowdata, column.frozen) + "|" + column['__id'];
        },
        _getHtmlFromData: function (data, frozen)
        {
            if (!data) return "";
            var g = this, p = this.options;
            var gridhtmlarr = [];
            for (var i = 0, l = data.length; i < l; i++)
            {
                var item = data[i];
                var rowid = item['__id'];
                if (!item) continue;
                gridhtmlarr.push('<tr');
                gridhtmlarr.push(' id="' + g._getRowDomId(item, frozen) + '"');
                gridhtmlarr.push(' class="l-grid-row'); //class start
                if (!frozen && g.enabledCheckbox() && p.isChecked && p.isChecked(item))
                {
                    g.select(item);
                    gridhtmlarr.push(' datagrid-selected-tr');
                }
                else if (g.isSelected(item))
                {
                    gridhtmlarr.push(' datagrid-selected-tr');
                }
                else if (p.isSelected && p.isSelected(item))
                {
                    g.select(item);
                    gridhtmlarr.push(' datagrid-selected-tr');
                }
                gridhtmlarr.push('" ');  //class end
                if (p.rowAttrRender) gridhtmlarr.push(p.rowAttrRender(item, rowid));
                if (p.tree && g.collapsedRows && g.collapsedRows.length)
                {
                    var isHide = function ()
                    {
                        var pitem = g.getParent(item);
                        while (pitem)
                        {
                            if ($.inArray(pitem, g.collapsedRows) != -1) return true;
                            pitem = g.getParent(pitem);
                        }
                        return false;
                    };
                    if (isHide()) gridhtmlarr.push(' style="display:none;" ');
                }
                else if (p.tree && p.tree.isExtend)
                {
                    var isHide = function ()
                    {
                        var pitem = g.getParent(item);
                        while (pitem)
                        {
                            if (p.tree.isExtend(pitem) == false) return true;
                            pitem = g.getParent(pitem);
                        }
                        return false;
                    };
                    if (isHide()) gridhtmlarr.push(' style="display:none;" ');
                }
                gridhtmlarr.push('>');
                $(g.columns).each(function (columnindex, column)
                {
                    if (frozen != column.frozen) return;
                    gridhtmlarr.push('<td');
                    gridhtmlarr.push(' id="' + g._getCellDomId(item, this) + '"');
                    //如果是行序号(系统列)
                    if (this.isrownumber)
                    {
                        gridhtmlarr.push(' class="l-grid-row-cell datagrid-linenumber-td" style="width:' + this.width + 'px"><div class="l-grid-row-cell-inner"');
                        gridhtmlarr.push(' style = "');
                        if (!p.fixedCellHeight) {
                            gridhtmlarr.push('min-');
                        }
                        gridhtmlarr.push('height:' + p.rowHeight + 'px;text-align:right;padding-right: 2px;" ');
                        //这里显示的是行号
                        gridhtmlarr.push('>' + (p.pageSize*(p.page-1)+parseInt(item['__index']) + 1) + '</div></td>');
                        return;
                    }
                    //如果是复选框(系统列)
                    if (this.ischeckbox)
                    {
                        gridhtmlarr.push(' class="l-grid-row-cell l-grid-row-cell-checkbox" style="width:' + this.width + 'px"><div class="l-grid-row-cell-inner"');
                        if (p.fixedCellHeight)
                            gridhtmlarr.push(' style = "height:' + p.rowHeight + 'px;" ');
                        else
                            gridhtmlarr.push(' style = "min-height:' + p.rowHeight + 'px;" ');
                        gridhtmlarr.push('>');
                        //hoping: 为了美观，这里对高度做一判断
                        var _t = 0;
                        if(p.rowHeight>13)
                            var _t = parseInt((p.rowHeight - 13)/2);
                        gridhtmlarr.push('<span class="l-grid-row-cell-btn-checkbox" style="margin-top:'+_t+'px;"></span>');
                        gridhtmlarr.push('</div></td>');
                        return;
                    }
                        //如果是明细列(系统列)
                    else if (this.isdetail)
                    {
                        gridhtmlarr.push(' class="l-grid-row-cell l-grid-row-cell-detail" style="width:' + this.width + 'px"><div class="l-grid-row-cell-inner"');
                        if (p.fixedCellHeight)
                            gridhtmlarr.push(' style = "height:' + p.rowHeight + 'px;" ');
                        else
                            gridhtmlarr.push(' style = "min-height:' + p.rowHeight + 'px;" ');
                        gridhtmlarr.push('>');
                        if (!p.isShowDetailToggle || p.isShowDetailToggle(item))
                        {
                            gridhtmlarr.push('<span class="l-grid-row-cell-detailbtn"></span>');
                        }
                        gridhtmlarr.push('</div></td>');
                        return;
                    }
                    var colwidth = this._width;
                    gridhtmlarr.push(' class="l-grid-row-cell ');
                    if (this.islast)
                        gridhtmlarr.push('l-grid-row-cell-last ');
                    gridhtmlarr.push('"');
                    //if (this.columnname) gridhtmlarr.push('columnname="' + this.columnname + '"');
                    gridhtmlarr.push(' style = "');
                    gridhtmlarr.push('width:' + colwidth + 'px; ');
                    if (column._hide)
                    {
                        gridhtmlarr.push('display:none;');
                    }
                    gridhtmlarr.push(' ">');
                    gridhtmlarr.push(g._getCellHtml(item, column));
                    gridhtmlarr.push('</td>');
                });
                gridhtmlarr.push('</tr>');
            }
            return gridhtmlarr.join('');
        },
        _getCellHtml: function (rowdata, column)
        {
            var g = this, p = this.options;
            if (column.isrownumber) {
                return '<div class="l-grid-row-cell-inner">' + (parseInt(rowdata['__index']) + 1) + '</div>';
            }
            var htmlarr = [];
            htmlarr.push('<div class="l-grid-row-cell-inner"');
            //htmlarr.push('<div');
            //hoping 添加一些方便控制的属性
            var row = parseInt(rowdata['__index']) + 1;
            var col = parseInt(column.columnindex) + 1;
            htmlarr.push(' row="'+row+'" col="'+col+'"');

            htmlarr.push(' style = "width:' + parseInt(column._width) + 'px;'); //-8?
            if (p.fixedCellHeight) htmlarr.push('height:' + p.rowHeight + 'px;');
            htmlarr.push('min-height:' + p.rowHeight + 'px; ');
            //hoping: 让表行显示的美观一些
            htmlarr.push('line-height:' + p.rowHeight + 'px; ');
            if (column.align) htmlarr.push('text-align:' + column.align + ';');
            var content = g._getCellContent(rowdata, column);
            htmlarr.push('">' + content + '</div>');
            return htmlarr.join('');
        },
        _setValueByName: function (data, name, value)
        {
            if (!data || !name) return null;
            if (name.indexOf('.') == -1)
            {
                data[name] = value;
            }
            else
            {
                try
                {
                    new Function("data,value", "data." + name + "=value;")(data, value);
                }
                catch (e)
                {
                }
            }
        },
        _getValueByName: function (data, name)
        {
            if (!data || !name) return null;
            if (name.indexOf('.') == -1)
            {
                return data[name];
            }
            else
            {
                try
                {
                    return new Function("data", "return data." + name + ";")(data);
                }
                catch (e)
                {
                    return null;
                }
            }
        },
        _getCellContent: function (rowdata, column)
        {
            var g = this, p = this.options;
            if (!rowdata || !column) return "";
            if (column.isrownumber) return parseInt(rowdata['__index']) + 1;
            var rowid = rowdata['__id'];
            var rowindex = rowdata['__index'];
            var value = g._getValueByName(rowdata, column.name);
            var text = g._getValueByName(rowdata, column.textField);
            var content = "";
            if (column.render) {
                content = column.render.call(g, value, rowdata, column, rowindex);
            } else if (p.formatters[column.type]) {
                content = p.formatters[column.type].call(g, value, column);
            } else if (text != null) {
                content = text.toString();
            } else if (value != null) {
                content = value.toString();
            }
            if (p.tree && (p.tree.columnName != null && p.tree.columnName == column.name || p.tree.columnId != null && p.tree.columnId == column.id))
            {
                content = g._getTreeCellHtml(content, rowdata);
            }
            return content || "";
        },
        _getTreeCellHtml: function (oldContent, rowdata)
        {
            var level = rowdata['__level'];
            var g = this, p = this.options;
            //var isExtend = p.tree.isExtend(rowdata);
            var isExtend = $.inArray(rowdata, g.collapsedRows || []) == -1;
            var isParent = p.tree.isParent(rowdata);
            var content = "";
            level = parseInt(level) || 1;
            //hoping: 为了美观，这里对高度做一判断
            var _t = 0;
            if(p.rowHeight>21)
                _t = parseInt((p.rowHeight - 21)/2);
            _t = " style='margin-top:"+_t+"px;'";
            for (var i = 1; i < level; i++)
            {
                content += "<div class='l-grid-tree-space'></div>";
            }
            if (isExtend && isParent)
                content += "<div class='l-grid-tree-space l-grid-tree-link l-grid-tree-link-open'"+_t+"></div>";
            else if (isParent)
                content += "<div class='l-grid-tree-space l-grid-tree-link l-grid-tree-link-close'"+_t+"></div>";
            else
                content += "<div class='l-grid-tree-space'></div>";
            content += "<span class='l-grid-tree-content'>" + oldContent + "</span>";
            return content;
        },
        _getCurrentPageData: function (source)
        {
            var g = this, p = this.options;
            var data = {};
            data[$.zui.ajaxReturn.root] = [];
            if (!source || !source[$.zui.ajaxReturn.root] || !source[$.zui.ajaxReturn.root].length)
            {
                data[$.zui.ajaxReturn.total] = 0;
                return data;
            }
            data[$.zui.ajaxReturn.total] = source[$.zui.ajaxReturn.root].length;
            if (!p.newPage) p.newPage = 1;
            for (i = (p.newPage - 1) * p.pageSize; i < source[$.zui.ajaxReturn.root].length && i < p.newPage * p.pageSize; i++)
            {
                data[$.zui.ajaxReturn.root].push(source[$.zui.ajaxReturn.root][i]);
            }
            return data;
        },
        _getTotalInfo : function(column, data)
        {
            var g = this, p = this.options;
            try{
                var totalsummaryArr = [];
                if (!column.totalSummary) return null;
                var sum = 0, count = 0, avg = 0, min = 0, max = 0;
                if (data && data.length)
                {
                    max = parseFloat(data[0][column.name]);
                    min = parseFloat(data[0][column.name]);
                    for (var i = 0; i < data.length; i++)
                    {
                        count += 1;
                        var value = data[i][column.name];
                        if (typeof (value) === "string") value = value.replace(/\$|\,/g, '');
                        value = parseFloat(value);
                        if (!value) continue;
                        sum += value;
                        if (value > max) max = value;
                        if (value < min) min = value;
                    }
                    avg = sum * 1.0 / data.length;
                }
                return {
                    sum: sum,
                    count: count,
                    avg: avg,
                    min: min,
                    max: max
                };
            } catch (e)
            {
                return {};
            }
        },
        _getTotalCellContent: function (column, data)
        {
            var g = this, p = this.options;
            var totalsummaryArr = [];
            if (column.totalSummary)
            {
                var isExist = function (type)
                {
                    for (var i = 0; i < types.length; i++)
                        if (types[i].toLowerCase() == type.toLowerCase()) return true;
                    return false;
                };
                var info = g._getTotalInfo(column, data);
                if (column.totalSummary.render)
                {
                    var renderhtml = column.totalSummary.render(info, column, g.data);
                    totalsummaryArr.push(renderhtml);
                }
                else if (column.totalSummary.type && info)
                {
                    var types = column.totalSummary.type.split(',');
                    if (isExist('sum'))
                        totalsummaryArr.push("<div>Sum=" + info.sum.toFixed(2) + "</div>");
                    if (isExist('tsum'))
                        totalsummaryArr.push("<div>" + sum.toFixed(0) + "</div>");
                    if (isExist('count'))
                        totalsummaryArr.push("<div>Count=" + info.count + "</div>");
                    if (isExist('max'))
                        totalsummaryArr.push("<div>Max=" + info.max.toFixed(2) + "</div>");
                    if (isExist('min'))
                        totalsummaryArr.push("<div>Min=" + info.min.toFixed(2) + "</div>");
                    if (isExist('avg'))
                        totalsummaryArr.push("<div>Avg=" + info.avg.toFixed(2) + "</div>");
                }
            }
            return totalsummaryArr.join('');
        },
        _getTotalSummaryHtml: function (data, classCssName, frozen)
        {
            var g = this, p = this.options;
            var totalsummaryArr = [];
            if (classCssName)
                totalsummaryArr.push('<tr class="l-grid-totalsummary ' + classCssName + '">');
            else
                totalsummaryArr.push('<tr class="l-grid-totalsummary">');
            $(g.columns).each(function (columnindex, column)
            {
                if (this.frozen != frozen) return;
                //如果是行序号(系统列)
                if (this.isrownumber)
                {
                    totalsummaryArr.push('<td class="l-grid-totalsummary-cell l-grid-totalsummary-cell-rownumbers" style="width:' + this.width + 'px"><div>&nbsp;</div></td>');
                    return;
                }
                //如果是复选框(系统列)
                if (this.ischeckbox)
                {
                    totalsummaryArr.push('<td class="l-grid-totalsummary-cell l-grid-totalsummary-cell-checkbox" style="width:' + this.width + 'px"><div>&nbsp;</div></td>');
                    return;
                }
                    //如果是明细列(系统列)
                else if (this.isdetail)
                {
                    totalsummaryArr.push('<td class="l-grid-totalsummary-cell l-grid-totalsummary-cell-detail" style="width:' + this.width + 'px"><div>&nbsp;</div></td>');
                    return;
                }
                totalsummaryArr.push('<td class="l-grid-totalsummary-cell');
                if (this.islast)
                    totalsummaryArr.push(" l-grid-totalsummary-cell-last");
                totalsummaryArr.push('" ');
                totalsummaryArr.push('id="' + g.id + "|total" + g.totalNumber + "|" + column.__id + '" ');
                totalsummaryArr.push('width="' + this._width + '" ');
                columnname = this.columnname;
                if (columnname)
                {
                    totalsummaryArr.push('columnname="' + columnname + '" ');
                }
                totalsummaryArr.push('columnindex="' + columnindex + '" ');
                totalsummaryArr.push('><div class="l-grid-totalsummary-cell-inner"');
                if (column.align)
                    totalsummaryArr.push(' style="text-Align:' + column.align + ';"');
                totalsummaryArr.push('>');
                totalsummaryArr.push(g._getTotalCellContent(column, data));
                totalsummaryArr.push('</div></td>');
            });
            totalsummaryArr.push('</tr>');
            if (!frozen) g.totalNumber++;
            return totalsummaryArr.join('');
        },
        _bulidTotalSummary: function (frozen)
        {
            var g = this, p = this.options;
            if (!g.isTotalSummary()) return false;
            if (!g.currentData || g.currentData[$.zui.ajaxReturn.root].length == 0) return false;
            var totalRow = $(g._getTotalSummaryHtml(g.currentData[$.zui.ajaxReturn.root], null, frozen));
            $("tbody:first", frozen ? g.f.gridbody : g.gridbody).append(totalRow);
            if (frozen) g.totalRow1 = totalRow;
            else g.totalRow2 = totalRow;
        },
        updateTotalSummary: function ()
        {
            var g = this, p = this.options;
            g.reRender({ totalOnly: true });
        },
        getPageInterval: function (count, pageCurrent, showPageNum) {
            var half = Math.ceil(showPageNum / 2), limit = count - showPageNum,
                start = pageCurrent > half ? Math.max(Math.min(pageCurrent - half, limit), 0) : 0,
                end = pageCurrent > half ? Math.min((pageCurrent + half), count) : Math.min(showPageNum, count)

            if (end - start == showPageNum) end = end + 1
            if (end < showPageNum) end = end + 1
            if (start + 1 == end) end = end + 1

            return {start:start + 1, end:end}
        },
        _buildPager: function ()
        {
            var g = this, p = this.options, paging = g.paging, pr = $.zui.regional.pagination,
            btnpaging = FRAG.gridPaging, pageNums = [], pageCount = p.pageCount, interval,
            pagingHtml = $.zui.StrBuilder()

            //$('.pcontrol input', g.footbar).val(p.page);
            if (!p.pageCount) p.pageCount = 1;
            if (!p.total) p.total = 0;

            interval = g.getPageInterval(pageCount, p.page, p.showPagenum)

            for (var i = interval.start; i < interval.end; i++) {
                pageNums.push(FRAG.gridPageNum.replace('#num#', i).replace('#active#', (p.page == i ? ' active' : '')))
            }

            btnpaging = $.zui.doRegional(btnpaging.replaceAll('#pageCurrent#', p.page).replaceAll('#count#', p.total + '/' + parseInt((pageCount || 0), 10)), pr)

            pagingHtml
                .add('<div class="paging-content">')
                .add('<div class="datagrid-paging">')
                .add(btnpaging.replace('#pageNumFrag#', pageNums.join('')))
                .add('</div>')
                .add('</div>')
            g.footbar.html(pagingHtml.toString())

            if(p.checkAction){
                g.footbar.find('div.paging-content').prepend($('<div>').addClass('datagrid-checkaction').addClass('pull-left').html(p.checkAction));
                g.footbar.find('div.paging-content').find('div.datagrid-paging').addClass('pull-right');
            }

            if (!p.total) {
                $(g.footbar).addClass("disabled");
            }
            if (p.page == 1) {
                $(".page-first", g.footbar).addClass("disabled");
                $(".page-prev", g.footbar).addClass("disabled");
            }
            else if (p.page > p.pageCount && p.pageCount > 0) {
                $(".page-first", g.footbar).removeClass("disabled");
                $(".page-prev", g.footbar).removeClass("disabled");
            }
            if (p.page == p.pageCount) {
                $(".page-last", g.footbar).addClass("disabled");
                $(".page-next", g.footbar).addClass("disabled");
            }
            else if (p.page < p.pageCount && p.pageCount > 0) {
                $(".page-last", g.footbar).removeClass("disabled");
                $(".page-next", g.footbar).removeClass("disabled");
            }
        },
        _getRowIdByDomId: function (domid)
        {
            var ids = domid.split('|');
            var rowid = ids[2];
            return rowid;
        },
        _getRowByDomId: function (domid)
        {
            return this.records[this._getRowIdByDomId(domid)];
        },
        _getSrcElementByEvent: function (e)
        {
            var g = this;
            var obj = (e.target || e.srcElement);
            var jobjs = $(obj).parents().add(obj);
            var fn = function (parm)
            {
                for (var i = 0, l = jobjs.length; i < l; i++)
                {
                    if (typeof parm == "string")
                    {
                        if ($(jobjs[i]).hasClass(parm)) return jobjs[i];
                    }
                    else if (typeof parm == "object")
                    {
                        if (jobjs[i] == parm) return jobjs[i];
                    }
                }
                return null;
            };
            if (jobjs.index(this.element) == -1) {
                return { out: true };
            }
            var indetail = false;
            if (jobjs.hasClass("l-grid-detailpanel") && g.detailrows)
            {
                for (var i = 0, l = g.detailrows.length; i < l; i++)
                {
                    if (jobjs.index(g.detailrows[i]) != -1)
                    {
                        indetail = true;
                        break;
                    }
                }
            }
            var r = {
                grid: fn("zui-datagrid"),
                indetail: indetail,
                frozen: fn(g.gridview1[0]) ? true : false,
                header: fn("datagrid-title"), //标题
                gridheader: fn("datagrid-box-h"), //表格头
                gridbody: fn("datagrid-box-b"),
                total: fn("l-panel-bar-total"), //总汇总
                popup: fn("l-grid-popup"),
                pagebar: fn("datagrid-paging-box")
            };
            if (r.gridheader)
            {
                r.hrow = fn("l-grid-hd-row");
                r.hcell = fn("l-grid-hd-cell");
                r.hcelltext = fn("l-grid-hd-cell-inner");
                r.checkboxall = fn("l-grid-hd-cell-checkbox");
                if (r.hcell)
                {
                    var columnid = r.hcell.id.split('|')[2];
                    r.column = g._columns[columnid];
                }
            }
            if (r.gridbody)
            {
                r.row = fn("l-grid-row");
                r.cell = fn("l-grid-row-cell");
                r.checkbox = fn("l-grid-row-cell-btn-checkbox");
                r.groupbtn = fn("l-grid-group-togglebtn");
                r.grouprow = fn("l-grid-grouprow");
                r.detailbtn = fn("l-grid-row-cell-detailbtn");
                r.detailrow = fn("l-grid-detailpanel");
                r.totalrow = fn("l-grid-totalsummary");
                r.totalcell = fn("l-grid-totalsummary-cell");
                r.rownumberscell = $(r.cell).hasClass("datagrid-linenumber-td") ? r.cell : null;
                r.detailcell = $(r.cell).hasClass("l-grid-row-cell-detail") ? r.cell : null;
                r.checkboxcell = $(r.cell).hasClass("l-grid-row-cell-checkbox") ? r.cell : null;
                r.treelink = fn("l-grid-tree-link");
                if (r.row) r.data = this._getRowByDomId(r.row.id);
            }
            if (r.pagebar) {
                r.first = fn("page-first");
                r.last = fn("page-last");
                r.next = fn("page-next");
                r.prev = fn("page-prev");
                r.load = fn("page-reload");
                r.button = fn("page-num");
            }

            return r;
        },
        _getOnePageHeight : function()
        {
            var g = this, p = this.options;
            return (parseFloat(p.rowHeight || 24) + 1)*parseInt(p.pageSize);
        },

        _setEvent: function ()
        {
            var g = this, p = this.options;
            g.grid.bind("dblclick.grid", function (e)
            {
                g._onDblClick.call(g, e);
            });
            g.grid.bind("contextmenu.grid", function (e)
            {
                return g._onContextmenu.call(g, e);
            });
            $(document).bind("click.grid", function (e)
            {
                g._onClick.call(g, e);
            });
            $(window).on($.zui.eventType.resizeGrid, $.proxy(g._onResize.call(g), g));
            $(window).bind("resize.grid", function (e){g._onResize.call(g);});
            $(document).bind("keydown.grid", function (e)
            {
                if (e.ctrlKey) g.ctrlKey = true;
            });
            $(document).bind("keyup.grid", function (e)
            {
                delete g.ctrlKey;
            });
            //表体 - 滚动联动事件
            g.gridbody.bind('scroll.grid', function ()
            {
                var scrollLeft = g.gridbody.scrollLeft();
                var scrollTop = g.gridbody.scrollTop();
                if (scrollLeft != null)
                    g.gridheader[0].scrollLeft = scrollLeft;
                if (scrollTop != null)
                    g.f.gridbody[0].scrollTop = scrollTop;
                g.trigger('SysGridHeightChanged');
            });
            //工具条 - 切换每页记录数事件
            $('select', g.toolbar).change(function ()
            {
                if (g.isDataChanged && p.dataAction != "local" && !confirm(p.isContinueByDataChanged))
                    return false;
                p.newPage = 1;
                p.pageSize = this.value;
                g.loadData(p.dataAction != "local" ? p.where : false);
            });
            //工具条 - 切换当前页事件
            $('span.pcontrol :text', g.toolbar).blur(function (e)
            {
                g.changePage('input');
            });
        },
        _onRowOver: function (rowParm, over)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            var methodName = over ? "addClass" : "removeClass";
            if (g.enabledFrozen())
                $(g.getRowObj(rowdata, true))[methodName](p.mouseoverRowCssClass);
            $(g.getRowObj(rowdata, false))[methodName](p.mouseoverRowCssClass);
        },
        _onContextmenu: function (e)
        {
            var g = this, p = this.options;
            var src = g._getSrcElementByEvent(e);
            if (src.row)
            {
                if (p.whenRClickToSelect)
                    g.select(src.data);
                if (g.hasBind('contextmenu'))
                {
                    return g.trigger('contextmenu', [{ data: src.data, rowindex: src.data['__index'], row: src.row }, e]);
                }
            }
            else if (src.hcell)
            {
                if (!p.allowHideColumn) return true;
                var columnindex = $(src.hcell).attr("columnindex");
                if (columnindex == undefined) return true;
                var left = (e.pageX - g.gridview.offset().left + parseInt(g.gridview.scrollLeft()));
                if (columnindex == g.columns.length - 1) left -= 50;
                g.popup.css({ left: left, top: g.gridheader.height() + 1 });
                g.popup.toggle();
                return false;
            }
        },
        _onDblClick: function (e)
        {
            var g = this, p = this.options;
            var src = g._getSrcElementByEvent(e);
            if (src.row)
            {
                g.trigger('dblClickRow', [src.data, src.data['__id'], src.row]);
            }
        },
        _onClick: function (e)
        {
            var obj = (e.target || e.srcElement);
            var g = this, p = this.options;
            var src = g._getSrcElementByEvent(e);
            if (src.out)
            {
                if (p.allowHideColumn) g.popup.hide();
                return;
            }
            if (src.indetail)
        {
                return;
            }
            if (p.allowHideColumn)
            {
                if (!src.popup)
                {
                    g.popup.hide();
                }
            }
            if (src.checkboxall) //复选框全选
            {
                var row = $(src.hrow);
                var uncheck = row.hasClass("l-checked");
                if (g.trigger('beforeCheckAllRow', [!uncheck, g.element]) == false) return false;
                if (uncheck)
                {
                    row.removeClass("l-checked");
                }
                else
                {
                    row.addClass("l-checked");
                }
                g.selected = [];
                for (var rowid in g.records)
                {
                    if (uncheck)
                        g.unselect(g.records[rowid]);
                    else
                        g.select(g.records[rowid]);
                }
                g.trigger('checkAllRow', [!uncheck, g.element]);
            }
            else if (src.hcelltext) //排序
            {
                var hcell = $(src.hcelltext).parent().parent();
                if (!p.enabledSort || !src.column) return;
                if (src.column.isSort == false) return;
                var sort = $('.datagrid-label:first>i.sort', src.hcelltext);//$(".l-grid-hd-cell-sort:first", hcell);
                var columnName = src.column.name;
                if (!columnName) return;
                if (sort.length > 0)
                {
                    if (sort.hasClass("icon-long-arrow-up"))
                    {
                        sort.removeClass('icon-long-arrow-up').addClass('icon-long-arrow-down');
                        g.trigger('ChangeSort', [columnName, 'desc']);
                        g.changeSort(columnName, 'desc');
                    }
                    else if (sort.hasClass("icon-long-arrow-down"))
                    {
                        sort.removeClass('icon-long-arrow-down').addClass('icon-long-arrow-up');
                        g.trigger('ChangeSort', [columnName, 'asc']);
                        g.changeSort(columnName, 'asc');
                    }
                }
                else
                {
                    //hcell.removeClass("l-grid-hd-cell-desc").addClass("l-grid-hd-cell-asc");
                    $('.datagrid-label:first', src.hcelltext).prepend("<i class='sort icon-long-arrow-up'></i>");
                    g.trigger('ChangeSort', [columnName, 'asc']);
                    g.changeSort(columnName, 'asc');
                }
                $(".datagrid-label i.sort", g.gridheader).add($(".datagrid-label i.sort", g.f.gridheader)).not($(".datagrid-label i.sort", src.hcelltext)).remove();
            }
                //明细
            else if (src.detailbtn && p.detail)
            {
                var item = src.data;
                var row = $([g.getRowObj(item, false)]);
                if (g.enabledFrozen()) row = row.add(g.getRowObj(item, true));
                var rowid = item['__id'];
                if ($(src.detailbtn).hasClass("l-open"))
                {
                    if (p.detail.onCollapse)
                        p.detail.onCollapse(item, $(".l-grid-detailpanel-inner:first", nextrow)[0]);
                    row.next("tr.l-grid-detailpanel").hide();
                    $(src.detailbtn).removeClass("l-open");
                }
                else
                {
                    var nextrow = row.next("tr.l-grid-detailpanel");
                    if (nextrow.length > 0)
                    {
                        nextrow.show();
                        if (p.detail.onExtend)
                            p.detail.onExtend(item, $(".l-grid-detailpanel-inner:first", nextrow)[0]);
                        $(src.detailbtn).addClass("l-open");
                        g.trigger('SysGridHeightChanged');
                        return;
                    }
                    $(src.detailbtn).addClass("l-open");
                    var frozenColNum = 0;
                    for (var i = 0; i < g.columns.length; i++)
                        if (g.columns[i].frozen) frozenColNum++;
                    var detailRow =       $("<tr class='l-grid-detailpanel'><td><div class='l-grid-detailpanel-inner' style='display:none'></div></td></tr>");
                    var detailFrozenRow = $("<tr class='l-grid-detailpanel'><td><div class='l-grid-detailpanel-inner' style='display:none'></div></td></tr>");
                    detailRow.find("div:first").width(g.gridheader.find("div:first").width());
                    detailRow.attr("id", g.id + "|detail|" + rowid);
                    g.detailrows = g.detailrows || [];
                    g.detailrows.push(detailRow[0]);
                    g.detailrows.push(detailFrozenRow[0]);
                    var detailRowInner = $("div:first", detailRow);
                    detailRowInner.parent().attr("colSpan", g.columns.length - frozenColNum);
                    row.eq(0).after(detailRow);
                    if (frozenColNum > 0)
                    {
                        detailFrozenRow.find("td:first").attr("colSpan", frozenColNum);
                        row.eq(1).after(detailFrozenRow);
                    }
                    if (p.detail.onShowDetail)
                    {
                        p.detail.onShowDetail(item, detailRowInner[0], function ()
                        {
                            g.trigger('SysGridHeightChanged');
                        });
                        $("div:first", detailFrozenRow).add(detailRowInner).show();//.height(p.detail.height || p.detailHeight);
                    }
                    else if (p.detail.render)
                    {
                        detailRowInner.append(p.detail.render());
                        detailRowInner.show();
                    }
                    g.trigger('SysGridHeightChanged');
                }
            }
            else if (src.groupbtn)
            {
                var grouprow = $(src.grouprow);
                var opening = true;
                if ($(src.groupbtn).hasClass("l-grid-group-togglebtn-close"))
                {
                    $(src.groupbtn).removeClass("l-grid-group-togglebtn-close");

                    if (grouprow.hasClass("l-grid-grouprow-last"))
                    {
                        $("td:first", grouprow).width('auto');
                    }
                }
                else
                {
                    opening = false;
                    $(src.groupbtn).addClass("l-grid-group-togglebtn-close");
                    if (grouprow.hasClass("l-grid-grouprow-last"))
                    {
                        $("td:first", grouprow).width(g.gridtablewidth);
                    }
                }
                var currentRow = grouprow.next(".l-grid-row,.l-grid-totalsummary-group,.l-grid-detailpanel");
                while (true)
                {
                    if (currentRow.length == 0) break;
                    if (opening)
                    {
                        currentRow.show();
                        //如果是明细展开的行，并且之前的状态已经是关闭的，隐藏之
                        if (currentRow.hasClass("l-grid-detailpanel") && !currentRow.prev().find("td.l-grid-row-cell-detail:first span.l-grid-row-cell-detailbtn:first").hasClass("l-open"))
                        {
                            currentRow.hide();
                        }
                    }
                    else
                    {
                        currentRow.hide();
                    }
                    currentRow = currentRow.next(".l-grid-row,.l-grid-totalsummary-group,.l-grid-detailpanel");
                }
                g.trigger(opening ? 'groupExtend' : 'groupCollapse');
                g.trigger('SysGridHeightChanged');
            }
                //树 - 伸展/收缩节点
            else if (src.treelink)
            {
                g.toggle(src.data);
            }
            else if (src.row && g.enabledCheckbox()) //复选框选择行
            {
                //复选框
                var selectRowButtonOnly = p.selectRowButtonOnly ? true : false;
                if (obj.tagName.toLowerCase() == "a") return;
                if ((src.checkbox || !selectRowButtonOnly) && p.selectable != false)
                {
                    var row = $(src.row);
                    var uncheck = row.hasClass("datagrid-selected-tr");
                    if (g.trigger('beforeCheckRow', [!uncheck, src.data, src.data['__id'], src.row]) == false)
                        return false;
                    var met = uncheck ? 'unselect' : 'select';
                    g[met](src.data);
                    if (p.tree && p.autoCheckChildren)
                    {
                        var children = g.getChildren(src.data, true);
                        for (var i = 0, l = children.length; i < l; i++)
                        {
                            g[met](children[i]);
                        }
                    }
                    g.trigger('checkRow', [!uncheck, src.data, src.data['__id'], src.row]);
                }
            }
            else if (src.row && !g.enabledCheckbox() && p.selectable != false)
            {

                //选择行
                if ($(src.row).hasClass("datagrid-selected-tr"))
                {
                    if (!p.allowUnSelectRow)
                    {
                        $(src.row).addClass("l-selected-again");
                        return;
                    }
                    g.unselect(src.data);
                }
                else
                {
                    g.select(src.data);
                }
            }
            else if (src.pagebar)
            {
                if (src.first)
                {
                    if (g.trigger('toFirst', [g.element]) == false) return false;
                    g.changePage('first');
                }
                else if (src.prev)
                {
                    if (g.trigger('toPrev', [g.element]) == false) return false;
                    g.changePage('prev');
                }
                else if (src.next)
                {
                    if (g.trigger('toNext', [g.element]) == false) return false;
                    g.changePage('next');
                }
                else if (src.last)
                {
                    if (g.trigger('toLast', [g.element]) == false) return false;
                    g.changePage('last');
                }
                else if (src.button) {
                    if($(src.button).hasClass('active')) return false;
                    if (g.trigger('toPage', [g.element]) == false) return false;
                    g.changePage($("a", src.button).html());
                }
                else if (src.load)
                {
                    if ($("span", src.load).hasClass("disabled")) return false;
                    if (g.trigger('reload', [g.element]) == false) return false;
                    g.loadData(p.where);
                }
            }
        },
        select: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            var rowid = rowdata['__id'];
            var rowobj = g.getRowObj(rowid);
            if (!p.rowSelectable || g.trigger('beforeSelectRow', [rowdata, rowid, rowobj]) == false) return;
            var rowobj1 = g.getRowObj(rowid, true);
            if ((!g.enabledCheckbox() && !g.ctrlKey) || p.isSingleCheck) //单选
            {
                for (var i in g.selected)
                {
                    var o = g.selected[i];
                    if (o['__id'] in g.records)
                    {
                        $(g.getRowObj(o)).removeClass("datagrid-selected-tr l-selected-again");
                        if (g.enabledFrozen())
                            $(g.getRowObj(o, true)).removeClass("datagrid-selected-tr l-selected-again");
                    }
                }
                g.selected = [];
            }
            if (rowobj) $(rowobj).addClass("datagrid-selected-tr");
            if (rowobj1) $(rowobj1).addClass("datagrid-selected-tr");
            g.selected[g.selected.length] = rowdata;
            g.trigger('selectRow', [rowdata, rowid, rowobj]);
        },
        unselect: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            var rowid = rowdata['__id'];
            var rowobj = g.getRowObj(rowid);
            var rowobj1 = g.getRowObj(rowid, true);
            $(rowobj).removeClass("datagrid-selected-tr l-selected-again");
            if (g.enabledFrozen())
                $(rowobj1).removeClass("datagrid-selected-tr l-selected-again");
            g._removeSelected(rowdata);
            g.trigger('unSelectRow', [rowdata, rowid, rowobj]);
        },
        isSelected: function (rowParm)
        {
            var g = this, p = this.options;
            var rowdata = g.getRow(rowParm);
            for (var i in g.selected)
            {
                if (g.selected[i] == rowdata) return true;
            }
            return false;
        },
        arrayToTree: function (data, id, pid)      //将ID、ParentID这种数据格式转换为树格式
        {
            var g = this, p = this.options;
            var childrenName = "children";
            if (p.tree) childrenName = p.tree.childrenName;
            if (!data || !data.length) return [];
            var targetData = [];                    //存储数据的容器(返回)
            var records = {};
            var itemLength = data.length;           //数据集合的个数
            for (var i = 0; i < itemLength; i++)
            {
                var o = data[i];
                var key = getKey(o[id]);
                records[key] = o;
            }
            for (var i = 0; i < itemLength; i++)
            {
                var currentData = data[i];
                var key = getKey(currentData[pid]);
                var parentData = records[key];
                if (!parentData)
                {
                    targetData.push(currentData);
                    continue;
                }
                parentData[childrenName] = parentData[childrenName] || [];
                parentData[childrenName].push(currentData);
            }
            return targetData;

            function getKey(key)
            {
                if (typeof (key) == "string") key = key.replace(/[.]/g, '').toLowerCase();
                return key;
            }
        },
        _onResize: function ()
        {
            var g = this, p = this.options;
            var pp = $(g.element).parent();
            if(p.inWindow) {
                var windowHeight = $(window).height();
            } else {
                var windowHeight = g.grid.parent().height();
            }
            g.grid.width(pp.width());
            g.footbar.width(pp.width());

            //hoping: 修正一下高度的问题
            if (p.height && p.height != 'auto' && p.height != '100%')
            {
                //if(g.windowHeight != undefined && g.windowHeight == windowHeight) return;

                var h = 0;
                var parentHeight = null;
                if (typeof (p.height) == "string" && p.height.indexOf('%') > 0)
                {
                    var gridparent = g.grid.parent();
                    parentHeight = windowHeight;
                    if (p.inWindow)
                    {
                        parentHeight -= parseInt($('body').css('paddingTop'));
                        parentHeight -= parseInt($('body').css('paddingBottom'));
                    }
                    h = parentHeight * parseInt(p.height) * 0.01;
                    if (p.inWindow || gridparent[0].tagName.toLowerCase() == "body")
                        h -= (g.grid.offset().top - parseInt($('body').css('paddingTop')));
                }
                else
                {
                    h = parseInt(p.height);
                }
                h += p.heightDiff;
                g.windowHeight = windowHeight;
                g._setHeight(h);
            }
            else
            {
                var h = 0;
                g._setHeight(windowHeight + h);//-g.options.rowHeight);
                g._updateHorizontalScrollStatus.ligerDefer(g, 10);
            }
            if (g.enabledFrozen())
            {
                var gridView1Width = g.gridview1.width();
                var gridViewWidth = pp.width();
                if (gridViewWidth - gridView1Width <= 0)
                {
                    g.gridview2.css({
                        width: 'auto'
                    });
                } else
                {
                    g.gridview2.css({
                        width: gridViewWidth - gridView1Width
                    });
                }
            }
            g.trigger('SysGridHeightChanged');
        }
    });

    $.ligerui.controls.Grid.prototype.enabledTotal = $.ligerui.controls.Grid.prototype.isTotalSummary;
    $.ligerui.controls.Grid.prototype.getSelected = $.ligerui.controls.Grid.prototype.getSelectedRow;
    $.ligerui.controls.Grid.prototype.getSelecteds = $.ligerui.controls.Grid.prototype.getSelectedRows;
    $.ligerui.controls.Grid.prototype.getCheckedRows = $.ligerui.controls.Grid.prototype.getSelectedRows;
    $.ligerui.controls.Grid.prototype.getCheckedRowObjs = $.ligerui.controls.Grid.prototype.getSelectedRowObjs;
    $.ligerui.controls.Grid.prototype.setOptions = $.ligerui.controls.Grid.prototype.set;
    $.ligerui.controls.Grid.prototype.reload = $.ligerui.controls.Grid.prototype.loadData;
    $.ligerui.controls.Grid.prototype.refreshSize = $.ligerui.controls.Grid.prototype._onResize;
})(jQuery);
