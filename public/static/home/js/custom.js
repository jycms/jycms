// Lazy Load - jQuery plugin for lazy loading images Version: 1.9.0
!function(a,b,c,d){var e=a(b);a.fn.lazyload=function(f){function g(){var b=0;i.each(function(){var c=a(this);if(!j.skip_invisible||c.is(":visible"))if(a.abovethetop(this,j)||a.leftofbegin(this,j));else if(a.belowthefold(this,j)||a.rightoffold(this,j)){if(++b>j.failure_limit)return!1}else c.trigger("appear"),b=0})}var h,i=this,j={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!0,appear:null,load:null,placeholder:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"};return f&&(d!==f.failurelimit&&(f.failure_limit=f.failurelimit,delete f.failurelimit),d!==f.effectspeed&&(f.effect_speed=f.effectspeed,delete f.effectspeed),a.extend(j,f)),h=j.container===d||j.container===b?e:a(j.container),0===j.event.indexOf("scroll")&&h.bind(j.event,function(){return g()}),this.each(function(){var b=this,c=a(b);b.loaded=!1,(c.attr("src")===d||c.attr("src")===!1)&&c.attr("src",j.placeholder),c.one("appear",function(){if(!this.loaded){if(j.appear){var d=i.length;j.appear.call(b,d,j)}a("<img />").bind("load",function(){var d=c.data(j.data_attribute);c.hide(),c.is("img")?c.attr("src",d):c.css("background-image","url('"+d+"')"),c[j.effect](j.effect_speed),b.loaded=!0;var e=a.grep(i,function(a){return!a.loaded});if(i=a(e),j.load){var f=i.length;j.load.call(b,f,j)}}).attr("src",c.data(j.data_attribute))}}),0!==j.event.indexOf("scroll")&&c.bind(j.event,function(){b.loaded||c.trigger("appear")})}),e.bind("resize",function(){g()}),/iphone|ipod|ipad.*os 5/gi.test(navigator.appVersion)&&e.bind("pageshow",function(b){b.originalEvent&&b.originalEvent.persisted&&i.each(function(){a(this).trigger("appear")})}),a(c).ready(function(){g()}),this},a.belowthefold=function(c,f){var g;return g=f.container===d||f.container===b?(b.innerHeight?b.innerHeight:e.height())+e.scrollTop():a(f.container).offset().top+a(f.container).height(),g<=a(c).offset().top-f.threshold},a.rightoffold=function(c,f){var g;return g=f.container===d||f.container===b?e.width()+e.scrollLeft():a(f.container).offset().left+a(f.container).width(),g<=a(c).offset().left-f.threshold},a.abovethetop=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollTop():a(f.container).offset().top,g>=a(c).offset().top+f.threshold+a(c).height()},a.leftofbegin=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollLeft():a(f.container).offset().left,g>=a(c).offset().left+f.threshold+a(c).width()},a.inviewport=function(b,c){return!(a.rightoffold(b,c)||a.leftofbegin(b,c)||a.belowthefold(b,c)||a.abovethetop(b,c))},a.extend(a.expr[":"],{"below-the-fold":function(b){return a.belowthefold(b,{threshold:0})},"above-the-top":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-screen":function(b){return a.rightoffold(b,{threshold:0})},"left-of-screen":function(b){return!a.rightoffold(b,{threshold:0})},"in-viewport":function(b){return a.inviewport(b,{threshold:0})},"above-the-fold":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-fold":function(b){return a.rightoffold(b,{threshold:0})},"left-of-fold":function(b){return!a.rightoffold(b,{threshold:0})}})}(jQuery,window,document);

!function(){var a=jQuery.event.special,b="D"+ +new Date,c="D"+(+new Date+1);a.scrollstart={setup:function(){var c,d=function(b){var d=this,e=arguments;c?clearTimeout(c):(b.type="scrollstart",jQuery.event.dispatch.apply(d,e)),c=setTimeout(function(){c=null},a.scrollstop.latency)};jQuery(this).bind("scroll",d).data(b,d)},teardown:function(){jQuery(this).unbind("scroll",jQuery(this).data(b))}},a.scrollstop={latency:300,setup:function(){var b,d=function(c){var d=this,e=arguments;b&&clearTimeout(b),b=setTimeout(function(){b=null,c.type="scrollstop",jQuery.event.dispatch.apply(d,e)},a.scrollstop.latency)};jQuery(this).bind("scroll",d).data(c,d)},teardown:function(){jQuery(this).unbind("scroll",jQuery(this).data(c))}}}();

// Infinite Ajax Scroll, a jQuery plugin 1.0.2
(function(e){"use strict";Date.now=Date.now||function(){return+(new Date)},e.ias=function(t){function u(){var t;i.onChangePage(function(e,t,r){s&&s.setPage(e,r),n.onPageChange.call(this,e,r,t)});if(n.triggerPageThreshold>0)a();else if(e(n.next).attr("href")){var u=r.getCurrentScrollOffset(n.scrollContainer);E(function(){p(u)})}return s&&s.havePage()&&(l(),t=s.getPage(),r.forceScrollTop(function(){var n;t>1?(v(t),n=h(!0),e("html, body").scrollTop(n)):a()})),o}function a(){c(),n.scrollContainer.scroll(f)}function f(){var e,t;e=r.getCurrentScrollOffset(n.scrollContainer),t=h(),e>=t&&(m()>=n.triggerPageThreshold?(l(),E(function(){p(e)})):p(e))}function l(){n.scrollContainer.unbind("scroll",f)}function c(){e(n.pagination).hide()}function h(t){var r,i;return r=e(n.container).find(n.item).last(),r.size()===0?0:(i=r.offset().top+r.height(),t||(i+=n.thresholdMargin),i)}function p(t,r){var s;s=e(n.next).attr("href");if(!s)return n.noneleft&&e(n.container).find(n.item).last().after(n.noneleft),l();if(n.beforePageChange&&e.isFunction(n.beforePageChange)&&n.beforePageChange(t,s)===!1)return;i.pushPages(t,s),l(),y(),d(s,function(t,i){var o=n.onLoadItems.call(this,i),u;o!==!1&&(e(i).hide(),u=e(n.container).find(n.item).last(),u.after(i),e(i).fadeIn()),s=e(n.next,t).attr("href"),e(n.pagination).replaceWith(e(n.pagination,t)),b(),c(),s?a():l(),n.onRenderComplete.call(this,i),r&&r.call(this)})}function d(t,r,i){var s=[],o,u=Date.now(),a,f;i=i||n.loaderDelay,e.get(t,null,function(t){o=e(n.container,t).eq(0),0===o.length&&(o=e(t).filter(n.container).eq(0)),o&&o.find(n.item).each(function(){s.push(this)}),r&&(f=this,a=Date.now()-u,a<i?setTimeout(function(){r.call(f,t,s)},i-a):r.call(f,t,s))},"html")}function v(t){var n=h(!0);n>0&&p(n,function(){l(),i.getCurPageNum(n)+1<t?(v(t),e("html,body").animate({scrollTop:n},400,"swing")):(e("html,body").animate({scrollTop:n},1e3,"swing"),a())})}function m(){var e=r.getCurrentScrollOffset(n.scrollContainer);return i.getCurPageNum(e)}function g(){var t=e(".ias_loader");return t.size()===0&&(t=e('<div class="ias_loader">'+n.loader+"</div>"),t.hide()),t}function y(){var t=g(),r;n.customLoaderProc!==!1?n.customLoaderProc(t):(r=e(n.container).find(n.item).last(),r.after(t),t.fadeIn())}function b(){var e=g();e.remove()}function w(t){var r=e(".ias_trigger");return r.size()===0&&(r=e('<div class="ias_trigger"><a href="#">'+n.trigger+"</a></div>"),r.hide()),e("a",r).unbind("click").bind("click",function(){return S(),t.call(),!1}),r}function E(t){var r=w(t),i;n.customTriggerProc!==!1?n.customTriggerProc(r):(i=e(n.container).find(n.item).last(),i.after(r),r.fadeIn())}function S(){var e=w();e.remove()}var n=e.extend({},e.ias.defaults,t),r=new e.ias.util,i=new e.ias.paging(n.scrollContainer),s=n.history?new e.ias.history:!1,o=this;u()},e.ias.defaults={container:"#container",scrollContainer:e(window),item:".item",pagination:"#pagination",next:".next",noneleft:!1,loader:'<img src="images/loader.gif"/>',loaderDelay:600,triggerPageThreshold:3,trigger:"Load more items",thresholdMargin:0,history:!0,onPageChange:function(){},beforePageChange:function(){},onLoadItems:function(){},onRenderComplete:function(){},customLoaderProc:!1,customTriggerProc:!1},e.ias.util=function(){function i(){e(window).load(function(){t=!0})}var t=!1,n=!1,r=this;i(),this.forceScrollTop=function(i){e("html,body").scrollTop(0),n||(t?(i.call(),n=!0):setTimeout(function(){r.forceScrollTop(i)},1))},this.getCurrentScrollOffset=function(e){var t,n;return e.get(0)===window?t=e.scrollTop():t=e.offset().top,n=e.height(),t+n}},e.ias.paging=function(){function s(){e(window).scroll(o)}function o(){var t,s,o,f,l;t=i.getCurrentScrollOffset(e(window)),s=u(t),o=a(t),r!==s&&(f=o[0],l=o[1],n.call({},s,f,l)),r=s}function u(e){for(var n=t.length-1;n>0;n--)if(e>t[n][0])return n+1;return 1}function a(e){for(var n=t.length-1;n>=0;n--)if(e>t[n][0])return t[n];return null}var t=[[0,document.location.toString()]],n=function(){},r=1,i=new e.ias.util;s(),this.getCurPageNum=function(t){return t=t||i.getCurrentScrollOffset(e(window)),u(t)},this.onChangePage=function(e){n=e},this.pushPages=function(e,n){t.push([e,n])}},e.ias.history=function(){function n(){t=!!(window.history&&history.pushState&&history.replaceState),t=!1}var e=!1,t=!1;n(),this.setPage=function(e,t){this.updateState({page:e},"",t)},this.havePage=function(){return this.getState()!==!1},this.getPage=function(){var e;return this.havePage()?(e=this.getState(),e.page):1},this.getState=function(){var e,n,r;if(t){n=history.state;if(n&&n.ias)return n.ias}else{e=window.location.hash.substring(0,7)==="#/page/";if(e)return r=parseInt(window.location.hash.replace("#/page/",""),10),{page:r}}return!1},this.updateState=function(t,n,r){e?this.replaceState(t,n,r):this.pushState(t,n,r)},this.pushState=function(n,r,i){var s;t?history.pushState({ias:n},r,i):(s=n.page>0?"#/page/"+n.page:"",window.location.hash=s),e=!0},this.replaceState=function(e,n,r){t?history.replaceState({ias:e},n,r):this.pushState(e,n,r)}}})(jQuery);

$.fn.serializeObject=function(){var a={},k=this.serializeArray();$.each(k,function(){void 0!==a[this.name]?(a[this.name].push||(a[this.name]=[a[this.name]]),a[this.name].push(this.value||"")):a[this.name]=this.value||""});return a};


/*
 * Project: Twitter Bootstrap Hover Dropdown
 * Author: Cameron Spear
 * Contributors: Mattia Larentis
 *
 * Dependencies?: Twitter Bootstrap's Dropdown plugin
 *
 * A simple plugin to enable twitter bootstrap dropdowns to active on hover and provide a nice user experience.
 *
 * No license, do what you want. I'd love credit or a shoutout, though.
 *
 * http://cameronspear.com/blog/twitter-bootstrap-dropdown-on-hover-plugin/
 */
;(function($, window, undefined) {
    // outside the scope of the jQuery plugin to
    // keep track of all dropdowns
    var $allDropdowns = $();

    // if instantlyCloseOthers is true, then it will instantly
    // shut other nav items when a new one is hovered over
    $.fn.dropdownHover = function(options) {

        // the element we really care about
        // is the dropdown-toggle's parent
        $allDropdowns = $allDropdowns.add(this.parent());

        return this.each(function() {
            var $this = $(this).parent(),
                defaults = {
                    delay: 500,
                    instantlyCloseOthers: true
                },
                data = {
                    delay: $(this).data('delay'),
                    instantlyCloseOthers: $(this).data('close-others')
                },
                options = $.extend(true, {}, defaults, options, data),
                timeout;

            $this.hover(function() {
                if(options.instantlyCloseOthers === true)
                    $allDropdowns.removeClass('open');

                window.clearTimeout(timeout);
                $(this).addClass('open');
            }, function() {
                timeout = window.setTimeout(function() {
                    $this.removeClass('open');
                }, options.delay);
            });
        });
    };

    $('[data-toggle="dropdown"]').dropdownHover();
})(jQuery, this);
if(typeof(login_uid)=='undefined'){
    var login_uid = 0;
}
window._is_login = (login_uid>0) ? true : false;
;(function($) {
	/*
	 * lazyload
	 * ====================================================
	*/
	 // lazy avatar
	$('.content .avatar').lazyload({
		attr:'data-original',
		placeholder: zhpConfig.imgUrl+'avatar-default.png',
		event: 'scrollstop'
	});
	$('.sidebar .avatar').lazyload({
		attr:'data-original',
		placeholder: zhpConfig.imgUrl+'avatar-default.png',
		event: 'scrollstop'
	});
	$('.content .thumb').lazyload({
		attr:'data-original',
		placeholder: zhpConfig.imgUrl+'thumbnail.png',
		event: 'scrollstop'
	});
	$('.sidebar .thumb').lazyload({
		attr:'data-original',
		placeholder: zhpConfig.imgUrl+'thumbnail.png',
		event: 'scrollstop'
	});
	$.ias({
		// thresholdMargin: 400,
		triggerPageThreshold: 10,
		history: false,
		container : '.excerpts',
		item: '.excerpt',
		pagination: '.list-page .pager',
		next: '.next a',
		loader: '<div class="pagination-loading"><img src="'+zhpConfig.imgUrl+'loading_line.gif"></div>',
		trigger: '下一页',
		onRenderComplete: function() {
			$('.excerpt:gt(-11) .thumb').lazyload({
				attr: 'data-original',
				placeholder: zhpConfig.imgUrl+'thumbnail.png',
				threshold: 400
			});

    $('.excerpt:gt(-11) header small').each(function() {
        $(this).tooltip({
            container: 'body',
            title: '此文有 ' + $(this).text() + '张 图片'
        })
    })
    $('.excerpt:gt(-11) header .cat').each(function() {
        $(this).tooltip({
            container: 'body',
            title: '查看分类《' + $(this).text() + '》下的文章'
        })
    })

	$('.article-tags a, .post-tags a').each(function() {
		$(this).tooltip({
			container: 'body',
			placement: 'bottom',
			title: '查看关于 ' + $(this).text() + ' 的文章'
		})
	})
		}
	});
	if( $('.widget_ui_tags').length ){
		$('.widget_ui_tags .items a:eq(2), .widget_ui_tags .items a:eq(8), .widget_ui_tags .items a:eq(11), .widget_ui_tags .items a:eq(13), .widget_ui_tags .items a:eq(30)').append('<span class="hot">H</span>')
	}

	$('.widget_postlist:first').affix({
		offset: {
			top: $('.sidebar').height()+100
			, bottom: 70
		}
	})

	var _navto = $('body').attr('data-navto')
	if( _navto ) $('.navto-' + _navto).addClass('active')

	if( $('.article-nav-next a').length ){
		$('section.container').append('<a class="pagebar pagebar-next" title="下一篇：<br>'+$('.article-nav-next').find('a').text()+'" href="'+$('.article-nav-next').find('a').attr('href')+'"><span class="glyphicon glyphicon-chevron-right"></span><i class="glyphicon glyphicon-chevron-right"></i></a>')
	}
	/*
	 * tooltips
	 * ====================================================
	*/
	$('.pagebar-next').tooltip({
		container: 'body',
		placement: 'left',
		html: true
	})

    $('.excerpt header small').each(function() {
        $(this).tooltip({
            container: 'body',
            title: '此文有 ' + $(this).text() + '张 图片'
        })
    })
    $('.excerpt header .cat').each(function() {
        $(this).tooltip({
            container: 'body',
            title: '查看分类《' + $(this).text() + '》下的文章'
        })
    })

	$('.article-tags a, .post-tags a').each(function() {
		$(this).tooltip({
			container: 'body',
			placement: 'bottom',
			title: '查看关于 ' + $(this).text() + ' 的文章'
		})
	})
	$('.widget_tags a').tooltip({
		container: 'body'
	})
	$('.post-edit-link').tooltip({
		container: 'body',
		placement: 'right',
		title: '去后台编辑此文章'
	})
	/*
	 * baidushare
	 * ====================================================
	*/
	if ($('.article-content').length) $('.article-content img').attr('data-tag', 'bdshare')
	window._bd_share_config = {
		common: {
			"bdText": $('title').text().split('--')[0],
			"bdMini": "2",
			"bdUrl": window.location.href+'?share',
			"bdDesc": $('.article-content p:lt(3)').text().substr(0,140),
			"bdMiniList": false,
			"bdPic": $('.article-content img:first') ? $('.article-content img:first').attr('src') : '',
			"bdStyle": "0",
			"bdSize": "24"
		},
		share: [{
			// "bdSize": 12,
			bdCustomStyle: SITEURL + 'Public/Home/css/share.css'
		}],
		/*slide : {
			bdImg : 4,
			bdPos : "right",
			bdTop : 200
		},*/
		image: {
			tag: 'bdshare',
			"viewList": ["qzone", "tsina", "weixin", "tqq", "sqq", "renren", "douban"],
			"viewText": " ",
			"viewSize": "24"
		}
		/*,
		selectShare : {
			"bdContainerClass":'article-content',
			"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]
		}*/
	}
	if( $('.bdsharebuttonbox').length ){
		with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];
	}
	/*
	 * rollbar
	 * ====================================================
	*/
	$('body').append('<div class="rollbar"><a evt="totop" href="javascript:;"></a></div>')
	var scroller = $('.rollbar')
	$(window).scroll(function() {
		document.documentElement.scrollTop + document.body.scrollTop > 200 ? scroller.fadeIn() : scroller.fadeOut();
	})
	/* functions
	 * ====================================================
	 */
	function scrollTo(name, speed) {
		if (!speed) speed = 300
		if (!name) {
			$('html,body').animate({
				scrollTop: 0
			}, speed)
		} else {
			if ($(name).length > 0) {
				$('html,body').animate({
					scrollTop: $(name).offset().top
				}, speed)
			}
		}
	}
	var _loginTipstimer
	function logtips(str){
		if( !str ) return false
		_loginTipstimer && clearTimeout(_loginTipstimer)
		$('.login-tips').html(str).animate({
			top: 0
		}, 220)
		_loginTipstimer = setTimeout(function(){
			$('.login-tips').animate({
				top: -30
			}, 220)
		}, 5000)
	}
	function is_name(str) {
		return /^(\w){3,20}$/.test(str)
	}
	function is_mail(str) {
		return /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(str)
	}
	/* event click
	 * ====================================================
	 */
	$(document).on('click', function(e) {
		e = e || window.event;
		var target = e.target || e.srcElement,
			_ta = $(target)
		if (_ta.hasClass('disabled')) return
		if (_ta.parent().attr('evt')) _ta = $(_ta.parent()[0])
		if (_ta.parent().parent().attr('evt')) _ta = $(_ta.parent().parent()[0])
		var type = _ta.attr('evt')
		switch (type) {
			case 'totop':
				scrollTo()
				break;
			case 'like':
				var pid = _ta.attr('data-pid')
				var clickid = _ta.attr('data-key');
				var hash = _ta.attr('data-hash');
				if ( !pid || !/^\d{1,}$/.test(pid) ) return;
				$.ajax({
					url: SITEURL + 'index.php?option=misc&task=click&op=add',
					type: 'POST',
					dataType: 'json',
					data: {
						clickid:clickid,
						key: 'like',
						idtype:'aid',
						hash:hash,
						id: pid,
						ajax: 1
					},
					success: function(data, textStatus, xhr) {
						if( data.status === 1 ){
							_ta.toggleClass('actived')
							_ta.find('span').html(data.info)
						} else {
							alert(data.info);
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						//called when there is an error
						console.log(xhr)
					}
				});
				break;
			case 'login':
				if( _is_login ) return
				if( !window._loginCache ){
					$.ajax({
						type: 'GET',
						url:  SITEURL+'index.php?option=member&task=login&ajax=1',
						success: function(data){
							window._loginCache = true
							$('body').append(data)
							$('#modal-login').on('shown.bs.modal', function (e) {
								$('.login-item:eq('+(_ta.attr('reg')?1:0)+')').find('input:eq(0)').focus()
							})
							$('.login-item:eq('+(_ta.attr('reg')?1:0)+')').show()
							$('#modal-login').modal('show')
						}
					});
				}else{
					$('.login-item:eq('+(_ta.attr('reg')?1:0)+')').show().siblings('.login-item').hide()
					$('#modal-login').modal('show')
				}
				break;
			case 'login.to.login':
				$('.login-item:eq(1)').hide()
				$('.login-item:eq(0)').show().find('input:eq(0)').focus()
				break;
			case 'login.to.reg':
				$('.login-item:eq(0)').hide()
				$('.login-item:eq(1)').show().find('input:eq(0)').focus()
				break;
			case 'login.submit':
				if( _is_login ) return
				var form = _ta.parent().parent()
				var isreg = _ta.attr('reg')
				var inputs = form.serializeObject()
				if( !inputs.action ){
					return
				}
				if( !is_name(inputs.username) ){
					logtips('昵称限制在3-20字')
					return
				}
				if( inputs.password.length < 6 ){
					logtips('密码太短，至少6位')
					return
				}
				if( isreg ){
					if( !is_mail(inputs.email) ){
						logtips('邮箱格式错误')
						return
					}
				}
				$.ajax({
					type: "POST",
					url:  SITEURL+'index.php?option=member&task=login&ajax=1',
					data: inputs,
					dataType: 'json',
					success: function(data){
						if( data.status!=1 ){
							if( data.info ){
								logtips(data.info)
							}
							return
						} else {
								alert(data.info);
						}
						if( !isreg ){
							location.reload()
						}else{
							if( data.goto ) location.href = data.goto
						}
					}
				});
				break;
		}
	})

})(jQuery)